////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file Toolbench\ToolbenchManager.cpp
/// @author Scott Greenup
/// @author Nathan Chambers
/// @date 28.08.2012
/// @pre LoadBlock() in first
/// @bug Default rotations on all objects is not fixed
/// @bug SpotLight not loaded
/// @bug PointLight not loaded
/// @bug AmbientLight not loaded
/// @bug PhysXModelLibrary not even touched
/// @brief Implements the toolbench manager class for loading blocks
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
#include "ToolbenchManager.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
#include <efd/File.h>
#include <efdPhysX/PhysXSDKManager.h>

////////////////////////////////////////////////////////////////////////////////////////////////////
#include <NiAmbientLight.h>
#include <NiDirectionalLight.h>
#include <NiLog.h>
#include <NiMath.h>
#include <NiNode.h>
#include <NiPointLight.h>
#include <NiSpotLight.h>
#include <NiStream.h>
#include <NiPhysX.h>

////////////////////////////////////////////////////////////////////////////////////////////////////
ToolbenchManager::ToolbenchManager(const char* ac_szGameSolutionPath)
{
	m_kSolutionPath = ac_szGameSolutionPath;
	m_kSolutionRoot = ac_szGameSolutionPath;
	m_kSolutionRoot.RemoveRange(m_kSolutionRoot.FindReverse('/') + 1, m_kSolutionRoot.Length());

	LoadAssetLinks();

	m_pakSpawnPoints = new efd::list<NiSpawnPoint>();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
ToolbenchManager::~ToolbenchManager()
{
	for (auto flatIter = m_akDefaultModels.begin(); flatIter != m_akDefaultModels.end(); ++flatIter)
	{
		NiDelete flatIter->second;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool ToolbenchManager::LoadBlock(const char* ac_szBlockName)
{
	// create scene
	m_spkScene = NiNew NiNode;

	//Create Empty PhysX Scene
	m_spkPhysXScene = NiNew NiPhysXScene;

	//Create Scene Description
	NxSceneDesc kSceneDesc;
	kSceneDesc.gravity.set(0.0f, 0.0f, -20.0f);

	//Set PhysX Scene
	efdPhysX::PhysXSDKManager* pkPhysXManager = efdPhysX::PhysXSDKManager::GetManager();
	NxScene* pkScene = pkPhysXManager->m_pPhysXSDK->createScene(kSceneDesc);
	m_spkPhysXScene->SetPhysXScene(pkScene);

	efd::TiXmlDocument kBlockDoc;
	bool bLoaded = kBlockDoc.LoadFile(m_akBlocks[ac_szBlockName]);

	if (bLoaded == false)
	{
		return false;
	}

	efd::TiXmlElement* pkGameEle		= kBlockDoc.FirstChildElement("game");
	efd::TiXmlElement* pkEntitySetEle	= pkGameEle->FirstChildElement("entitySet");
	efd::TiXmlElement* pkEntityEle		= pkEntitySetEle->FirstChildElement("entity");

	// entity loop
	while (pkEntityEle != nullptr)
	{
		//HandleEntity(pkEntityEle);
		NiString kModelName = pkEntityEle->Attribute("modelName");
		NiString kName = pkEntityEle->Attribute("name");

		if (kModelName.Compare("Mesh") == 0)
		{
			HandleMesh(pkEntityEle);
		}
		else if (kModelName.Compare("DirectionalLight") == 0)
		{
			HandleDirectionalLight(pkEntityEle);
		}
		else if (kModelName.Compare("AmbientLight") == 0)
		{
			HandleAmbientLight(pkEntityEle);
		}
		else if (kModelName.Compare("SpotLight") == 0)
		{
			HandleSpotLight(pkEntityEle);
		}
		else if (kModelName.Compare("PointLight") == 0)
		{
			HandlePointLight(pkEntityEle);
		}
		else if (kModelName.Compare("PlayerSpawn") == 0)
		{
			HandlePlayerSpawn(pkEntityEle);
		}
		else if (kModelName.Compare("DemonSpawn") == 0)
		{
			HandleDemonSpawn(pkEntityEle);
		}

		pkEntityEle = pkEntityEle->NextSiblingElement();
	}

	m_spkScene->Update(0);
	m_spkScene->UpdateProperties();
	m_spkScene->UpdateEffects();

	m_spkPhysXScene->SetUpdateSrc(true);
	m_spkPhysXScene->SetUpdateDest(true);

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
NiNode* ToolbenchManager::GetScene()
{
	return m_spkScene;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
NiPhysXScene* ToolbenchManager::GetPhysXScene()
{
	return m_spkPhysXScene;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::LoadAssetLinks()
{
	// paths to file
	NiString kPathToRELPATH = m_kSolutionRoot + "asset-web-metadata/relpath.nt";
	NiString kPathToLLID	= m_kSolutionRoot + "asset-web-metadata/llid.nt";

	// file containers
	efd::File *pkFileRELPATH = efd::File::GetFile( kPathToRELPATH, efd::File::READ_ONLY );
	efd::File *pkFileLLID = efd::File::GetFile( kPathToLLID, efd::File::READ_ONLY );

	if (pkFileRELPATH->IsGood() == false)
		NIASSERT(0);

	if (pkFileLLID->IsGood() == false)
		NIASSERT(0);

	pkFileRELPATH->Seek(0, efd::File::SO_BEGIN);
	pkFileLLID->Seek(0, efd::File::SO_BEGIN);

	// handle the relpath.nt then then llid.nt
	HandleRELPATH(pkFileRELPATH);
	HandleLLID(pkFileLLID);

	for (auto iter = m_akUnhandledFlats.begin(); iter != m_akUnhandledFlats.end(); ++iter)
	{
		efd::TiXmlDocument kFlatDoc;
		bool bLoaded = kFlatDoc.LoadFile((*iter).m_kPath, efd::TIXML_DEFAULT_ENCODING);

		if ( bLoaded == true)
		{
			HandleFlat(kFlatDoc);
		}
	}


	NiDelete pkFileRELPATH;
	NiDelete pkFileLLID;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleLLID(efd::File* a_pkFile)
{
	NiString kLineBuffer;

	NiString kUUIDTemp;
	NiString kLLIDTemp;

	unsigned int uiStart;
	unsigned int uiEnd;

	char acBuff[256];

	while ( a_pkFile->eof() == false )
	{
		// clear buffer
		for (int i = 0; i < 256; ++i)
			acBuff[i] = '\0';

		a_pkFile->GetLine(acBuff, 256);
		kLineBuffer = acBuff;

		// get UUID
		uiStart		= 10;
		uiEnd		= kLineBuffer.Find('>');
		kUUIDTemp	= kLineBuffer.GetSubstring(uiStart, uiEnd);

		// get LLID
		uiStart		= kLineBuffer.Find('\"');
		uiEnd		= kLineBuffer.FindReverse('\"');
		kLLIDTemp	= kLineBuffer.GetSubstring(uiStart + 1, uiEnd);

		for (auto iter = m_akNifs.begin(); iter != m_akNifs.end(); ++iter)
		{
			if ( (*iter).m_kUUID == kUUIDTemp )
			{
				(*iter).m_kLLID = kLLIDTemp;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleRELPATH(efd::File* a_pkFile)
{
	NiString kLineBuffer;

	unsigned int uiStart;
	unsigned int uiEnd;

	char acBuff[256];

	while ( a_pkFile->eof() == false )
	{
		NiNifMeta kLink = { 0 };

		// clear buffer
		for (int i = 0; i < 256; ++i)
			acBuff[i] = '\0';

		a_pkFile->GetLine(acBuff, 256);
		kLineBuffer = acBuff;

		// find location of uuid
		uiStart		= 10;
		uiEnd		= kLineBuffer.Find('>');

		kLink.m_kUUID = kLineBuffer.GetSubstring(uiStart, uiEnd);
			
		// find location of path
		uiStart		= kLineBuffer.Find('\"');
		uiStart		= uiStart + 1;
		uiEnd		= kLineBuffer.FindReverse('\"');

		kLink.m_kPath = m_kSolutionRoot + kLineBuffer.GetSubstring(uiStart + 1, uiEnd);

		int iType = 0;

		if (		kLink.m_kPath.Find(".nif")		!= NiString::INVALID_INDEX) { iType = 1; }
		else if (	kLink.m_kPath.Find(".flat")		!= NiString::INVALID_INDEX) { iType = 2; }
		else if (	kLink.m_kPath.Find(".xblock")	!= NiString::INVALID_INDEX) { iType = 3; }

		switch(iType)
		{
		case 1: // nif
			{
				m_akNifs.push_back(kLink);
				break;
			}
		case 2: // flat
			{
				NiFlatMeta kFlatMeta;
				kFlatMeta.m_kPath = kLink.m_kPath;

				m_akUnhandledFlats.push_back(kFlatMeta);

				//efd::TiXmlDocument kFlatDoc;
				//bool bLoaded = kFlatDoc.LoadFile(kLink.m_kPath, efd::TIXML_DEFAULT_ENCODING);

				//if ( bLoaded == true)
				//{
				//	HandleFlat(kFlatDoc);
				//}

				break;
			}
		case 3: // xblock
			{
				uiEnd	= kLink.m_kPath.FindReverse('.');
				uiStart = kLink.m_kPath.FindReverse('/') + 1;

				NiString kName = kLink.m_kPath.GetSubstring(uiStart, uiEnd);
				m_akBlocks[kName] = kLink.m_kPath;

				break;
			}
		default:
			{
				break;
			}
		} // end switch
	} // end while
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool ToolbenchManager::HandleFlat(efd::TiXmlDocument &a_rkFlatDoc)
{
	efd::TiXmlElement* pkModel = a_rkFlatDoc.FirstChildElement("model");

	if (pkModel == nullptr)
		return false;

	efd::TiXmlElement* pkProp = pkModel->FirstChildElement("property");

	if (pkProp == nullptr)
		return false;

	NiFlatMeta* pkFlatMeta = new NiFlatMeta();
	pkFlatMeta->m_kName = pkModel->Attribute("name");
	
	// iterate through properties
	while (pkProp != nullptr)
	{
		efd::TiXmlElement* pkSet = pkProp->FirstChildElement("set");

		if (pkSet == nullptr)
		{
			pkProp = pkProp->NextSiblingElement();
			continue;
		}

		NiPropertyMeta kPropMeta;
		kPropMeta.m_kName = pkProp->Attribute("name");
		kPropMeta.m_kType = pkProp->Attribute("type");
		kPropMeta.m_kValue = pkSet->Attribute("value");

		
		if (kPropMeta.m_kType.Compare("AssetID") == 0 &&
			kPropMeta.m_kName.Compare("NifAsset") == 0)
		{
			kPropMeta.m_kValue.RemoveRange(0, 9);

			// needs mallet slaming
			for (auto iter = m_akNifs.begin(); iter != m_akNifs.end(); ++iter)
			{
				NiString kLLID = (*iter).m_kLLID;

				if ( kLLID.Compare(kPropMeta.m_kValue) == 0)
				{
					kPropMeta.m_kValue = (*iter).m_kPath;
				}
			}
		}
		
		pkFlatMeta->m_akProps[kPropMeta.m_kName] = kPropMeta;

		pkProp = pkProp->NextSiblingElement("property");
	}

	m_akDefaultModels[pkFlatMeta->m_kName] = pkFlatMeta;

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleMesh(efd::TiXmlElement* a_pkEntity)
{
	NiNodePtr spkMesh = NiNew NiNode();
	spkMesh->SetName(a_pkEntity->Attribute("name"));

	NiFlatMeta* pkFlatData = m_akDefaultModels["Mesh"];

	//Default Position
	NiPoint3 kPosition;
	pkFlatData->m_akProps["Position"].m_kValue.ToPoint3(kPosition);
	spkMesh->SetTranslate(kPosition);

	//Default Rotation
	NiPoint3 kRotation;
	pkFlatData->m_akProps["Rotation"].m_kValue.ToPoint3(kRotation);
	NiMatrix3 mRotation;
	mRotation.FromEulerAnglesXYZ(kRotation.x,kRotation.y,kRotation.z);
	spkMesh->SetRotate(mRotation);

	//Default Scale
	float fScale;
	pkFlatData->m_akProps["Scale"].m_kValue.ToFloat(fScale);
	spkMesh->SetScale(fScale);

	//Override Properties
	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");
	NiString kNifPath;

	while (pkProperty != nullptr)
	{
		NiString kPropertyName = pkProperty->Attribute("name");
		NiString kValue = pkProperty->FirstChildElement("set")->Attribute("value");

		if(kPropertyName.Compare("Position") == 0)
		{
			NiPoint3 kPosition;
			kValue.ToPoint3(kPosition);
			spkMesh->SetTranslate(kPosition);
		}
		else if(kPropertyName.Compare("Rotation") == 0)
		{
			NiPoint3 kRotation;
			kValue.ToPoint3(kRotation);

			NiMatrix3 mRotation;
			kRotation = kRotation * -NI_PI / 180.0f;
			mRotation.FromEulerAnglesXYZ(kRotation.x ,kRotation.y ,kRotation.z );
			spkMesh->SetRotate(mRotation);
		}
		else if(kPropertyName.Compare("Scale") == 0)
		{
			float fScale;
			kValue.ToFloat(fScale);
			spkMesh->SetScale(fScale);
		}
		else if(kPropertyName.Compare("NifAsset") == 0)
		{
			kValue.RemoveRange(0, 9);

			// needs mallet slaming
			for (auto iter = m_akNifs.begin(); iter != m_akNifs.end(); ++iter)
			{
				NiString kLLID = (*iter).m_kLLID;

				if ( kLLID.Compare(kValue) == 0)
				{
					kValue = (*iter).m_kPath;
					break;
				}
			}
			kNifPath = kValue;
		}

		pkProperty = pkProperty->NextSiblingElement();
	}
	
	NiStream kStream;
	if (kStream.Load(kNifPath))
	{
		NiNodePtr kNifRoot = NiDynamicCast(NiNode,kStream.GetObjectAt(0));
		spkMesh->AttachChild(kNifRoot);
	}
	else
	{
		NiOutputDebugString("NIF did not load");
		NIASSERT(0);
		return;
	}

	NiMatrix3 mSpawnRotation = spkMesh->GetRotate();
	NiPoint3 vSpawnPosition = spkMesh->GetTranslate();

	//Extract PhysX Prop
	for(unsigned int ui = 0;ui < kStream.GetObjectCount();++ui)
	{
		if(kStream.GetObjectAt(ui)->IsKindOf(&NiPhysXProp::ms_RTTI))
		{
			NiPhysXProp* pkProp = NiDynamicCast(NiPhysXProp,kStream.GetObjectAt(ui));

			NxMat34 kXForm;
			NiPhysXTypes::NiTransformToNxMat34(mSpawnRotation,vSpawnPosition,kXForm);
			pkProp->SetXform(kXForm);

			m_spkPhysXScene->AddProp(pkProp);

			NiPhysXPropDesc* pkPropDesc = pkProp->GetSnapshot();
			if(pkPropDesc->GetActorCount() > 0)
			{
				NiPhysXActorDesc* pkActorDesc = pkPropDesc->GetActorAt(0);
				NxActor* pkObjActor = pkActorDesc->GetActor();
				pkObjActor->userData = (void*)spkMesh;
			}
		}
	}

	//Add Prop to Scene
	m_spkScene->AttachChild(spkMesh);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleDirectionalLight(efd::TiXmlElement* a_pkEntity)
{
	NiDirectionalLightPtr spkLight = NiNew NiDirectionalLight();

	NiFlatMeta* pkFlatData = m_akDefaultModels["DirectionalLight"];

	//Default Position
	NiPoint3 kPosition;
	pkFlatData->m_akProps["Position"].m_kValue.ToPoint3(kPosition);
	spkLight->SetTranslate(kPosition);

	//Default Rotation
	NiPoint3 kRotation;
	pkFlatData->m_akProps["Rotation"].m_kValue.ToPoint3(kRotation);
	NiMatrix3 mRotation;
	mRotation.FromEulerAnglesXYZ(kRotation.x,kRotation.y,kRotation.z);
	spkLight->SetRotate(mRotation);

	//Default Scale
	float fScale;
	pkFlatData->m_akProps["Scale"].m_kValue.ToFloat(fScale);
	spkLight->SetScale(fScale);

	NiPoint3 kDiffuseColor;
	pkFlatData->m_akProps["DiffuseColor"].m_kValue.ToPoint3(kDiffuseColor);
	spkLight->SetDiffuseColor( NiColor(kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));

	NiPoint3 kAmbientColor;
	pkFlatData->m_akProps["AmbientColor"].m_kValue.ToPoint3(kAmbientColor);
	spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));

	NiPoint3 kSpecularColor;
	pkFlatData->m_akProps["SpecularColor"].m_kValue.ToPoint3(kSpecularColor);
	spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));

	//Override Properties
	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");
	NiString kNifPath;

	while (pkProperty != nullptr)
	{
		NiString kPropertyName = pkProperty->Attribute("name");
		NiString kValue = pkProperty->FirstChildElement("set")->Attribute("value");

		if(kPropertyName.Compare("Position") == 0)
		{
			NiPoint3 kPosition;
			kValue.ToPoint3(kPosition);
			spkLight->SetTranslate(kPosition);
		}
		else if(kPropertyName.Compare("Rotation") == 0)
		{
			NiPoint3 kRotation;
			kValue.ToPoint3(kRotation);

			NiMatrix3 mRotation;
			kRotation = kRotation * -NI_PI / 180.0f;
			mRotation.FromEulerAnglesXYZ(kRotation.x ,kRotation.y ,kRotation.z );
			spkLight->SetRotate(mRotation);
		}
		else if(kPropertyName.Compare("Scale") == 0)
		{
			float fScale;
			kValue.ToFloat(fScale);
			spkLight->SetScale(fScale);
		}
		else if(kPropertyName.Compare("DiffuseColor") == 0)
		{
			NiPoint3 kDiffuseColor;
			kValue.ToPoint3(kDiffuseColor);

			spkLight->SetDiffuseColor( NiColor( kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));
		}
		else if(kPropertyName.Compare("AmbientColor") == 0)
		{
			NiPoint3 kAmbientColor;
			kValue.ToPoint3(kAmbientColor);

			spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));
		}
		else if(kPropertyName.Compare("SpecularColor") == 0)
		{
			NiPoint3 kSpecularColor;
			kValue.ToPoint3(kSpecularColor);

			spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));
		}
		else if(kPropertyName.Compare("Dimmer") == 0)
		{
			float fDimmer;
			kValue.ToFloat(fDimmer);

			spkLight->SetDimmer(fDimmer);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	spkLight->AttachAffectedNode(m_spkScene);
	m_spkScene->AttachChild(spkLight);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleAmbientLight(efd::TiXmlElement* a_pkEntity)
{
	NiAmbientLightPtr spkLight = NiNew NiAmbientLight();

	NiFlatMeta* pkFlatData = m_akDefaultModels["AmbientLight"];

	//Default Position
	NiPoint3 kPosition;
	pkFlatData->m_akProps["Position"].m_kValue.ToPoint3(kPosition);
	spkLight->SetTranslate(kPosition);

	//Default Rotation
	NiPoint3 kRotation;
	pkFlatData->m_akProps["Rotation"].m_kValue.ToPoint3(kRotation);
	NiMatrix3 mRotation;
	mRotation.FromEulerAnglesXYZ(kRotation.x,kRotation.y,kRotation.z);
	spkLight->SetRotate(mRotation);

	//Default Scale
	float fScale;
	pkFlatData->m_akProps["Scale"].m_kValue.ToFloat(fScale);
	spkLight->SetScale(fScale);

	NiPoint3 kDiffuseColor;
	pkFlatData->m_akProps["DiffuseColor"].m_kValue.ToPoint3(kDiffuseColor);
	spkLight->SetDiffuseColor( NiColor(kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));

	NiPoint3 kAmbientColor;
	pkFlatData->m_akProps["AmbientColor"].m_kValue.ToPoint3(kAmbientColor);
	spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));

	NiPoint3 kSpecularColor;
	pkFlatData->m_akProps["SpecularColor"].m_kValue.ToPoint3(kSpecularColor);
	spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));

	//Override Properties
	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");
	NiString kNifPath;

	while (pkProperty != nullptr)
	{
		NiString kPropertyName = pkProperty->Attribute("name");
		NiString kValue = pkProperty->FirstChildElement("set")->Attribute("value");

		if(kPropertyName.Compare("Position") == 0)
		{
			NiPoint3 kPosition;
			kValue.ToPoint3(kPosition);
			spkLight->SetTranslate(kPosition);
		}
		else if(kPropertyName.Compare("Rotation") == 0)
		{
			NiPoint3 kRotation;
			kValue.ToPoint3(kRotation);

			NiMatrix3 mRotation;
			kRotation = kRotation * -NI_PI / 180.0f;
			mRotation.FromEulerAnglesXYZ(kRotation.x ,kRotation.y ,kRotation.z );
			spkLight->SetRotate(mRotation);
		}
		else if(kPropertyName.Compare("Scale") == 0)
		{
			float fScale;
			kValue.ToFloat(fScale);
			spkLight->SetScale(fScale);
		}
		else if(kPropertyName.Compare("DiffuseColor") == 0)
		{
			NiPoint3 kDiffuseColor;
			kValue.ToPoint3(kDiffuseColor);

			spkLight->SetDiffuseColor( NiColor( kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));
		}
		else if(kPropertyName.Compare("AmbientColor") == 0)
		{
			NiPoint3 kAmbientColor;
			kValue.ToPoint3(kAmbientColor);

			spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));
		}
		else if(kPropertyName.Compare("SpecularColor") == 0)
		{
			NiPoint3 kSpecularColor;
			kValue.ToPoint3(kSpecularColor);

			spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));
		}
		else if(kPropertyName.Compare("Dimmer") == 0)
		{
			float fDimmer;
			kValue.ToFloat(fDimmer);

			spkLight->SetDimmer(fDimmer);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	spkLight->AttachAffectedNode(m_spkScene);
	m_spkScene->AttachChild(spkLight);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleSpotLight(efd::TiXmlElement* a_pkEntity)
{
	NiSpotLightPtr spkLight = NiNew NiSpotLight();

	NiFlatMeta* pkFlatData = m_akDefaultModels["SpotLight"];

	//Default Position
	NiPoint3 kPosition;
	pkFlatData->m_akProps["Position"].m_kValue.ToPoint3(kPosition);
	spkLight->SetTranslate(kPosition);

	//Default Rotation
	NiPoint3 kRotation;
	pkFlatData->m_akProps["Rotation"].m_kValue.ToPoint3(kRotation);
	NiMatrix3 mRotation;
	mRotation.FromEulerAnglesXYZ(kRotation.x,kRotation.y,kRotation.z);
	spkLight->SetRotate(mRotation);

	//Default Scale
	float fScale;
	pkFlatData->m_akProps["Scale"].m_kValue.ToFloat(fScale);
	spkLight->SetScale(fScale);

	NiPoint3 kDiffuseColor;
	pkFlatData->m_akProps["DiffuseColor"].m_kValue.ToPoint3(kDiffuseColor);
	spkLight->SetDiffuseColor( NiColor(kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));

	NiPoint3 kAmbientColor;
	pkFlatData->m_akProps["AmbientColor"].m_kValue.ToPoint3(kAmbientColor);
	spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));

	NiPoint3 kSpecularColor;
	pkFlatData->m_akProps["SpecularColor"].m_kValue.ToPoint3(kSpecularColor);
	spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));

	float fOuterSpotAngle;
	pkFlatData->m_akProps["OuterSpotAngle"].m_kValue.ToFloat(fOuterSpotAngle);
	spkLight->SetSpotAngle(fOuterSpotAngle);

	float fInnerSpotAngle;
	pkFlatData->m_akProps["InnerSpotAngle"].m_kValue.ToFloat(fInnerSpotAngle);
	spkLight->SetInnerSpotAngle(fInnerSpotAngle);

	float fSpotExponent;
	pkFlatData->m_akProps["SpotExponent"].m_kValue.ToFloat(fSpotExponent);
	spkLight->SetSpotExponent(fSpotExponent);

	float fConstantAttenuation;
	pkFlatData->m_akProps["ConstantAttenuation"].m_kValue.ToFloat(fConstantAttenuation);
	spkLight->SetConstantAttenuation(fConstantAttenuation);

	float fLinearAttenuation;
	pkFlatData->m_akProps["LinearAttenuation"].m_kValue.ToFloat(fLinearAttenuation);
	spkLight->SetLinearAttenuation(fLinearAttenuation);

	float fQuadraticAttenuation;
	pkFlatData->m_akProps["QuadraticAttenuation"].m_kValue.ToFloat(fQuadraticAttenuation);
	spkLight->SetQuadraticAttenuation(fQuadraticAttenuation);

	//Override Properties
	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");
	NiString kNifPath;

	while (pkProperty != nullptr)
	{
		NiString kPropertyName = pkProperty->Attribute("name");
		NiString kValue = pkProperty->FirstChildElement("set")->Attribute("value");

		if(kPropertyName.Compare("Position") == 0)
		{
			//NiPoint3 kPosition;
			kValue.ToPoint3(kPosition);
			spkLight->SetTranslate(kPosition);
		}
		else if(kPropertyName.Compare("Rotation") == 0)
		{
			//NiPoint3 kRotation;
			kValue.ToPoint3(kRotation);

			//NiMatrix3 mRotation;
			kRotation = kRotation * -NI_PI / 180.0f;
			mRotation.FromEulerAnglesXYZ(kRotation.x ,kRotation.y ,kRotation.z );
			spkLight->SetRotate(mRotation);
		}
		else if(kPropertyName.Compare("Scale") == 0)
		{
			//float fScale;
			kValue.ToFloat(fScale);
			spkLight->SetScale(fScale);
		}
		else if(kPropertyName.Compare("DiffuseColor") == 0)
		{
			//NiPoint3 kDiffuseColor;
			kValue.ToPoint3(kDiffuseColor);
			spkLight->SetDiffuseColor( NiColor( kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));
		}
		else if(kPropertyName.Compare("AmbientColor") == 0)
		{
			//NiPoint3 kAmbientColor;
			kValue.ToPoint3(kAmbientColor);
			spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));
		}
		else if(kPropertyName.Compare("SpecularColor") == 0)
		{
			//NiPoint3 kSpecularColor;
			kValue.ToPoint3(kSpecularColor);
			spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));
		}
		else if(kPropertyName.Compare("Dimmer") == 0)
		{
			float fDimmer;
			kValue.ToFloat(fDimmer);

			spkLight->SetDimmer(fDimmer);
		}
		else if(kPropertyName.Compare("OuterSpotAngle") == 0)
		{
			kValue.ToFloat(fOuterSpotAngle);
			spkLight->SetSpotAngle(fOuterSpotAngle);
		}
		else if(kPropertyName.Compare("InnerSpotAngle") == 0)
		{
			kValue.ToFloat(fInnerSpotAngle);
			spkLight->SetInnerSpotAngle(fInnerSpotAngle);
		}
		else if(kPropertyName.Compare("SpotExponent") == 0)
		{
			kValue.ToFloat(fSpotExponent);
			spkLight->SetSpotExponent(fSpotExponent);
		}
		else if(kPropertyName.Compare("ConstantAttenuation") == 0)
		{
			kValue.ToFloat(fConstantAttenuation);
			spkLight->SetConstantAttenuation(fConstantAttenuation);
		}
		else if(kPropertyName.Compare("LinearAttenuation") == 0)
		{
			kValue.ToFloat(fLinearAttenuation);
			spkLight->SetLinearAttenuation(fLinearAttenuation);
		}
		else if(kPropertyName.Compare("QuadraticAttenuation") == 0)
		{
			kValue.ToFloat(fQuadraticAttenuation);
			spkLight->SetQuadraticAttenuation(fQuadraticAttenuation);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	spkLight->AttachAffectedNode(m_spkScene);
	m_spkScene->AttachChild(spkLight);
}

void ToolbenchManager::HandlePointLight(efd::TiXmlElement* a_pkEntity)
{
	NiPointLightPtr spkLight = NiNew NiPointLight();

	NiFlatMeta* pkFlatData = m_akDefaultModels["PointLight"];

	//Default Position
	NiPoint3 kPosition;
	pkFlatData->m_akProps["Position"].m_kValue.ToPoint3(kPosition);
	spkLight->SetTranslate(kPosition);

	//Default Rotation
	NiPoint3 kRotation;
	pkFlatData->m_akProps["Rotation"].m_kValue.ToPoint3(kRotation);
	NiMatrix3 mRotation;
	mRotation.FromEulerAnglesXYZ(kRotation.x,kRotation.y,kRotation.z);
	spkLight->SetRotate(mRotation);

	//Default Scale
	float fScale;
	pkFlatData->m_akProps["Scale"].m_kValue.ToFloat(fScale);
	spkLight->SetScale(fScale);

	NiPoint3 kDiffuseColor;
	pkFlatData->m_akProps["DiffuseColor"].m_kValue.ToPoint3(kDiffuseColor);
	spkLight->SetDiffuseColor( NiColor(kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));

	NiPoint3 kAmbientColor;
	pkFlatData->m_akProps["AmbientColor"].m_kValue.ToPoint3(kAmbientColor);
	spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));

	NiPoint3 kSpecularColor;
	pkFlatData->m_akProps["SpecularColor"].m_kValue.ToPoint3(kSpecularColor);
	spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));

	float fConstantAttenuation;
	pkFlatData->m_akProps["ConstantAttenuation"].m_kValue.ToFloat(fConstantAttenuation);
	spkLight->SetConstantAttenuation(fConstantAttenuation);

	float fLinearAttenuation;
	pkFlatData->m_akProps["LinearAttenuation"].m_kValue.ToFloat(fLinearAttenuation);
	spkLight->SetLinearAttenuation(fLinearAttenuation);

	float fQuadraticAttenuation;
	pkFlatData->m_akProps["QuadraticAttenuation"].m_kValue.ToFloat(fQuadraticAttenuation);
	spkLight->SetQuadraticAttenuation(fQuadraticAttenuation);

	//Override Properties
	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");
	NiString kNifPath;

	while (pkProperty != nullptr)
	{
		NiString kPropertyName = pkProperty->Attribute("name");
		NiString kValue = pkProperty->FirstChildElement("set")->Attribute("value");

		if(kPropertyName.Compare("Position") == 0)
		{
			//NiPoint3 kPosition;
			kValue.ToPoint3(kPosition);
			spkLight->SetTranslate(kPosition);
		}
		else if(kPropertyName.Compare("Rotation") == 0)
		{
			//NiPoint3 kRotation;
			kValue.ToPoint3(kRotation);

			//NiMatrix3 mRotation;
			kRotation = kRotation * -NI_PI / 180.0f;
			mRotation.FromEulerAnglesXYZ(kRotation.x ,kRotation.y ,kRotation.z );
			spkLight->SetRotate(mRotation);
		}
		else if(kPropertyName.Compare("Scale") == 0)
		{
			//float fScale;
			kValue.ToFloat(fScale);
			spkLight->SetScale(fScale);
		}
		else if(kPropertyName.Compare("DiffuseColor") == 0)
		{
			//NiPoint3 kDiffuseColor;
			kValue.ToPoint3(kDiffuseColor);
			spkLight->SetDiffuseColor( NiColor( kDiffuseColor.x, kDiffuseColor.y, kDiffuseColor.z ));
		}
		else if(kPropertyName.Compare("AmbientColor") == 0)
		{
			//NiPoint3 kAmbientColor;
			kValue.ToPoint3(kAmbientColor);
			spkLight->SetAmbientColor( NiColor( kAmbientColor.x, kAmbientColor.y, kAmbientColor.z ));
		}
		else if(kPropertyName.Compare("SpecularColor") == 0)
		{
			//NiPoint3 kSpecularColor;
			kValue.ToPoint3(kSpecularColor);
			spkLight->SetSpecularColor( NiColor( kSpecularColor.x, kSpecularColor.y, kSpecularColor.z ));
		}
		else if(kPropertyName.Compare("Dimmer") == 0)
		{
			float fDimmer;
			kValue.ToFloat(fDimmer);

			spkLight->SetDimmer(fDimmer);
		}
		else if(kPropertyName.Compare("ConstantAttenuation") == 0)
		{
			kValue.ToFloat(fConstantAttenuation);
			spkLight->SetConstantAttenuation(fConstantAttenuation);
		}
		else if(kPropertyName.Compare("LinearAttenuation") == 0)
		{
			kValue.ToFloat(fLinearAttenuation);
			spkLight->SetLinearAttenuation(fLinearAttenuation);
		}
		else if(kPropertyName.Compare("QuadraticAttenuation") == 0)
		{
			kValue.ToFloat(fQuadraticAttenuation);
			spkLight->SetQuadraticAttenuation(fQuadraticAttenuation);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	spkLight->AttachAffectedNode(m_spkScene);
	m_spkScene->AttachChild(spkLight);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandlePlayerSpawn(efd::TiXmlElement* a_pkEntity)
{
	NiSpawnPoint kSpawnPoint;
	kSpawnPoint.m_eType = NiSpawnPoint::ST_PLAYER;

	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");

	while (pkProperty != nullptr)
	{
		NiString kName = pkProperty->Attribute("name");
		
		if (kName.Compare("Position") == 0)
		{
			NiString kValue;
			kValue = pkProperty->FirstChildElement("set")->Attribute("value");

			kValue.ToPoint3(kSpawnPoint.m_kTranslate);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	m_pakSpawnPoints->push_back(kSpawnPoint);

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ToolbenchManager::HandleDemonSpawn(efd::TiXmlElement* a_pkEntity)
{
	NiSpawnPoint kSpawnPoint;
	kSpawnPoint.m_eType = NiSpawnPoint::ST_DEMON;

	efd::TiXmlElement* pkProperty = a_pkEntity->FirstChildElement("property");

	while (pkProperty != nullptr)
	{
		NiString kName = pkProperty->Attribute("name");

		if (kName.Compare("Position") == 0)
		{
			NiString kValue;
			kValue = pkProperty->FirstChildElement("set")->Attribute("value");

			kValue.ToPoint3(kSpawnPoint.m_kTranslate);
		}

		pkProperty = pkProperty->NextSiblingElement();
	}

	m_pakSpawnPoints->push_back(kSpawnPoint);
}