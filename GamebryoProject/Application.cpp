//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        13/01/2012
// \brief		User derived custom application.
//				Add game/simulation code here.
//
//////////////////////////////////////////////////////////////////////////
#include "Application.h"
#include <NiMain.h>
#include <NiDX9Renderer.h>
#include "AppBase/Window.h"
#include "AppBase/Utilities.h"
#include "GameStates/GameStateManager.h"
#include "GameStates/SplashState.h"
#include "Input/Input.h"
#include "Scripting/LuaVM.h"
#include "Time.h"
#include <efdPhysX\PhysXSDKManager.h>

#include <NiShadowManager.h>

//////////////////////////////////////////////////////////////////////////

const unsigned int DEFAULT_SCREEN_WIDTH = 1280;
const unsigned int DEFAULT_SCREEN_HEIGHT = 720;
const char* DEFAULT_APP_NAME = "YOUR PROJECT NAME HERE";

//////////////////////////////////////////////////////////////////////////
Application::Application()
	: m_pWindow(0)
{
}

//////////////////////////////////////////////////////////////////////////
Application::~Application()
{

}

//////////////////////////////////////////////////////////////////////////
bool Application::OnCreate(const char* a_szCommandLine)
{
	// seed random
	NiSrand((unsigned int)time(0));

	// initialize VM and load settings script
	m_pLuaVM = NiNew LuaVM();
	m_pLuaVM->Initialise();
	
	if (m_pLuaVM->RunScript("Scripts/Startup.lua") != 0)
	{
		char acBuffer[512];
		sprintf(acBuffer,"%s\n", m_pLuaVM-GetLastError());
		OutputDebugStringA(acBuffer);
	}

	/////////////////////////////////////////////////////////////////////////
	//PHYSX INITIALISATION
	
	m_pkPhysXManager = efdPhysX::PhysXSDKManager::GetManager();
	if(!m_pkPhysXManager->Initialize())
	{
		EE_ASSERT("PHYSX FAILED TO INITIALISE");
	}

	m_pkPhysXManager->m_pPhysXSDK->setParameter(NX_SKIN_WIDTH, 0.01f);
    m_pkPhysXManager->m_pPhysXSDK->setParameter(NX_BOUNCE_THRESHOLD, -0.75f);

	//Init Shadow Manager
	NiShadowManager::Initialize();

	/////////////////////////////////////////////////////////////////////////

	// initialize window and renderer
	SetupRenderWindow();

	// start timer 
	m_pTime = NiNew GlobalTime();

	// start game states
	m_pStateManager = NiNew GameStateManager();
	m_pStateManager->PushState( NiNew SplashState(0) );

	//m_pStateManager->PushState( NiNew InGameState() );
	Utility::ConsoleShow(true);

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Application::OnUpdate()
{
	// update the window, closing our application if the window was destroyed
	if (m_pWindow->Tick() == false)
	{
		return false;
	}

	// if the window isn't active (ALT-TAB'd) then sleep it so it doesn't use so much CPU time
	if (m_pWindow->IsActive() == false)
		Sleep(16);

	// update any active input
	Input::UpdateDevices();

	// update timing
	m_pTime->Tick();

	// update states
	m_pStateManager->UpdateStates();	

	return m_pStateManager->HasStates();
}

//////////////////////////////////////////////////////////////////////////
void Application::OnDestroy()
{
	NiDelete m_pStateManager;
	NiDelete m_pTime;
	NiDelete m_pLuaVM;

	Input::Destroy();

	NiShaderFactory::ReleaseAllShaders();
	NiShaderFactory::UnregisterAllLibraries();
	NiShadowManager::Shutdown();

	m_spRenderer = nullptr;
	NiDelete m_pWindow;

	m_pkPhysXManager->Shutdown();
}

//////////////////////////////////////////////////////////////////////////
void Application::OnRender()
{
	m_spRenderer->BeginFrame();

	m_pStateManager->RenderStates(m_spRenderer);

	m_spRenderer->EndUsingRenderTargetGroup();
	m_spRenderer->EndFrame();
	m_spRenderer->DisplayFrame();
}

//////////////////////////////////////////////////////////////////////////
void Application::SetupRenderWindow()
{
	// read settings
	unsigned int uiScreenWidth = DEFAULT_SCREEN_WIDTH;
	unsigned int uiScreenHeight = DEFAULT_SCREEN_HEIGHT;
	bool bFullscreen = false;
	efd::string szAppName = DEFAULT_APP_NAME;

	m_pLuaVM->GetTableVariable("Renderer","width", uiScreenWidth);
	m_pLuaVM->GetTableVariable("Renderer","height", uiScreenHeight);
	m_pLuaVM->GetTableVariable("Renderer","fullscreen", bFullscreen);
	m_pLuaVM->GetTableVariable("Game","name",szAppName);

	// create default window
	m_pWindow = NiNew Window(szAppName.c_str(),uiScreenWidth,uiScreenHeight,!bFullscreen);
	NIVERIFY(m_pWindow);

	// setup initial render settings
	NiImageConverter::SetImageConverter(NiNew NiDevImageConverter);
	NiTexture::SetMipmapByDefault(true);
	NiSourceTexture::SetUseMipmapping(true);
	NiMaterial::SetDefaultWorkingDirectory("Shaders/Generated");
	
	// create renderer
	m_spRenderer = NiDX9Renderer::Create( 
		uiScreenWidth, 
		uiScreenHeight, 
		bFullscreen ? NiDX9Renderer::USE_FULLSCREEN : 0, 
		m_pWindow->GetHandle(), 
		m_pWindow->GetHandle(),
		0,
		NiDX9Renderer::DEVDESC_PURE,
		NiDX9Renderer::FBFMT_UNKNOWN,
		NiDX9Renderer::DSFMT_UNKNOWN,
		NiDX9Renderer::PRESENT_INTERVAL_IMMEDIATE,
		NiDX9Renderer::SWAPEFFECT_DEFAULT,
		NiDX9Renderer::FBMODE_MULTISAMPLES_16,
		1,
		2);
	
	m_spRenderer->SetBackgroundColor(NiColor(0,0,0));

	// setup shaders
	// NOTE: Only need to parse shaders to BUILD the binary shaders.
	// Final version of game handed to others should disable the
	// parsing and should include the NSB binaries, NOT the NSF
	Utility::AddShaderDirectory("Shaders/DX9");
	Utility::ParseShaders("Shaders");
	Utility::LoadShaders("Shaders");
}
