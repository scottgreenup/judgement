//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		Debug mesh to draw diffuse triangles easily.
//
//////////////////////////////////////////////////////////////////////////
#include "DebugTriMesh.h"
#include <NiMeshLib.h>
#include <NiMain.h>
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
DebugTriMesh::DebugTriMesh(unsigned int a_uiMaxTriangles /* = DEFUALT_MAX_TRIANGLES */)
	: m_uiMaxTriangles(a_uiMaxTriangles),
	m_uiCurrentTriCount(0),
	m_pVertexRef(0),
	m_pColourRef(0)
{
	NIASSERT(m_uiMaxTriangles > 0);

	SetPrimitiveType(NiPrimitiveType::PRIMITIVE_TRIANGLES);

	m_avVertices = NiNew NiPoint3[ m_uiMaxTriangles * 3 ];
	m_aoColours = NiNew NiColorA[ m_uiMaxTriangles * 3 ];
	memset(m_avVertices,0,sizeof(NiPoint3) * m_uiMaxTriangles * 3 );
	memset(m_aoColours,0,sizeof(NiColorA) * m_uiMaxTriangles * 3 );

	// create position buffer
	m_pVertexRef = AddStream(NiCommonSemantics::POSITION(),
		0,
		NiDataStreamElement::F_FLOAT32_3,
		m_uiMaxTriangles * 3,
		NiDataStream::ACCESS_GPU_READ | NiDataStream::ACCESS_CPU_WRITE_VOLATILE,
		NiDataStream::USAGE_VERTEX,   
		m_avVertices);

	// create colour buffer
	m_pColourRef = AddStream(NiCommonSemantics::COLOR(),
		0,
		NiDataStreamElement::F_FLOAT32_4,
		m_uiMaxTriangles * 3,
		NiDataStream::ACCESS_GPU_READ | NiDataStream::ACCESS_CPU_WRITE_VOLATILE,
		NiDataStream::USAGE_VERTEX,   
		m_aoColours);

	// set to use vertex colours
	NiVertexColorProperty* pVertexColouring = NiNew NiVertexColorProperty;
	pVertexColouring->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
	pVertexColouring->SetLightingMode(NiVertexColorProperty::LIGHTING_E);
	AttachProperty(pVertexColouring);

	NiAlphaProperty* pAlpha = NiNew NiAlphaProperty;
	pAlpha->SetAlphaBlending(true);
	AttachProperty(pAlpha);

	UpdateProperties();

	NiBound kBound;
	kBound.SetRadius(0);
	kBound.SetCenter(NiPoint3::ZERO);
	SetModelBound(kBound);

	SetName("Debug Tri Mesh");
}

//////////////////////////////////////////////////////////////////////////
DebugTriMesh::~DebugTriMesh()
{
	NiDelete[] m_aoColours;
	NiDelete[] m_avVertices;
}

//////////////////////////////////////////////////////////////////////////
// prepares the mesh for rendering by updating the vertex buffers and adjusting bounds
void DebugTriMesh::PrepareForRender()
{
	if (m_bDirty == true)
	{
		if (m_uiCurrentTriCount > 0)
		{
			m_pVertexRef->SetActiveCount(0,m_uiCurrentTriCount * 3);

			// map current positions
			NiDataStream* pStream = m_pVertexRef->GetDataStream();
			NiPoint3* avVertices = (NiPoint3*)pStream->LockWrite();
			memcpy(avVertices,m_avVertices,sizeof(NiPoint3) * 3 * m_uiCurrentTriCount);
			pStream->UnlockWrite();

			m_pColourRef->SetActiveCount(0,m_uiCurrentTriCount * 3);

			// map current colours
			pStream = m_pColourRef->GetDataStream();
			NiColorA* aoColours = (NiColorA*)pStream->LockWrite();
			memcpy(aoColours,m_aoColours,sizeof(NiColorA) * 3 * m_uiMaxTriangles);
			pStream->UnlockWrite();

			// adjust bounds and primitive count
			UpdateCachedPrimitiveCount();
			AdjustBounds();
		}
		else
		{
			m_pVertexRef->SetActiveCount(0,0);
			m_pColourRef->SetActiveCount(0,0);

			UpdateCachedPrimitiveCount();

			NiBound kBound;
			kBound.SetRadius(0);
			kBound.SetCenter(NiPoint3::ZERO);
			SetModelBound(kBound);
		}
		m_bDirty = false;
	}
}

//////////////////////////////////////////////////////////////////////////
// debug drawing functions
void DebugTriMesh::AddTri(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiPoint3& a_rv2, const NiColorA& a_roColour)
{
	if (m_uiCurrentTriCount < m_uiMaxTriangles)
	{
		m_avVertices[ m_uiCurrentTriCount * 3 + 0 ] = a_rv0;
		m_avVertices[ m_uiCurrentTriCount * 3 + 1 ] = a_rv1;
		m_avVertices[ m_uiCurrentTriCount * 3 + 2 ] = a_rv2;

		m_aoColours[ m_uiCurrentTriCount * 3 + 0 ] = a_roColour;
		m_aoColours[ m_uiCurrentTriCount * 3 + 1 ] = a_roColour;
		m_aoColours[ m_uiCurrentTriCount * 3 + 2 ] = a_roColour;

		++m_uiCurrentTriCount;
		m_bDirty = true;
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugTriMesh::AddAABB(const NiPoint3& a_rvCenter, const NiPoint3& a_rvExtents,
						   const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	NiPoint3 vVerts[8];
	NiPoint3 vX(a_rvExtents.x, 0, 0);
	NiPoint3 vY(0, a_rvExtents.y, 0);
	NiPoint3 vZ(0, 0, a_rvExtents.z);

	if (a_pmTransform)
	{
		vX = *a_pmTransform * vX;
		vY = *a_pmTransform * vY;
		vZ = *a_pmTransform * vZ;
	}

	// corners
	vVerts[0] = a_rvCenter - vX - vZ - vY;
	vVerts[1] = a_rvCenter - vX + vZ - vY;
	vVerts[2] = a_rvCenter + vX + vZ - vY;
	vVerts[3] = a_rvCenter + vX - vZ - vY;
	vVerts[4] = a_rvCenter - vX - vZ + vY;
	vVerts[5] = a_rvCenter - vX + vZ + vY;
	vVerts[6] = a_rvCenter + vX + vZ + vY;
	vVerts[7] = a_rvCenter + vX - vZ + vY;

	AddTri(vVerts[2], vVerts[1], vVerts[0], a_rvColour);
	AddTri(vVerts[3], vVerts[2], vVerts[0], a_rvColour);
	AddTri(vVerts[5], vVerts[6], vVerts[4], a_rvColour);
	AddTri(vVerts[6], vVerts[7], vVerts[4], a_rvColour);
	AddTri(vVerts[4], vVerts[3], vVerts[0], a_rvColour);
	AddTri(vVerts[7], vVerts[3], vVerts[4], a_rvColour);
	AddTri(vVerts[1], vVerts[2], vVerts[5], a_rvColour);
	AddTri(vVerts[2], vVerts[6], vVerts[5], a_rvColour);
	AddTri(vVerts[0], vVerts[1], vVerts[4], a_rvColour);
	AddTri(vVerts[1], vVerts[5], vVerts[4], a_rvColour);
	AddTri(vVerts[2], vVerts[3], vVerts[7], a_rvColour);
	AddTri(vVerts[6], vVerts[2], vVerts[7], a_rvColour);
}

//////////////////////////////////////////////////////////////////////////
// cylinder is aligned to the Z-axis
void DebugTriMesh::AddCylinder(const NiPoint3& a_rvCenter, float a_fRadius, float a_fHalfLength,
							   unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * 3.14159f) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v0top(0,0,a_fHalfLength);
		NiPoint3 v1top( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, a_fHalfLength );
		NiPoint3 v2top( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, a_fHalfLength );
		NiPoint3 v0bottom(0,0,-a_fHalfLength);
		NiPoint3 v1bottom( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, -a_fHalfLength );
		NiPoint3 v2bottom( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, -a_fHalfLength );

		if (a_pmTransform)
		{
			v0top = *a_pmTransform * v0top;
			v1top = *a_pmTransform * v1top;
			v2top = *a_pmTransform * v2top;
			v0bottom = *a_pmTransform * v0bottom;
			v1bottom = *a_pmTransform * v1bottom;
			v2bottom = *a_pmTransform * v2bottom;
		}

		// lines
		AddTri( v0top + a_rvCenter, v1top + a_rvCenter, v2top + a_rvCenter,a_roColour);
		AddTri( v0bottom + a_rvCenter, v2bottom + a_rvCenter, v1bottom + a_rvCenter,a_roColour);
		AddTri( v2top + a_rvCenter, v1top + a_rvCenter, v1bottom + a_rvCenter,a_roColour);
		AddTri( v1bottom + a_rvCenter, v2bottom + a_rvCenter, v2top + a_rvCenter,a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
// disc / arc / ring are all in the XY plane, with any a_fRotation value being radians around the Z-axis
// all are double sided
void DebugTriMesh::AddDisc(const NiPoint3& a_rvCenter, float a_fRadius,
						   unsigned int a_uiSegments, const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * 3.14159f) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v1outer( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, 0 );
		NiPoint3 v2outer( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, 0 );

		if (a_pmTransform)
		{
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
		}

		AddTri(a_rvCenter, v1outer + a_rvCenter, v2outer + a_rvCenter, a_rvColour);
		AddTri( v2outer + a_rvCenter, v1outer + a_rvCenter, a_rvCenter,a_rvColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugTriMesh::AddRing(const NiPoint3& a_rvCenter, float a_fInnerRadius, float a_fOuterRadius,
						   unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * 3.14159f) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v1outer( sinf( i * fSegmentSize ) * a_fOuterRadius, cosf( i * fSegmentSize ) * a_fOuterRadius, 0 );
		NiPoint3 v2outer( sinf( (i+1) * fSegmentSize ) * a_fOuterRadius, cosf( (i+1) * fSegmentSize ) * a_fOuterRadius, 0 );
		NiPoint3 v1inner( sinf( i * fSegmentSize ) * a_fInnerRadius, cosf( i * fSegmentSize ) * a_fInnerRadius, 0 );
		NiPoint3 v2inner( sinf( (i+1) * fSegmentSize ) * a_fInnerRadius, cosf( (i+1) * fSegmentSize ) * a_fInnerRadius, 0 );

		if (a_pmTransform)
		{
			v1inner = *a_pmTransform * v1inner;
			v2inner = *a_pmTransform * v2inner;
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
		}

		AddTri(v1inner + a_rvCenter, v1outer + a_rvCenter, v2outer + a_rvCenter, a_roColour);
		AddTri(v2outer + a_rvCenter, v2inner + a_rvCenter, v1inner + a_rvCenter, a_roColour);

		AddTri(v2outer + a_rvCenter, v1outer + a_rvCenter, v1inner + a_rvCenter, a_roColour);
		AddTri(v1inner + a_rvCenter, v2inner + a_rvCenter, v2outer + a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugTriMesh::AddArc(const NiPoint3& a_rvCenter, float a_fRotation,
						  float a_fRadius, float a_fHalfAngle, 
						  unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * a_fHalfAngle) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v1outer( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );
		NiPoint3 v2outer( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );

		if (a_pmTransform)
		{
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
		}

		AddTri(a_rvCenter, v1outer + a_rvCenter, v2outer + a_rvCenter, a_roColour);
		AddTri(v2outer + a_rvCenter, v1outer + a_rvCenter, a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugTriMesh::AddArcRing(const NiPoint3& a_rvCenter, float a_fRotation,
							  float a_fInnerRadius, float a_fOuterRadius, float a_fHalfAngle,
							  unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * a_fHalfAngle) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v1outer( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );
		NiPoint3 v2outer( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );
		NiPoint3 v1inner( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation  ) * a_fInnerRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation  ) * a_fInnerRadius, 0 );
		NiPoint3 v2inner( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation  ) * a_fInnerRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation  ) * a_fInnerRadius, 0 );

		if (a_pmTransform)
		{
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
			v1inner = *a_pmTransform * v1inner;
			v2inner = *a_pmTransform * v2inner;
		}

		AddTri(v1inner + a_rvCenter, v1outer + a_rvCenter, v2outer + a_rvCenter, a_roColour);
		AddTri(v2outer + a_rvCenter, v2inner + a_rvCenter, v1inner + a_rvCenter, a_roColour);

		AddTri(v2outer + a_rvCenter, v1outer + a_rvCenter, v1inner + a_rvCenter, a_roColour);
		AddTri(v1inner + a_rvCenter, v2inner + a_rvCenter, v2outer + a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugTriMesh::AdjustBounds()
{
	// compute the axis-aligned box containing the data
	NiPoint3 kMin(m_avVertices[0][0], m_avVertices[0][1], m_avVertices[0][2]);
	NiPoint3 kMax = kMin;
	for (NiUInt32 ui = 1; ui < (m_uiCurrentTriCount*3); ui++)
	{
		if (kMin.x > m_avVertices[ui][0])
			kMin.x = m_avVertices[ui][0];
		if (kMin.y > m_avVertices[ui][1])
			kMin.y = m_avVertices[ui][1];
		if (kMin.z > m_avVertices[ui][2])
			kMin.z = m_avVertices[ui][2];
		if (kMax.x < m_avVertices[ui][0])
			kMax.x = m_avVertices[ui][0];
		if (kMax.y < m_avVertices[ui][1])
			kMax.y = m_avVertices[ui][1];
		if (kMax.z < m_avVertices[ui][2])
			kMax.z = m_avVertices[ui][2];
	}

	// sphere center is the axis-aligned box center
	NiPoint3 kCenter = 0.5f * (kMin + kMax);

	// compute the radius
	float fRadiusSqr = 0.0f;
	for (NiUInt32 ui = 0; ui < (m_uiCurrentTriCount*3); ui++)
	{
		NiPoint3 diff(m_avVertices[ui][0], m_avVertices[ui][1], m_avVertices[ui][2]);
		diff -= kCenter;
		float fLengthSqr = diff * diff;
		if (fLengthSqr > fRadiusSqr)
			fRadiusSqr = fLengthSqr;
	}

	NiBound kBound;
	kBound.SetRadius(NiSqrt(fRadiusSqr));
	kBound.SetCenter(kCenter);
	SetModelBound(kBound);
}