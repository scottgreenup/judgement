//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		Debug mesh to draw diffuse triangles easily.
//
//////////////////////////////////////////////////////////////////////////
#ifndef __DEBUGTRIMESH_H_
#define __DEBUGTRIMESH_H_
//////////////////////////////////////////////////////////////////////////
#include <NiMesh.h>
//////////////////////////////////////////////////////////////////////////
class DebugTriMesh : public NiMesh
{
public:

	enum 
	{
		DEFUALT_MAX_TRIANGLES = 2048,
	};

	DebugTriMesh(unsigned int a_uiMaxTriangles = DEFUALT_MAX_TRIANGLES);
	virtual ~DebugTriMesh();

	unsigned int	GetMaxTriCount() const;

	// returns current number of triangles to draw
	unsigned int	CurrentTriCount() const;

	// resets the current tri count to 0
	void	Reset();

	// prepares the mesh for rendering by updating the vertex buffers and adjusting bounds
	void	PrepareForRender();

	// debug drawing functions
	void	AddTri(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiPoint3& a_rv2, const NiColorA& a_roColour);

	void	AddAABB(const NiPoint3& a_rvCenter, const NiPoint3& a_rvExtents,
					const NiColorA& a_roColour, const NiMatrix3* a_pmTransform = 0);

	// cylinder is aligned to the Z-axis
	void	AddCylinder(const NiPoint3& a_rvCenter, float a_fRadius, float a_fHalfLength,
						unsigned int a_uiSegments, const NiColorA& a_roColour,
						const NiMatrix3* a_pmTransform = 0);

	// disc / arc / ring are all in the XY plane, with any a_fRotation value being radians around the Z-axis
	// all are double sided
	void	AddDisc(const NiPoint3& a_rvCenter, float a_fRadius,
					unsigned int a_uiSegments, const NiColorA& a_roColour, 
					const NiMatrix3* a_pmTransform = 0);

	void	AddRing(const NiPoint3& a_rvCenter, float a_fInnerRadius, float a_fOuterRadius,
					unsigned int a_uiSegments, const NiColorA& a_roColour,
					const NiMatrix3* a_pmTransform = 0);

	void	AddArc(const NiPoint3& a_rvCenter, float a_fRotation,
				   float a_fRadius, float a_fHalfAngle, 
				   unsigned int a_uiSegments, const NiColorA& a_roColour, 
				   const NiMatrix3* a_pmTransform = 0);

	void	AddArcRing(const NiPoint3& a_rvCenter, float a_fRotation,
					   float a_fInnerRadius, float a_fOuterRadius, float a_fHalfAngle,
					   unsigned int a_uiSegments, const NiColorA& a_roColour,
					   const NiMatrix3* a_pmTransform = 0);
	
private:

	void	AdjustBounds();

	unsigned int	m_uiMaxTriangles;
	unsigned int	m_uiCurrentTriCount;

	NiPoint3*		m_avVertices;
	NiColorA*		m_aoColours;

	NiDataStreamRef*	m_pVertexRef;
	NiDataStreamRef*	m_pColourRef;

	bool			m_bDirty;
};

// Inline Functions

//////////////////////////////////////////////////////////////////////////
inline unsigned int DebugTriMesh::GetMaxTriCount() const
{
	return m_uiMaxTriangles;
}

//////////////////////////////////////////////////////////////////////////
inline unsigned int DebugTriMesh::CurrentTriCount() const
{
	return m_uiCurrentTriCount;
}

//////////////////////////////////////////////////////////////////////////
inline void DebugTriMesh::Reset()
{
	if (m_uiCurrentTriCount != 0)
	{
		m_bDirty = true;
		m_uiCurrentTriCount = 0;
	}
}
//////////////////////////////////////////////////////////////////////////
#endif // __DEBUGTRIMESH_H_
//////////////////////////////////////////////////////////////////////////