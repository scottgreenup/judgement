//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		Utility functions.
//
//////////////////////////////////////////////////////////////////////////
#include "Utilities.h"
#include "DebugTriMesh.h"
#include "DebugLineMesh.h"

#include <NiNode.h>

#include <NiShaderLibraryDesc.h>

#include <NiD3DDefines.h>
#include <NiD3DShaderProgramFactory.h>
#include <NiD3DRendererHeaders.h>    

#include <NSBShaderLib.h>
#include <NSFParserLib.h>
#include <NiBinaryShader.h>

#include <stdlib.h>
#include <io.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////////

NiNode* Utility::sm_pDebugScene = 0;
DebugTriMesh* Utility::sm_pDebugTris = 0;
DebugLineMesh* Utility::sm_pDebugLines = 0;

//////////////////////////////////////////////////////////////////////////
// CAMERAS
void Utility::FreeCameraMovement(float a_fDeltaTime, float a_fMoveSpeed, NiCamera* a_pCamera,
								 NiInputMouse* a_pMouse, NiInputKeyboard* a_pKeyboard, const NiPoint3& a_rvUp /* = NiPoint3::UNIT_Z */)
{
	int iDeltaX = 0, iDeltaY = 0, iDeltaZ = 0;
	a_pMouse->GetPositionDelta(iDeltaX,iDeltaY,iDeltaZ);

	// Get the camera's forward, right, up, and location vectors
	NiPoint3 vForward = a_pCamera->GetWorldDirection();
	NiPoint3 vRight = a_pCamera->GetWorldRightVector();
	NiPoint3 vUp = a_pCamera->GetWorldUpVector();
	NiPoint3 vTranslation = a_pCamera->GetTranslate();

	// Translate camera
	float fSpeed = a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_LSHIFT) ? a_fMoveSpeed * 2 : a_fMoveSpeed;	

	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_W))
	{
		vTranslation += vForward * a_fDeltaTime * fSpeed;
	}
	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_S))
	{
		vTranslation -= vForward * a_fDeltaTime * fSpeed;
	}
	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_D))
	{
		vTranslation += vRight * a_fDeltaTime * fSpeed;
	}
	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_A))
	{
		vTranslation -= vRight * a_fDeltaTime * fSpeed;
	}
	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_Q))
	{
		vTranslation += vUp * a_fDeltaTime * fSpeed;
	}
	if (a_pKeyboard->KeyIsDown(NiInputKeyboard::KEY_E))
	{
		vTranslation -= vUp * a_fDeltaTime * fSpeed;
	}

	if (iDeltaZ != 0)
	{
		float fScroll = iDeltaZ / 10.f;
		vTranslation += vForward * a_fDeltaTime * fSpeed * fScroll;
	}

	a_pCamera->SetTranslate(vTranslation);
	a_pCamera->Update(0);

	// check for camera rotation
	if (a_pMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT))
	{
		NiMatrix3 mMat;

		// pitch
		if (iDeltaY != 0)
		{
			NiMatrix3 mMat;
			mMat.MakeRotation(iDeltaY / 150.f,vRight);
			a_pCamera->SetRotate( mMat * a_pCamera->GetRotate() );
			a_pCamera->Update(0);
		}

		// yaw
		if (iDeltaX != 0)
		{
			NiMatrix3 mMat;
			mMat.MakeRotation(iDeltaX / 150.f,a_rvUp);
			a_pCamera->SetRotate(  mMat * a_pCamera->GetRotate() );
			a_pCamera->Update(0);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// SHADERS
unsigned int Utility::ShaderError( const char* a_szError, NiShaderError a_eErrorCode, bool a_bRecoverable )
{
	printf("Shader Error:\n");
	printf(a_szError);
	NiOutputDebugString("Shader Error:\n");
	NiOutputDebugString(a_szError);
	return 0;
}

//////////////////////////////////////////////////////////////////////////
void Utility::AddShaderDirectory(const char* a_szSubDirectory)
{
	NiShaderFactory::AddShaderProgramFileDirectory(a_szSubDirectory);
}

//////////////////////////////////////////////////////////////////////////
bool Utility::ParseShaders(const char* a_szRootDirectory)
{
	NiShaderFactory::RegisterErrorCallback(ShaderError);

	NiShaderFactory::RegisterRunParserCallback(RunShaderParser);

	unsigned int uiCount = NiShaderFactory::LoadAndRunParserLibrary(0,a_szRootDirectory, false);
	char acTemp[256];
	NiSprintf(acTemp, 256, "Parsed %d shaders with NSFShaderParser!\n", uiCount);
	NiOutputDebugString(acTemp);

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Utility::LoadShaders(const char* a_szRootDirectory)
{
	NiShaderFactory::RegisterClassCreationCallback(LoadShaderLibrary);

	unsigned int uiCount = 1;
	const char* aszDirectories[1];
	aszDirectories[0] = a_szRootDirectory;

	if (NiShaderFactory::LoadAndRegisterShaderLibrary(0, uiCount, aszDirectories, false) == false)
	{
		NiMessageBox("Failed to load shader library!", "ERROR");
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Utility::LoadShaderLibrary(const char* a_szLibFile, NiRenderer* a_pRenderer,
								int a_iDirectoryCount, const char* a_aszDirectories[],
								bool a_bRecurseSubFolders, NiShaderLibrary** a_ppLibrary)
{
	*a_ppLibrary = 0;
	return NSBShaderLib_LoadShaderLibrary(a_pRenderer,
										  a_iDirectoryCount, a_aszDirectories, 
										  a_bRecurseSubFolders, a_ppLibrary);
}

//////////////////////////////////////////////////////////////////////////
unsigned int Utility::RunShaderParser(const char* a_szLibFile, NiRenderer* a_pRenderer, 
									  const char* a_szDirectory, bool a_bRecurseSubFolders)
{
	return NSFParserLib_RunShaderParser(a_szDirectory, a_bRecurseSubFolders);
}

//////////////////////////////////////////////////////////////////////////
// DEBUG RENDER
NiNode* Utility::SetupDebugScene()
{
	if (sm_pDebugScene == 0)
	{
		sm_pDebugScene = NiNew NiNode;
		sm_pDebugTris = NiNew DebugTriMesh;
		sm_pDebugLines = NiNew DebugLineMesh;

		sm_pDebugScene->AttachChild(sm_pDebugLines);
		sm_pDebugScene->AttachChild(sm_pDebugTris);
		
		sm_pDebugScene->Update(0.0f);
		sm_pDebugScene->UpdateProperties();
		sm_pDebugScene->UpdateEffects();
	}

	 return sm_pDebugScene;
}

//////////////////////////////////////////////////////////////////////////
void Utility::DestroyDebugScene()
{
	NiDelete sm_pDebugScene;
	sm_pDebugScene = nullptr;
	sm_pDebugTris = nullptr;
	sm_pDebugLines = nullptr;
}

//////////////////////////////////////////////////////////////////////////
void Utility::ClearDebugScene()
{
	if (sm_pDebugLines != 0)
		sm_pDebugLines->Reset();
	if (sm_pDebugTris != 0)
		sm_pDebugTris->Reset();
}

//////////////////////////////////////////////////////////////////////////
void Utility::PreRenderDebugScene()
{
	if (sm_pDebugLines != 0)
		sm_pDebugLines->PrepareForRender();
	if (sm_pDebugTris != 0)
		sm_pDebugTris->PrepareForRender();
}

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleShow(bool a_bShow)
{
	if (a_bShow)
	{
		AllocConsole();

		HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
		int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
		FILE* hf_out = _fdopen(hCrt, "w");
		setvbuf(hf_out, NULL, _IONBF, 1);
		*stdout = *hf_out;

		HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
		hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
		FILE* hf_in = _fdopen(hCrt, "r");
		setvbuf(hf_in, NULL, _IONBF, 128);
		*stdin = *hf_in;

		SetConsoleTitle("Debug Console");

		//move the window to the upper left corner
		HWND hwnd = GetConsoleWindow();
		RECT rect;
		GetWindowRect(hwnd, &rect);
		MoveWindow(hwnd, 0, 0, rect.right - rect.left, rect.bottom - rect.top, TRUE);

		ConsoleClear();
	}
	else
	{
		FreeConsole();
	}
}

#include <conio.h>

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleClear()
{
	system("cls");
}

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleMessage(const char* a_szText, ...)
{
	NI_UNUSED_ARG(a_szText);

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	va_list argptr;
	va_start(argptr, a_szText);
	vprintf(a_szText, argptr);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleWarning(const char* a_szText, ...)
{
	NI_UNUSED_ARG(a_szText);

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	va_list argptr;
	va_start(argptr, a_szText);
	vprintf(a_szText, argptr);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleError(const char* a_szText, ...)
{
	NI_UNUSED_ARG(a_szText);

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
	va_list argptr;
	va_start(argptr, a_szText);
	vprintf(a_szText, argptr);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}

//////////////////////////////////////////////////////////////////////////
void Utility::ConsoleRead(char a_acBuffer[])
{
	NI_UNUSED_ARG(a_acBuffer);

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

	printf("\n>>");

	a_acBuffer[0] = '\0';
	int iChar = _getch();
	int iPos  = 0;
	while (iChar != '\r')
	{
		switch (iChar)
		{
		case '\b':
			if (iPos > 0)
			{
				WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),&iChar,1,0,0);		// move cursor backwards one
				WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE)," ",1,0,0);		// writes a empty space over the next character
				WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),&iChar,1,0,0);		// move cursor backwards again
				--iPos;
			}
			break;
		default:
			WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),&iChar,1,0,0);
			a_acBuffer[iPos++] = (char)iChar;
			break;
		}

		iChar = _getch();
	}
	a_acBuffer[iPos] = '\0';
	printf("\n");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}