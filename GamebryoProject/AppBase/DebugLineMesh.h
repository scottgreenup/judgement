//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		Debug line list mesh to draw diffuse lines easily.
//
//////////////////////////////////////////////////////////////////////////
#ifndef __DEBUGLINEMESH_H_
#define __DEBUGLINEMESH_H_
//////////////////////////////////////////////////////////////////////////
#include <NiMesh.h>
#include <NiColor.h>
//////////////////////////////////////////////////////////////////////////
class DebugLineMesh : public NiMesh
{
public:

	enum 
	{
		DEFUALT_MAX_LINES = 2048,
	};

	DebugLineMesh(unsigned int a_uiMaxLines = DEFUALT_MAX_LINES);
	virtual ~DebugLineMesh();

	unsigned int	GetMaxLineCount() const;

	// returns current number of lines to draw
	unsigned int	CurrentLineCount() const;

	// resets the current tri count to 0
	void	Reset();

	// prepares the mesh for rendering by updating the vertex buffers and adjusting bounds
	void	PrepareForRender();

	// debug drawing functions
	void	AddLine(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiColorA& a_roColour);
	void	AddLine(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiColorA& a_roColour0, const NiColorA& a_roColour1);

	void	AddTransform(const NiTransform& a_roTransform);
	void	AddMatrix(const NiMatrix3& a_rmMatrix, const NiPoint3& a_rvCenter = NiPoint3::ZERO, float a_fScale = 1);

	void	AddGrid(const NiPoint3& a_rvCenter, float a_fCellSize, unsigned int a_uiCellExtents,
					const NiColorA& a_roColour = NiColorA(0.25f,0.25f,0.25f,1), const NiColorA& a_roHighlight = NiColorA::BLACK,
					const NiMatrix3* a_pmTransform = 0);

	void	AddAABB(const NiPoint3& a_rvCenter, const NiPoint3& a_rvExtents,
					const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform = 0);

	// cylinder is aligned to the Z-axis
	void	AddCylinder(const NiPoint3& a_rvCenter, float a_fRadius, float a_fHalfLength,
						unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform = 0);

	// arc / ring are all in the XY plane, with any a_fRotation value being radians around the Z-axis
	// all are double sided
	void	AddRing(const NiPoint3& a_rvCenter, float a_fRadius,
					unsigned int a_uiSegments, const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform = 0);

	void	AddArc(const NiPoint3& a_rvCenter, float a_fRotation,
				   float a_fRadius, float a_fHalfAngle, 
				   unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform = 0);

	void	AddArcRing(const NiPoint3& a_rvCenter, float a_fRotation,
					   float a_fInnerRadius, float a_fOuterRadius, float a_fHalfAngle,
					   unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform = 0);

private:

	void		AdjustBounds();
	
	unsigned int	m_uiMaxLineCount;
	unsigned int	m_uiCurrentLineCount;

	NiPoint3*		m_avVertices;
	NiColorA*		m_aoColours;

	NiDataStreamRef*	m_pVertexRef;
	NiDataStreamRef*	m_pColourRef;

	bool			m_bDirty;
};

// Inline Functions

//////////////////////////////////////////////////////////////////////////
inline unsigned int DebugLineMesh::GetMaxLineCount() const
{
	return m_uiMaxLineCount;
}

//////////////////////////////////////////////////////////////////////////
inline unsigned int DebugLineMesh::CurrentLineCount() const
{
	return m_uiCurrentLineCount;
}

//////////////////////////////////////////////////////////////////////////
inline void DebugLineMesh::Reset()
{
	if (m_uiCurrentLineCount != 0)
	{
		m_bDirty = true;
		m_uiCurrentLineCount = 0;
	}
}

//////////////////////////////////////////////////////////////////////////
#endif // __DEBUGLINEMESH_H_
//////////////////////////////////////////////////////////////////////////