//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		Utility functions.
//
//////////////////////////////////////////////////////////////////////////
#ifndef __UTILITIES_H_
#define __UTILITIES_H_
//////////////////////////////////////////////////////////////////////////
#include <NiCamera.h>
#include <NiShader.h>
#include <NiShaderFactory.h>
#include <NiShaderLibrary.h>
#include <NiInputMouse.h>
#include <NiInputKeyboard.h>

//////////////////////////////////////////////////////////////////////////
class DebugTriMesh;
class DebugLineMesh;

//////////////////////////////////////////////////////////////////////////
class Utility
{
public:

	// CAMERA
	// Uses keyboard and mouse controls to move a camera
	static void	FreeCameraMovement(float a_fDeltaTime, float a_fMoveSpeed, NiCamera* a_pCamera,
								   NiInputMouse* a_pMouse, NiInputKeyboard* a_pKeyboard, const NiPoint3& a_rvUp = NiPoint3::UNIT_Z);

	// SHADERS
	// parses the shader folders
	static void		AddShaderDirectory(const char* a_szSubDirectory);
	static bool		ParseShaders(const char* a_szRootDirectory);
	static bool		LoadShaders(const char* a_szRootDirectory);

	// DEBUG RENDER
	// creates a DebugTriMesh and DebugLineMesh and attaches them to an NiNode
	static NiNode*	SetupDebugScene();
	static void		DestroyDebugScene();

	// recommended to call clear at the start of each update
	static void		ClearDebugScene();

	// call just before rendering the debug scene to refresh the buffers
	static void		PreRenderDebugScene();

	// debug scene accessors
	static NiNode*			GetDebugScene();
	static DebugTriMesh*	GetDebugTriMesh();
	static DebugLineMesh*	GetDebugLineMesh();

	// CONSOLE
	static void ConsoleShow(bool a_bShow);
	static void ConsoleClear();
	static void ConsoleMessage(const char* a_szText, ...);
	static void ConsoleWarning(const char* a_szText, ...);
	static void ConsoleError(const char* a_szText, ...);
	static void	ConsoleRead(char a_acBuffer[]);

protected:

	static bool LoadShaderLibrary(const char* a_szLibFile, NiRenderer* a_pRenderer,
								  int a_iDirectoryCount, const char* a_aszDirectories[],
								  bool a_bRecurseSubFolders, NiShaderLibrary** a_ppLibrary);

	static unsigned int RunShaderParser(const char* a_szLibFile, NiRenderer* a_pRenderer, 
										const char* a_szDirectory, bool a_bRecurseSubFolders);

	static unsigned int ShaderError( const char* a_szError, NiShaderError a_eErrorCode, bool a_bRecoverable );

private:

	static NiNode*			sm_pDebugScene;
	static DebugTriMesh*	sm_pDebugTris;
	static DebugLineMesh*	sm_pDebugLines;

};

// Inline Functions

//////////////////////////////////////////////////////////////////////////
inline NiNode* Utility::GetDebugScene()
{
	return sm_pDebugScene;
}

//////////////////////////////////////////////////////////////////////////
inline DebugTriMesh* Utility::GetDebugTriMesh()
{
	return sm_pDebugTris;
}

//////////////////////////////////////////////////////////////////////////
inline DebugLineMesh* Utility::GetDebugLineMesh()
{
	return sm_pDebugLines;
}

//////////////////////////////////////////////////////////////////////////
#endif // __UTILITIES_H_
//////////////////////////////////////////////////////////////////////////