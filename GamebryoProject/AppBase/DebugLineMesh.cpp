//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        16/01/2012
// \brief		
//
//////////////////////////////////////////////////////////////////////////
#include "DebugLineMesh.h"
#include <NiMeshLib.h>
#include <NiMain.h>
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
DebugLineMesh::DebugLineMesh(unsigned int a_uiMaxLines /* = DEFUALT_MAX_LINES */)
	: m_uiMaxLineCount(a_uiMaxLines),
	m_uiCurrentLineCount(0),
	m_pVertexRef(0),
	m_pColourRef(0),
	m_bDirty(false)
{
	NIASSERT(m_uiMaxLineCount > 0);

	SetPrimitiveType(NiPrimitiveType::PRIMITIVE_LINES);

	m_avVertices = NiNew NiPoint3[ m_uiMaxLineCount * 2 ];
	m_aoColours = NiNew NiColorA[ m_uiMaxLineCount * 2 ];
	memset(m_avVertices,0,sizeof(NiPoint3) * m_uiMaxLineCount * 2);
	memset(m_aoColours,0,sizeof(NiColorA) * m_uiMaxLineCount * 2);

	// create position buffer
	m_pVertexRef = AddStream(NiCommonSemantics::POSITION(),
		0,
		NiDataStreamElement::F_FLOAT32_3,
		m_uiMaxLineCount * 2,
		NiDataStream::ACCESS_GPU_READ | NiDataStream::ACCESS_CPU_WRITE_VOLATILE,
		NiDataStream::USAGE_VERTEX,   
		m_avVertices);

	// create colour buffer
	m_pColourRef = AddStream(NiCommonSemantics::COLOR(),
		0,
		NiDataStreamElement::F_FLOAT32_4,
		m_uiMaxLineCount * 2,
		NiDataStream::ACCESS_GPU_READ | NiDataStream::ACCESS_CPU_WRITE_VOLATILE,
		NiDataStream::USAGE_VERTEX,   
		m_aoColours);

	// set to use vertex colours
	NiVertexColorProperty* oProperty = NiNew NiVertexColorProperty;
	oProperty->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
	oProperty->SetLightingMode(NiVertexColorProperty::LIGHTING_E);
	AttachProperty(oProperty);

	UpdateProperties();

	NiBound kBound;
	kBound.SetRadius(0);
	kBound.SetCenter(NiPoint3::ZERO);
	SetModelBound(kBound);

	SetName("Debug Line Mesh");
}

//////////////////////////////////////////////////////////////////////////
DebugLineMesh::~DebugLineMesh()
{
	NiDelete[] m_aoColours;
	NiDelete[] m_avVertices;
}

//////////////////////////////////////////////////////////////////////////
// prepares the mesh for rendering by updating the vertex buffers and adjusting bounds
void DebugLineMesh::PrepareForRender()
{
	if (m_bDirty == true)
	{
		if (m_uiCurrentLineCount > 0)
		{
			m_pVertexRef->SetActiveCount(0,m_uiCurrentLineCount * 2);

			// map current positions
			NiDataStream* pStream = m_pVertexRef->GetDataStream();
			NiPoint3* avVertices = (NiPoint3*)pStream->LockWrite();
			memcpy(avVertices,m_avVertices,sizeof(NiPoint3) * 2 * m_uiCurrentLineCount);
			pStream->UnlockWrite();

			m_pColourRef->SetActiveCount(0,m_uiCurrentLineCount * 2);

			// map current colours
			pStream = m_pColourRef->GetDataStream();
			NiColorA* aoColours = (NiColorA*)pStream->LockWrite();
			memcpy(aoColours,m_aoColours,sizeof(NiColorA) * 2 * m_uiCurrentLineCount);
			pStream->UnlockWrite();

			UpdateCachedPrimitiveCount();
			AdjustBounds();
		}
		else
		{
			m_pVertexRef->SetActiveCount(0,0);
			m_pColourRef->SetActiveCount(0,0);

			UpdateCachedPrimitiveCount();

			NiBound kBound;
			kBound.SetRadius(0);
			kBound.SetCenter(NiPoint3::ZERO);
			SetModelBound(kBound);
		}
		m_bDirty = false;
	}
}

//////////////////////////////////////////////////////////////////////////
// debug drawing functions
void DebugLineMesh::AddLine(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiColorA& a_roColour)
{
	if (m_uiCurrentLineCount < m_uiMaxLineCount)
	{
		m_avVertices[ m_uiCurrentLineCount * 2 + 0 ] = a_rv0;
		m_avVertices[ m_uiCurrentLineCount * 2 + 1 ] = a_rv1;

		m_aoColours[ m_uiCurrentLineCount * 2 + 0 ] = a_roColour;
		m_aoColours[ m_uiCurrentLineCount * 2 + 1 ] = a_roColour;

		++m_uiCurrentLineCount;
		m_bDirty = true;
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddLine(const NiPoint3& a_rv0, const NiPoint3& a_rv1, const NiColorA& a_roColour0, const NiColorA& a_roColour1)
{
	if (m_uiCurrentLineCount < m_uiMaxLineCount)
	{
		m_avVertices[ m_uiCurrentLineCount * 2 + 0 ] = a_rv0;
		m_avVertices[ m_uiCurrentLineCount * 2 + 1 ] = a_rv1;

		m_aoColours[ m_uiCurrentLineCount * 2 + 0 ] = a_roColour0;
		m_aoColours[ m_uiCurrentLineCount * 2 + 1 ] = a_roColour1;

		++m_uiCurrentLineCount;
		m_bDirty = true;
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddTransform(const NiTransform& a_roTransform)
{
	NiPoint3 vXAxis, vYAxis, vZAxis;
	a_roTransform.m_Rotate.GetCol(0,vXAxis);
	a_roTransform.m_Rotate.GetCol(1,vYAxis);
	a_roTransform.m_Rotate.GetCol(2,vZAxis);

	AddLine(a_roTransform.m_Translate,a_roTransform.m_Translate + vXAxis * a_roTransform.m_fScale,NiColorA(1,0,0,1));
	AddLine(a_roTransform.m_Translate,a_roTransform.m_Translate + vYAxis * a_roTransform.m_fScale,NiColorA(0,1,0,1));
	AddLine(a_roTransform.m_Translate,a_roTransform.m_Translate + vZAxis * a_roTransform.m_fScale,NiColorA(0,0,1,1));
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddMatrix(const NiMatrix3& a_rmMatrix, const NiPoint3& a_rvCenter /* = NiPoint3::ZERO */, float a_fScale /* = 1 */)
{
	NiPoint3 vXAxis, vYAxis, vZAxis;
	a_rmMatrix.GetCol(0,vXAxis);
	a_rmMatrix.GetCol(1,vYAxis);
	a_rmMatrix.GetCol(2,vZAxis);

	AddLine(a_rvCenter,a_rvCenter + vXAxis * a_fScale,NiColorA(1,0,0,1));
	AddLine(a_rvCenter,a_rvCenter + vYAxis * a_fScale,NiColorA(0,1,0,1));
	AddLine(a_rvCenter,a_rvCenter + vZAxis * a_fScale,NiColorA(0,0,1,1));
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddGrid(const NiPoint3& a_rvCenter, float a_fCellSize, unsigned int a_uiCellExtents,
							const NiColorA& a_roColour /* = NiColorA(0.25f,0.25f,0.25f,1) */, const NiColorA& a_roHighlight /* = NiColorA::BLACK */,
							const NiMatrix3* a_pmTransform /* = 0 */)
{
	unsigned int uiSize = a_uiCellExtents * 2;

	float fStart = a_fCellSize * a_uiCellExtents;

	for ( unsigned int i = 0 ; i < (uiSize + 1) ; ++i )
	{
		NiColorA vColour = a_roColour;

		if (i == a_uiCellExtents)
			vColour = a_roHighlight;

		AddLine(NiPoint3(-fStart + i * a_fCellSize,fStart,0), NiPoint3(-fStart + i * a_fCellSize, -fStart,0), vColour);
		AddLine(NiPoint3(fStart, -fStart + i * a_fCellSize,0), NiPoint3(-fStart, -fStart + i * a_fCellSize,0), vColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddAABB(const NiPoint3& a_rvCenter, const NiPoint3& a_rvExtents,
							const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	NiPoint3 vVerts[8];
	NiPoint3 vX(a_rvExtents.x, 0, 0);
	NiPoint3 vY(0, a_rvExtents.y, 0);
	NiPoint3 vZ(0, 0, a_rvExtents.z);

	if (a_pmTransform)
	{
		vX = *a_pmTransform * vX;
		vY = *a_pmTransform * vY;
		vZ = *a_pmTransform * vZ;
	}

	// corners
	vVerts[0] = a_rvCenter - vX - vZ - vY;
	vVerts[1] = a_rvCenter - vX + vZ - vY;
	vVerts[2] = a_rvCenter + vX + vZ - vY;
	vVerts[3] = a_rvCenter + vX - vZ - vY;
	vVerts[4] = a_rvCenter - vX - vZ + vY;
	vVerts[5] = a_rvCenter - vX + vZ + vY;
	vVerts[6] = a_rvCenter + vX + vZ + vY;
	vVerts[7] = a_rvCenter + vX - vZ + vY;

	AddLine(vVerts[0], vVerts[1], a_rvColour);
	AddLine(vVerts[1], vVerts[2], a_rvColour);
	AddLine(vVerts[2], vVerts[3], a_rvColour);
	AddLine(vVerts[3], vVerts[0], a_rvColour);

	AddLine(vVerts[4], vVerts[5], a_rvColour);
	AddLine(vVerts[5], vVerts[6], a_rvColour);
	AddLine(vVerts[6], vVerts[7], a_rvColour);
	AddLine(vVerts[7], vVerts[4], a_rvColour);

	AddLine(vVerts[0], vVerts[4], a_rvColour);
	AddLine(vVerts[1], vVerts[5], a_rvColour);
	AddLine(vVerts[2], vVerts[6], a_rvColour);
	AddLine(vVerts[3], vVerts[7], a_rvColour);
}

//////////////////////////////////////////////////////////////////////////
// cylinder is aligned to the Z-axis
void DebugLineMesh::AddCylinder(const NiPoint3& a_rvCenter, float a_fRadius, float a_fHalfLength,
	unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * 3.14159f) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v0top(0,0,a_fHalfLength);
		NiPoint3 v1top( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, a_fHalfLength );
		NiPoint3 v2top( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, a_fHalfLength );
		NiPoint3 v0bottom(0,0,-a_fHalfLength);
		NiPoint3 v1bottom( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, -a_fHalfLength );
		NiPoint3 v2bottom( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, -a_fHalfLength );

		if (a_pmTransform)
		{
			v0top = *a_pmTransform * v0top;
			v1top = *a_pmTransform * v1top;
			v2top = *a_pmTransform * v2top;
			v0bottom = *a_pmTransform * v0bottom;
			v1bottom = *a_pmTransform * v1bottom;
			v2bottom = *a_pmTransform * v2bottom;
		}

		// lines
		AddLine(v1top + a_rvCenter, v2top + a_rvCenter, a_roColour);
		AddLine(v1top + a_rvCenter, v1bottom + a_rvCenter, a_roColour);
		AddLine(v1bottom + a_rvCenter, v2bottom + a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
// arc / ring are all in the XY plane, with any a_fRotation value being radians around the Z-axis
// all are double sided
void DebugLineMesh::AddRing(const NiPoint3& a_rvCenter, float a_fRadius,
	unsigned int a_uiSegments, const NiColorA& a_rvColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	float fSegmentSize = (2 * 3.14159f) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		NiPoint3 v1( sinf( i * fSegmentSize ) * a_fRadius, cosf( i * fSegmentSize ) * a_fRadius, 0 );
		NiPoint3 v2( sinf( (i+1) * fSegmentSize ) * a_fRadius, cosf( (i+1) * fSegmentSize ) * a_fRadius, 0 );

		if (a_pmTransform)
		{
			v1 = *a_pmTransform * v1;
			v2 = *a_pmTransform * v2;
		}

		// lines
		AddLine(v1 + a_rvCenter, v2 + a_rvCenter, a_rvColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddArc(const NiPoint3& a_rvCenter, float a_fRotation,
	float a_fRadius, float a_fHalfAngle, 
	unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	NiPoint3 v1outer( sinf( -a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( -a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );
	NiPoint3 v2outer( sinf( a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );

	if (a_pmTransform)
	{
		v1outer = *a_pmTransform * v1outer;
		v2outer = *a_pmTransform * v2outer;
	}

	AddLine(a_rvCenter, v1outer + a_rvCenter, a_roColour);
	AddLine(a_rvCenter, v2outer + a_rvCenter, a_roColour);

	float fSegmentSize = (2 * a_fHalfAngle) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		v1outer = NiPoint3( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );
		v2outer = NiPoint3( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fRadius, 0 );

		if (a_pmTransform)
		{
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
		}

		AddLine(v1outer + a_rvCenter, v2outer + a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AddArcRing(const NiPoint3& a_rvCenter, float a_fRotation,
	float a_fInnerRadius, float a_fOuterRadius, float a_fHalfAngle,
	unsigned int a_uiSegments, const NiColorA& a_roColour, const NiMatrix3* a_pmTransform /* = 0 */)
{
	NiPoint3 v1inner( sinf( -a_fHalfAngle + a_fRotation ) * a_fInnerRadius, cosf( -a_fHalfAngle + a_fRotation ) * a_fInnerRadius, 0 );
	NiPoint3 v2inner( sinf( a_fHalfAngle + a_fRotation ) * a_fInnerRadius, cosf( a_fHalfAngle + a_fRotation ) * a_fInnerRadius, 0 );
	NiPoint3 v1outer( sinf( -a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( -a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );
	NiPoint3 v2outer( sinf( a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );

	if (a_pmTransform)
	{
		v1inner = *a_pmTransform * v1inner;
		v2inner = *a_pmTransform * v2inner;
		v1outer = *a_pmTransform * v1outer;
		v2outer = *a_pmTransform * v2outer;
	}

	AddLine(v1inner + a_rvCenter, v1outer + a_rvCenter, a_roColour);
	AddLine(v2inner + a_rvCenter, v2outer + a_rvCenter, a_roColour);

	float fSegmentSize = (2 * a_fHalfAngle) / a_uiSegments;

	for ( unsigned int i = 0 ; i < a_uiSegments ; ++i )
	{
		v1inner = NiPoint3( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fInnerRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fInnerRadius, 0 );
		v2inner = NiPoint3( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fInnerRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fInnerRadius, 0 );
		v1outer = NiPoint3( sinf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( i * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );
		v2outer = NiPoint3( sinf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, cosf( (i+1) * fSegmentSize - a_fHalfAngle + a_fRotation ) * a_fOuterRadius, 0 );

		if (a_pmTransform)
		{
			v1inner = *a_pmTransform * v1inner;
			v2inner = *a_pmTransform * v2inner;
			v1outer = *a_pmTransform * v1outer;
			v2outer = *a_pmTransform * v2outer;
		}

		AddLine(v1outer + a_rvCenter, v2outer + a_rvCenter, a_roColour);
		AddLine(v1inner + a_rvCenter, v2inner + a_rvCenter, a_roColour);
	}
}

//////////////////////////////////////////////////////////////////////////
void DebugLineMesh::AdjustBounds()
{
	// compute the axis-aligned box containing the data
	NiPoint3 kMin(m_avVertices[0][0], m_avVertices[0][1], m_avVertices[0][2]);
	NiPoint3 kMax = kMin;
	for (NiUInt32 ui = 1; ui < (m_uiCurrentLineCount*2); ui++)
	{
		if (kMin.x > m_avVertices[ui][0])
			kMin.x = m_avVertices[ui][0];
		if (kMin.y > m_avVertices[ui][1])
			kMin.y = m_avVertices[ui][1];
		if (kMin.z > m_avVertices[ui][2])
			kMin.z = m_avVertices[ui][2];
		if (kMax.x < m_avVertices[ui][0])
			kMax.x = m_avVertices[ui][0];
		if (kMax.y < m_avVertices[ui][1])
			kMax.y = m_avVertices[ui][1];
		if (kMax.z < m_avVertices[ui][2])
			kMax.z = m_avVertices[ui][2];
	}

	// sphere center is the axis-aligned box center
	NiPoint3 kCenter = 0.5f * (kMin + kMax);

	// compute the radius
	float fRadiusSqr = 0.0f;
	for (NiUInt32 ui = 0; ui < (m_uiCurrentLineCount*2); ui++)
	{
		NiPoint3 diff(m_avVertices[ui][0], m_avVertices[ui][1], m_avVertices[ui][2]);
		diff -= kCenter;
		float fLengthSqr = diff * diff;
		if (fLengthSqr > fRadiusSqr)
			fRadiusSqr = fLengthSqr;
	}

	NiBound kBound;
	kBound.SetRadius(NiSqrt(fRadiusSqr));
	kBound.SetCenter(kCenter);
	SetModelBound(kBound);
}