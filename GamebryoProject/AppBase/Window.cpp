//////////////////////////////////////////////////////////////////////////
//
// \author      Conan H. Bourke
// \date        13/01/2012
// \brief		Simple Win32 window wrapper.
//
//////////////////////////////////////////////////////////////////////////
#include "Window.h"
//////////////////////////////////////////////////////////////////////////

static LRESULT CALLBACK WindowProc(HWND Handle, UINT msg, WPARAM wParam, LPARAM lParam);

ATOM Window::sm_oWindowclass = 0;

//////////////////////////////////////////////////////////////////////////
Window::Window(const char* a_szName, int a_iWidth /* = 640 */, int a_iHeight /* = 480 */, bool a_bHasBorder /* = true */)
: m_oHwnd(0),
m_iWidth(a_iWidth),
m_iHeight(a_iHeight)
{
	RECT oScreenRect;
	RECT oWndRect = { 0, 0, m_iWidth, m_iHeight };

	// adjust window size so the display area matches width/height not including menu bar
	AdjustWindowRect(&oWndRect, WS_OVERLAPPEDWINDOW, FALSE);

	// the actual size of the window, including menu
	int iWndWidth = (oWndRect.right - oWndRect.left);
	int iWndHeight = (oWndRect.bottom - oWndRect.top);

	if (sm_oWindowclass == 0)
	{
		RegisterWindowClass();
	}

	GetWindowRect(GetDesktopWindow(), &oScreenRect);

	int iX = (oScreenRect.right - oScreenRect.left - iWndWidth) >> 1;
	int iY = (oScreenRect.bottom - oScreenRect.top - iWndHeight) >> 1;

	m_oHwnd = CreateWindow(
		MAKEINTATOM(sm_oWindowclass), a_szName, 
		a_bHasBorder ? WS_OVERLAPPEDWINDOW : WS_POPUP, 
		iX, iY, iWndWidth, iWndHeight, 
		GetDesktopWindow(), 0, GetModuleHandle(0), 0);

	ShowWindow(m_oHwnd, SW_SHOW);
	UpdateWindow(m_oHwnd);
}

//////////////////////////////////////////////////////////////////////////
Window::~Window()
{
	if (m_oHwnd)
	{
		DestroyWindow(m_oHwnd);
		m_oHwnd = 0;
	}
}

//////////////////////////////////////////////////////////////////////////
bool Window::Tick()
{
	MSG	msg;

	while(PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE))
	{
        if (msg.message == WM_QUIT)
            return false;

		if(GetMessage(&msg, 0, 0, 0))
		{
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
void Window::RegisterWindowClass()
{
	WNDCLASSEX	wcx;

	memset(&wcx, 0,	sizeof(wcx));

	wcx.cbSize			= sizeof(wcx);
	wcx.lpfnWndProc		= WindowProc;
	wcx.hInstance		= GetModuleHandle(0);
	wcx.hCursor			= LoadCursor(0, IDC_ARROW);
	wcx.hbrBackground	= (HBRUSH) GetStockObject(BLACK_BRUSH);
	wcx.lpszClassName	= "AIEWindow";

	sm_oWindowclass = RegisterClassEx(&wcx);
}

//////////////////////////////////////////////////////////////////////////
bool Window::IsActive()
{
	return (GetFocus() == m_oHwnd);
}

//////////////////////////////////////////////////////////////////////////
static LRESULT CALLBACK WindowProc(HWND Handle, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_SYSCOMMAND:
		{
			if (wParam == SC_CLOSE)
			{
				PostMessage(Handle,WM_DESTROY,0,0);
			}

			if (wParam == SC_KEYMENU)
			{
				return 0;
			}

			break;
		}
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}
/*	case WM_KEYDOWN:
		{
			if (wParam == VK_ESCAPE)
			{
				PostMessage(Handle,WM_DESTROY,0,0);
			}
			break;
		}*/
	default:	break;
	};

	return DefWindowProc(Handle, msg, wParam, lParam);
}