//////////////////////////////////////////////////////////////////////////
#include "Input.h"
#include <NiDI8InputSystem.h>
#include "../Application.h"
#include "../AppBase/Window.h"
//////////////////////////////////////////////////////////////////////////

Input* Input::sm_pSingleton = nullptr;

//////////////////////////////////////////////////////////////////////////
Input::Input()
{

}

//////////////////////////////////////////////////////////////////////////
Input::~Input()
{

}

//////////////////////////////////////////////////////////////////////////
Input* Input::Create()
{
	NIASSERT(sm_pSingleton == nullptr);
	sm_pSingleton = EE_NEW Input();
	sm_pSingleton->Initialise();
	return sm_pSingleton;
}

//////////////////////////////////////////////////////////////////////////
void Input::Destroy()
{
	if (sm_pSingleton != nullptr)
	{
		sm_pSingleton->Shutdown();
		EE_DELETE sm_pSingleton;
		sm_pSingleton = nullptr;
	}
}

//////////////////////////////////////////////////////////////////////////
void Input::UpdateDevices()
{
	if (sm_pSingleton != nullptr)
	{
		sm_pSingleton->m_spSystem->UpdateAllDevices();
	}
}

//////////////////////////////////////////////////////////////////////////
void Input::Initialise()
{
	// setup input
	NiDI8InputSystem::DI8CreateParams oInputParams(
		GetModuleHandle(0), 
		((Application*)Application::GetSingleton())->GetWindow()->GetHandle());
	oInputParams.SetKeyboardUsage(NiInputSystem::FOREGROUND | NiInputSystem::NONEXCLUSIVE);
	oInputParams.SetMouseUsage(NiInputSystem::FOREGROUND | NiInputSystem::NONEXCLUSIVE);
	oInputParams.SetGamePadCount(0);

	m_spSystem = NiInputSystem::Create(&oInputParams);
	EE_ASSERT(m_spSystem != NULL);

	// grab mouse and keyboard
	m_spMouse = m_spSystem->OpenMouse();
	EE_ASSERT(m_spMouse != NULL);

	m_spKeyboard = m_spSystem->OpenKeyboard();
	EE_ASSERT(m_spKeyboard != NULL);
}

//////////////////////////////////////////////////////////////////////////
void Input::Shutdown()
{
	m_spKeyboard = nullptr;
	m_spMouse = nullptr;
	m_spSystem = nullptr;
}