//////////////////////////////////////////////////////////////////////////
#ifndef __INPUT_H_
#define __INPUT_H_
//////////////////////////////////////////////////////////////////////////
#include <NiInputSystem.h>
#include <NiMemObject.h>

//////////////////////////////////////////////////////////////////////////
class Input : public NiMemObject
{
public:

	static Input*			Create();
	static Input*			GetSingleton()	{	return sm_pSingleton;				}
	static void				Destroy();

	static NiInputMouse*	Mouse()			{	return sm_pSingleton->m_spMouse;	}
	static NiInputKeyboard*	Keyboard()		{	return sm_pSingleton->m_spKeyboard;	}

	static NiInputSystem*	System()		{	return sm_pSingleton->m_spSystem;	}

	static void				UpdateDevices();

private:

	Input();
	~Input();

	void	Initialise();
	void	Shutdown();

	NiInputSystemPtr	m_spSystem;
	NiInputMousePtr		m_spMouse;
	NiInputKeyboardPtr	m_spKeyboard;

	static Input*		sm_pSingleton;
};

//////////////////////////////////////////////////////////////////////////
#endif // __INPUT_H_
//////////////////////////////////////////////////////////////////////////