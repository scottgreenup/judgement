//////////////////////////////////////////////////////////////////////////
#ifndef __LUAVM_H_
#define __LUAVM_H_
//////////////////////////////////////////////////////////////////////////
#include <NiString.h>
#include <NiMemObject.h>
extern "C"
{
	#include "Lua/lua.h"
};
//////////////////////////////////////////////////////////////////////////
typedef int (*LuaFuncPtr)( lua_State* );
//////////////////////////////////////////////////////////////////////////
class LuaVM : public NiMemObject
{
public:

	LuaVM();
	virtual ~LuaVM();

	bool	Initialise();
	void	Shutdown();

	lua_State*	GetLuaState()	{	return m_pLua;	}

	int		RunScript(const char* a_szFilename);
	int		RunCommand(const char* a_szCommand);

	const char*	GetLastError();

	void	RegisterFunction(const char* a_szName, LuaFuncPtr a_fpFunction);

	void	RegisterVariable(const char* a_szName, bool a_bValue);
	void	RegisterVariable(const char* a_szName, int a_iValue);
	void	RegisterVariable(const char* a_szName, unsigned int a_uiValue);
	void	RegisterVariable(const char* a_szName, double a_fValue);
	void	RegisterVariable(const char* a_szName, const char* a_szValue);

	bool	GetVariable(const char* a_szName, bool& a_rbValue);
	bool	GetVariable(const char* a_szName, int& a_riValue);
	bool	GetVariable(const char* a_szName, unsigned int& a_ruiValue);
	bool	GetVariable(const char* a_szName, double& a_rfValue);
	bool	GetVariable(const char* a_szName, efd::string& a_rszValue);

	void	RegisterTable(const char* a_szName);
	void	RegisterTableEntry(const char* a_szTable, const char* a_szName, bool a_bValue);
	void	RegisterTableEntry(const char* a_szTable, const char* a_szName, int a_iValue);
	void	RegisterTableEntry(const char* a_szTable, const char* a_szName, unsigned int a_uiValue);
	void	RegisterTableEntry(const char* a_szTable, const char* a_szName, double a_fValue);
	void	RegisterTableEntry(const char* a_szTable, const char* a_szName, const char* a_szValue);

	bool	GetTableVariable(const char* a_szTable, const char* a_szName, bool& a_rbValue);
	bool	GetTableVariable(const char* a_szTable, const char* a_szName, int& a_riValue);
	bool	GetTableVariable(const char* a_szTable, const char* a_szName, unsigned int& a_ruiValue);
	bool	GetTableVariable(const char* a_szTable, const char* a_szName, double& a_rfValue);
	bool	GetTableVariable(const char* a_szTable, const char* a_szName, efd::string& a_rszValue);

private:

	lua_State*	m_pLua;
};
//////////////////////////////////////////////////////////////////////////
#endif // __LUAVM_H_
//////////////////////////////////////////////////////////////////////////
