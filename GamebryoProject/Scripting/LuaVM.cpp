//////////////////////////////////////////////////////////////////////////
#include "LuaVM.h"
#include <NiDebug.h>
extern "C"
{
#include "Lua/lualib.h"
#include "Lua/lauxlib.h"
};
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
LuaVM::LuaVM()
	: m_pLua(nullptr)
{

}

//////////////////////////////////////////////////////////////////////////
LuaVM::~LuaVM()
{
	Shutdown();
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::Initialise()
{
	NIASSERT(m_pLua == nullptr);

	m_pLua = luaL_newstate();

	if (m_pLua == nullptr)
		return false;

	luaL_openlibs(m_pLua);
	return true;
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::Shutdown()
{
	if (m_pLua != nullptr)
	{
		lua_close(m_pLua);
		m_pLua = nullptr;
	}
}

//////////////////////////////////////////////////////////////////////////
int LuaVM::RunScript(const char* a_szFilename)
{
	return luaL_dofile(m_pLua,a_szFilename);
}

//////////////////////////////////////////////////////////////////////////
int LuaVM::RunCommand(const char* a_szCommand)
{
	return luaL_dostring(m_pLua, a_szCommand);
}

//////////////////////////////////////////////////////////////////////////
const char* LuaVM::GetLastError()
{
	const char* szError = lua_tostring(m_pLua, -1);
	lua_pop(m_pLua, 1);
	return szError;
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterFunction(const char* a_szName, LuaFuncPtr a_fpFunction)
{
	lua_register(m_pLua,a_szName,a_fpFunction);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterVariable(const char* a_szName, bool a_bValue)
{
	lua_pushboolean(m_pLua, (int)a_bValue);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterVariable(const char* a_szName, int a_iValue)
{
	lua_pushinteger(m_pLua, a_iValue);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterVariable(const char* a_szName, unsigned int a_uiValue)
{
	lua_pushinteger(m_pLua, a_uiValue);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterVariable(const char* a_szName, double a_fValue)
{
	lua_pushnumber(m_pLua, a_fValue);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterVariable(const char* a_szName, const char* a_szValue)
{
	lua_pushstring(m_pLua, a_szValue);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTable(const char* a_szName)
{
	lua_newtable(m_pLua);
	lua_setglobal(m_pLua, a_szName);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTableEntry(const char* a_szTable, const char* a_szName, bool a_bValue)
{
	lua_getglobal(m_pLua,a_szTable);
	lua_pushstring(m_pLua,a_szName);
	lua_pushboolean(m_pLua,(int)a_bValue);
	lua_settable(m_pLua,-3);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTableEntry(const char* a_szTable, const char* a_szName, int a_iValue)
{
	lua_getglobal(m_pLua,a_szTable);
	lua_pushstring(m_pLua,a_szName);
	lua_pushinteger(m_pLua,a_iValue);
	lua_settable(m_pLua,-3);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTableEntry(const char* a_szTable, const char* a_szName, unsigned int a_uiValue)
{
	lua_getglobal(m_pLua,a_szTable);
	lua_pushstring(m_pLua,a_szName);
	lua_pushinteger(m_pLua,a_uiValue);
	lua_settable(m_pLua,-3);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTableEntry(const char* a_szTable, const char* a_szName, double a_fValue)
{
	lua_getglobal(m_pLua,a_szTable);
	lua_pushstring(m_pLua,a_szName);
	lua_pushnumber(m_pLua,a_fValue);
	lua_settable(m_pLua,-3);
}

//////////////////////////////////////////////////////////////////////////
void LuaVM::RegisterTableEntry(const char* a_szTable, const char* a_szName, const char* a_szValue)
{
	lua_getglobal(m_pLua,a_szTable);
	lua_pushstring(m_pLua,a_szName);
	lua_pushstring(m_pLua,a_szValue);
	lua_settable(m_pLua,-3);
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetVariable(const char* a_szName, bool& a_rbValue)
{
	lua_getglobal(m_pLua,a_szName);
	if (lua_isboolean(m_pLua,-1) == true)
	{
		a_rbValue = lua_toboolean(m_pLua,-1) != 0;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetVariable(const char* a_szName, int& a_riValue)
{
	lua_getglobal(m_pLua,a_szName);
	if (lua_isnumber(m_pLua,-1) != 0)
	{
		a_riValue = lua_tointeger(m_pLua,-1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetVariable(const char* a_szName, unsigned int& a_ruiValue)
{
	lua_getglobal(m_pLua,a_szName);
	if (lua_isnumber(m_pLua,-1) != 0)
	{
		a_ruiValue = lua_tointeger(m_pLua,-1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetVariable(const char* a_szName, double& a_rfValue)
{
	lua_getglobal(m_pLua,a_szName);
	if (lua_isnumber(m_pLua,-1) != 0)
	{
		a_rfValue = lua_tonumber(m_pLua,-1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetVariable(const char* a_szName, efd::string& a_rszValue)
{
	lua_getglobal(m_pLua,a_szName);
	if (lua_isstring(m_pLua,-1) != 0)
	{
		a_rszValue = lua_tostring(m_pLua,-1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetTableVariable(const char* a_szTable, const char* a_szName, bool& a_rbValue)
{
	lua_getglobal(m_pLua, a_szTable);
	lua_pushstring(m_pLua, a_szName);
	lua_gettable(m_pLua, -2);

	if (lua_isboolean(m_pLua, -1) == true)
	{
		a_rbValue = lua_toboolean(m_pLua, -1) != 0;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetTableVariable(const char* a_szTable, const char* a_szName, int& a_riValue)
{
	lua_getglobal(m_pLua, a_szTable);
	lua_pushstring(m_pLua, a_szName);
	lua_gettable(m_pLua, -2);

	if (lua_isnumber(m_pLua, -1) != 0)
	{
		a_riValue = lua_tointeger(m_pLua, -1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetTableVariable(const char* a_szTable, const char* a_szName, unsigned int& a_ruiValue)
{
	lua_getglobal(m_pLua, a_szTable);
	lua_pushstring(m_pLua, a_szName);
	lua_gettable(m_pLua, -2);

	if (lua_isnumber(m_pLua, -1) != 0)
	{
		a_ruiValue = lua_tointeger(m_pLua, -1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetTableVariable(const char* a_szTable, const char* a_szName, double& a_rfValue)
{
	lua_getglobal(m_pLua, a_szTable);
	lua_pushstring(m_pLua, a_szName);
	lua_gettable(m_pLua, -2);

	if (lua_isnumber(m_pLua, -1) != 0)
	{
		a_rfValue = lua_tonumber(m_pLua, -1);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
bool LuaVM::GetTableVariable(const char* a_szTable, const char* a_szName, efd::string& a_rszValue)
{
	lua_getglobal(m_pLua, a_szTable);
	lua_pushstring(m_pLua, a_szName);
	lua_gettable(m_pLua, -2);

	if (lua_isstring(m_pLua, -1) != 0)
	{
		a_rszValue = lua_tostring(m_pLua, -1);
	}
	return false;
}