//////////////////////////////////////////////////////////////////////////
#ifndef __GAMESTATE_H_
#define __GAMESTATE_H_
//////////////////////////////////////////////////////////////////////////
#include <NiMemObject.h>

class NiRenderer;

//////////////////////////////////////////////////////////////////////////
// A simple Game State.
class GameState : public NiMemObject
{
	friend class GameStateManager;

protected:

	GameState() : m_bSuspended(false)	{}
	virtual ~GameState()				{}

	enum UPDATE_RESULT
	{
		STATE_RUNNING,
		STATE_FINISHED,
	};

	// insertion / removal from state stack
	// occurs when first added to stack or removed
	virtual void			OnInsertion()	{}
	virtual void			OnRemoval()		{}

	// suspension / activation from top of state stack
	// occurs when a state is pushed / popped on top of
	// this state.
	virtual void			OnSuspend()		{}
	virtual void			OnResume()		{}

	// if Update returns FINISHED then the state is popped
	// from the stack
	virtual UPDATE_RESULT	OnUpdate()							{	return STATE_RUNNING;	}


	virtual void			OnRender(NiRenderer* a_pRenderer)	{}
	
private:

	// controlled by GameStateManager
	void	SetSuspended(bool a_bSet)		{	m_bSuspended = a_bSet; }

private:

	bool	m_bSuspended;

};
//////////////////////////////////////////////////////////////////////////
#endif // __GAMESTATE_H_
//////////////////////////////////////////////////////////////////////////
