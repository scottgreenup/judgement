//////////////////////////////////////////////////////////////////////////
#include "GameStateManager.h"
#include "GameState.h"
//////////////////////////////////////////////////////////////////////////

GameStateManager* GameStateManager::sm_pSingleton = nullptr;

//////////////////////////////////////////////////////////////////////////
GameStateManager::GameStateManager()
	: m_pNextState(nullptr)
{
	NIASSERT(sm_pSingleton == nullptr);
	sm_pSingleton = this;
}

//////////////////////////////////////////////////////////////////////////
GameStateManager::~GameStateManager()
{
	while (m_aStates.empty() == false)
	{
		GameState* pState = m_aStates.back();
		EE_DELETE pState;
		m_aStates.pop_back();
	}

	sm_pSingleton = nullptr;
}

//////////////////////////////////////////////////////////////////////////
void GameStateManager::UpdateStates()
{
	// update current if we have states
	if (m_aStates.empty() == false)
	{
		GameState* pState = m_aStates.back();
		if (pState->OnUpdate() == GameState::STATE_FINISHED)
		{
			// destroy current and resume state under it
			m_aStates.pop_back();

			pState->OnRemoval();

			EE_DELETE pState;

			if (m_aStates.empty() == false)
			{
				m_aStates.back()->SetSuspended(false);
				m_aStates.back()->OnResume();
			}
		}
	}

	// suspend current and push next state
	if (m_pNextState != nullptr)
	{
		if (m_aStates.empty() == false)
		{
			m_aStates.back()->OnSuspend();
			m_aStates.back()->SetSuspended(true);
		}

		m_aStates.push_back(m_pNextState);
		m_pNextState = nullptr;

		m_aStates.back()->OnInsertion();
	}
}

//////////////////////////////////////////////////////////////////////////
void GameStateManager::RenderStates(NiRenderer* a_pRenderer)
{
	// render all states from lowest to highest
	auto oIter = m_aStates.begin();
	auto oEnd = m_aStates.end();
	while (oIter != oEnd)
	{
		(*oIter)->OnRender(a_pRenderer);
		++oIter;
	}
}