#define DEBUG_TEST_CODE TRUE


//////////////////////////////////////////////////////////////////////////
#include "InGameState.h"

#include <NiAmbientLight.h>
#include <NiDirectionalLight.h>
#include <NiRenderer.h>
#include <NiMeshCullingProcess.h>

#include "../AppBase/DebugLineMesh.h"
#include "../AppBase/Utilities.h"
#include "../AppBase/Window.h"
#include "../Application.h"
#include "../GameStates/GameStateManager.h"
#include "../GameStates/MenuState.h"
#include "../GameStates/PauseState.h"
#include "../Input/Input.h"
#include "../Time.h"

#include "../User Interface/InGameGUI.h"
#include "../Cameras/FreeLookCamera.h"
#include "../Cameras/ThirdPersonCamera.h"
#include "../Cameras/Camera.h"


#include <NiMatrix3.h>
#include <NiQuaternion.h>

#include <NiAnimation.h>
#include <NiParticle.h>
#include <NiCollision.h>
#include <NiFogProperty.h>
#include <NiAmbientLight.h>
#include <NiDirectionalLight.h>
#include <NiTerrainLib.h>

#include <NiShadowGenerator.h>

struct FindNode
{
	char* m_czName;
	NiLight* pkLight;

	void operator() (NiAVObject* pkObj)
	{
		NiNode* pNode = NiDynamicCast( NiNode, pkObj);
		if(pNode && pkLight)
		{
			if(pNode->GetName().ContainsNoCase(m_czName))
			{
 				pkLight->AttachUnaffectedNode(pNode);
			}
		}
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////
InGameState::InGameState(NiNode* a_pkScene,NiPhysXScene* a_pkPhysXScene, efd::list<NiSpawnPoint> *a_pakSpawnPoints)
	: m_bIsQuiting(false),
	m_spCamera(nullptr),
	m_pkTPCamera(nullptr),
	m_oVisibleEntities(32,32),
	m_spCullingProcess(nullptr)
{ 
	m_spkScene = a_pkScene;
	m_spkPhysXScene = a_pkPhysXScene;

	m_pkPlayer = nullptr;
	m_pkBoss = nullptr;
	m_pkPickupController = nullptr;

	m_pakSpawnPoints = a_pakSpawnPoints;
}

void MyHitFunc(const NxRaycastHit& a_rkHitReport)
{
	NiPoint3 vHitPos;
	NiPhysXTypes::NxVec3ToNiPoint3(a_rkHitReport.worldImpact,vHitPos);
	Utility::GetDebugTriMesh()->AddAABB(vHitPos,NiPoint3(0.5f,0.5f,0.5f),NiColorA(0,0,1,1));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
InGameState::~InGameState()
{
	//Destroy Scene Content
	DestroyScene();

	m_spCamera = nullptr;
	m_spCullingProcess = nullptr;
	
	m_spkPhysXScene = nullptr;
	m_spkScene = nullptr;

	m_pakSpawnPoints->clear();
	delete m_pakSpawnPoints;
	
	Utility::DestroyDebugScene();

#if DEBUG_TEST_CODE == TRUE
	DeleteTest();
#endif


	NiDelete m_pkInterface;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::OnInsertion()
{
	// set variables
	ITime::Reset(0);
	ShowCursor(0);

	// create game content
	if (m_spkScene == nullptr || !m_spkPhysXScene)
	{
		return;
	}

	CreateCamera(NiPoint3(0,-20,-30));

	HandleSpawns();

	m_pkPickupController = NiNew PickupController();

	Utility::SetupDebugScene();

	m_spkPhysXScene->SetDebugRender(true, m_spkScene);

	efdPhysX::PhysXSDKManager* pkPhysXManager = efdPhysX::PhysXSDKManager::GetManager();
	pkPhysXManager->m_pPhysXSDK->getFoundationSDK().getRemoteDebugger()->connect("localhost",5425);

	//Lighting
	NiDirectionalLight* pkDirLight = NiNew NiDirectionalLight();
	pkDirLight->SetDimmer(0.5f);
	pkDirLight->SetRotate(-0.75f, 0,1,0);
	
	pkDirLight->AttachAffectedNode(m_spkScene);
	m_spkScene->AttachChild(pkDirLight);

	FindNode kFindNode;
	kFindNode.m_czName = "Skybox";
	kFindNode.pkLight = pkDirLight;
	NiTNodeTraversal::DepthFirst_AllObjects( m_spkScene, kFindNode);
	kFindNode.m_czName = "Ground";
	NiTNodeTraversal::DepthFirst_AllObjects( m_spkScene, kFindNode);
	kFindNode.m_czName = "Cliffs";
	NiTNodeTraversal::DepthFirst_AllObjects( m_spkScene, kFindNode);

	NiShadowGenerator* pkShadowGenerator = NiNew NiShadowGenerator(pkDirLight);
	pkShadowGenerator->SetShadowTechnique("NiVSMShadowTechnique");
	NiVSMShadowTechnique* pkShadowTechnique = NiDynamicCast(NiVSMShadowTechnique,pkShadowGenerator->GetShadowTechnique());
	if(pkShadowTechnique)
	{
		pkShadowTechnique->SetBlurKernelSize(256);
	}
	pkShadowGenerator->SetRenderBackfaces(false);
	pkShadowGenerator->SetSizeHint(1024);

	NiShadowManager::AddShadowGenerator(pkShadowGenerator);
	NiShadowManager::SetSceneCamera(m_spCamera);

	m_spkScene->Update(0);
	m_spkScene->UpdateEffects();
	m_spkScene->UpdateProperties();

	m_spkPhysXScene->SetUpdateSrc(true);
	m_spkPhysXScene->SetUpdateDest(true);

	m_pkInterface = NiNew InGameGUI();
	
	m_uiCentreScreenX = ((Application*)Application::GetSingleton())->GetWindow()->GetWidth() / 2;
	m_uiCentreScreenY = ((Application*)Application::GetSingleton())->GetWindow()->GetHeight() / 2;

	#if DEBUG_TEST_CODE == TRUE
		CreateTest();
	#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::OnRemoval()
{
	GameStateManager::GetSingleton()->PushState(EE_NEW MenuState());
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::OnSuspend()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::OnResume()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
GameState::UPDATE_RESULT InGameState::OnUpdate()
{
	// centre the mouse
	SetCursorPos(m_uiCentreScreenX, m_uiCentreScreenY);

	// open pause menu
	if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_ESCAPE))
	{
		GameStateManager::GetSingleton()->PushState(EE_NEW PauseState(m_bIsQuiting));
		return STATE_RUNNING;
	}

	if (m_bIsQuiting)
	{
		return STATE_FINISHED;
	}

	Utility::ClearDebugScene();

	// update player
	m_pkPlayer->Update(*m_spCamera, m_apkEnemies, *m_pkBoss,m_spkPhysXScene);

	UpdateCamera();
	UpdateEnemies();

	// do test shit
	#if DEBUG_TEST_CODE == TRUE
		UpdateTest();
	#endif

	// update interface
	m_pkInterface->Update(m_pkPlayer);

	// update scene and physx
	m_spkScene->Update( *(NiUpdateProcess*)ITime::UserData() );
	m_spkPhysXScene->UpdateSources(ITime::CurrentTime());
	m_spkPhysXScene->Simulate(ITime::CurrentTime());
	m_spkPhysXScene->FetchResults(ITime::CurrentTime());
	m_spkPhysXScene->UpdateDestinations(ITime::CurrentTime());

	return STATE_RUNNING;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::OnRender(NiRenderer* a_pRenderer)
{
	a_pRenderer->SetBackgroundColor(NiColor(0.2f,0.2f,0.2f));

	// render shadow clicks
	const NiTPointerList<NiRenderClick*>& apkRenderClicks = NiShadowManager::GenerateRenderClicks();
	NiTListIterator oIter = apkRenderClicks.GetHeadPos();

	while(oIter)
	{
		NiRenderClickPtr pkClick = apkRenderClicks.GetNext(oIter);

		pkClick->Render(NiRenderer::GetRenderer()->GetRendererID());
	}
	
	a_pRenderer->EndUsingRenderTargetGroup();
	a_pRenderer->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ALL);

	#if DEBUG_TEST_CODE == TRUE
		RenderTest();
	#endif

	NiDrawScene(m_spCamera, m_spkScene, *m_spCullingProcess);

	m_pkInterface->Render(a_pRenderer);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::DestroyScene()
{
	NiDelete m_pkPlayer;
	NiDelete m_pkBoss;

	for (auto iter = m_apkEnemies.begin(); iter != m_apkEnemies.end(); ++iter)
	{
		 NiDelete (*iter);
	}
	m_apkEnemies.clear();

	NiDelete m_pkPickupController;
	NiDelete m_pkTPCamera;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::CreateCamera(const NiPoint3& a_rvCameraPosition)
{
	//Create New Camera
	m_spCamera = NiNew NiCamera();

	Window* pkWindow = ((Application*)Application::GetSingleton())->GetWindow();

	//Update the camera's aspect to fit the screen
	float fAspectRatio = pkWindow->GetWidth() / (float)pkWindow->GetHeight();
	NiFrustum oFrustum = m_spCamera->GetViewFrustum();
	oFrustum.m_fLeft = fAspectRatio * oFrustum.m_fBottom;
	oFrustum.m_fRight = fAspectRatio * oFrustum.m_fTop;
	oFrustum.m_fNear = 0.1f;
	oFrustum.m_fFar = 10000.0f;
	m_spCamera->SetViewFrustum( oFrustum );

	// create a standard mesh sphere/frustum culling process
	m_spCullingProcess = NiNew NiMeshCullingProcess( &m_oVisibleEntities, nullptr );

	// create third person camera
	m_pkTPCamera = new ThirdPersonCamera();
	m_pkTPCamera->SetCamera(m_spCamera);
	m_pkTPCamera->SetOffset(NiPoint3(0, -3, 2));

	m_spCamera->LookAtWorldPoint(NiPoint3(0,1,0), NiPoint3::UNIT_Z);
	m_spCamera->Update(0);
}

void InGameState::UpdateCamera()
{
	//m_pkTPCamera->SetTarget(m_pkPlayer-);
	m_pkTPCamera->Update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void InGameState::UpdateEnemies()
{
	for (auto iter = m_apkEnemies.begin(); iter != m_apkEnemies.end(); ++iter)
	{
		(*iter)->Update(*m_pkPlayer);

		if ((*iter)->IsAlive() == false)
		{
			(*iter)->OnDeath(*m_pkPickupController);

			Enemy *pkEnemy = *iter;
			++iter;
			m_apkEnemies.remove(pkEnemy);
			NiDelete (pkEnemy);
		}
	}
}

void InGameState::HandleSpawns()
{
	Enemy *pkEnemy;
	NiPoint3 kSpawnPoint;

	// spawn the entities from spawn points
	for (auto iter = m_pakSpawnPoints->begin(); iter != m_pakSpawnPoints->end(); ++iter)
	{
		kSpawnPoint = (*iter).m_kTranslate;

		switch ( (*iter).m_eType )
		{
		case NiSpawnPoint::ST_PLAYER:


			m_pkPlayer = NiNew Player(m_spkScene,m_spkPhysXScene, kSpawnPoint);


			m_pkTPCamera->SetTarget(*m_pkPlayer);
			m_pkTPCamera->Update();

			break;
		case NiSpawnPoint::ST_DEMON:
			{
				pkEnemy = NiNew Enemy(m_spkScene,m_spkPhysXScene,NiPoint3(kSpawnPoint.x, kSpawnPoint.y, 20));
				pkEnemy->Spawn( NiPoint3(kSpawnPoint.x, kSpawnPoint.y, kSpawnPoint.z + 2));
				m_apkEnemies.push_back(pkEnemy);

				break;
			}
		case NiSpawnPoint::ST_ANGEL:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE #
////////////////////////////////////////////////////////////////////////////////////////////////////

void InGameState::CreateTest()
{
	//m_pkFreeCam = new FreeLookCamera();
	//m_spCamera->LookAtWorldPoint(NiPoint3(0,0,0), NiPoint3::UNIT_Z);
	//m_spCamera->Update(0);
}

void InGameState::DeleteTest()
{
	//delete m_pkFreeCam;
}

void InGameState::UpdateTest()
{
	Utility::GetDebugScene()->Update( *(NiUpdateProcess*)ITime::UserData() );
	//m_pkFreeCam->Update(ITime::DeltaTime(), m_spCamera );


}

void InGameState::RenderTest()
{
	Utility::PreRenderDebugScene();
	NiDrawScene(m_spCamera, Utility::GetDebugScene(),*m_spCullingProcess);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE # TEST ZONE #
////////////////////////////////////////////////////////////////////////////////////////////////////
