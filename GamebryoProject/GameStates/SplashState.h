//////////////////////////////////////////////////////////////////////////
#ifndef __SPLASHSTATE_H_
#define __SPLASHSTATE_H_
//////////////////////////////////////////////////////////////////////////
#include "GameState.h"
#include <NiMeshScreenElements.h>

//////////////////////////////////////////////////////////////////////////
// Simple splash state that lasts a set duration.
// Currently also enables Input.
// Is usually the first state for a game, and pushes the menu state on removal.
class SplashState : public GameState
{
public:

	SplashState(float a_fDuration = 0);
	virtual ~SplashState();

	virtual void			OnInsertion();
	virtual void			OnRemoval();

	virtual UPDATE_RESULT	OnUpdate();
	virtual void			OnRender(NiRenderer* a_pRenderer);

private:

	NiMeshScreenElementsPtr m_spkSplashScreen;

	float	m_fDuration;
	float	m_fTimer;

};
//////////////////////////////////////////////////////////////////////////
#endif // __SPLASHSTATE_H_
//////////////////////////////////////////////////////////////////////////
