//////////////////////////////////////////////////////////////////////////
#include "LoadingState.h"
#include <NiRenderer.h>
#include "InGameState.h"
#include "GameStateManager.h"

#include "../Application.h"
#include "../AppBase/Window.h"
#include <NiTexturingProperty.h>
#include <NiMaterialProperty.h>

#include <stdio.h>

#include "..\AppBase\Utilities.h"
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
LoadingState::LoadingState(unsigned int a_uiLevelID)
	: m_uiLevelID(a_uiLevelID)
	, m_pThread(nullptr)
	, m_pkManager(nullptr)
{

}

//////////////////////////////////////////////////////////////////////////
LoadingState::~LoadingState()
{

}

//////////////////////////////////////////////////////////////////////////
void LoadingState::OnInsertion()
{
	m_pThread = efd::Thread::Create(this);
	m_pThread->Resume();

	NiTexturingPropertyPtr pkTextureProp = NiNew NiTexturingProperty("./Textures/Loading.bmp");
	Window* pkWindow = ((Application*)Application::GetSingleton())->GetWindow();

	NiMaterialPropertyPtr pkMaterialProp = NiNew NiMaterialProperty();
	pkMaterialProp->SetEmittance(NiColor(1,1,1));

	m_spkLoading = NiMeshScreenElements::Create(NiRenderer::GetRenderer(),0.0f,0.0f,pkWindow->GetWidth(),pkWindow->GetHeight(),NiRenderer::CORNER_TOP_LEFT);
	m_spkLoading->AttachProperty(pkTextureProp);
	m_spkLoading->AttachProperty(pkMaterialProp);

	m_spkLoading->Update(0);
	m_spkLoading->UpdateProperties();
}

//////////////////////////////////////////////////////////////////////////
void LoadingState::OnRemoval()
{
	if (m_pThread->GetReturnValue() == 0)
	{
		if (m_pkManager == nullptr)
			GameStateManager::GetSingleton()->PushState(EE_NEW InGameState(nullptr,nullptr, nullptr));
		else
			GameStateManager::GetSingleton()->PushState(EE_NEW InGameState(m_pkManager->GetScene(),m_pkManager->GetPhysXScene(), m_pkManager->GetSpawnPoints()));
		NiDelete m_pkManager;
	}

	NiDelete m_pThread;
	m_pThread = nullptr;
}

//////////////////////////////////////////////////////////////////////////
GameState::UPDATE_RESULT LoadingState::OnUpdate()
{
	if (m_pThread->GetStatus() == efd::Thread::COMPLETE)
	{
		return STATE_FINISHED;
	}
	return STATE_RUNNING;
}

//////////////////////////////////////////////////////////////////////////
unsigned int LoadingState::Execute(efd::Thread* a_pOwner)
{
	// implement load here
	FILE* pkFile;
	pkFile = fopen("./LevelData/Judgement.gsl", "r");

	if (pkFile != NULL)
	{
		fclose(pkFile);
		m_pkManager = NiNew ToolbenchManager("./LevelData/Judgement.gsl");
		m_pkManager->LoadBlock("Judgement");
	}
	else
	{
		printf( "===============================================================================\n" );
		printf( "\tCould not find GSL to load.\n" );
		printf( "===============================================================================\n" );
		fclose(pkFile);
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
void LoadingState::OnRender(NiRenderer* a_pRenderer)
{
	a_pRenderer->SetBackgroundColor(NiColor(NiUnitRandom(),NiUnitRandom(),NiUnitRandom()));
	a_pRenderer->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ALL);

	a_pRenderer->SetScreenSpaceCameraData();
	m_spkLoading->RenderImmediate(a_pRenderer);
}