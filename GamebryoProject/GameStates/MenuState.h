//////////////////////////////////////////////////////////////////////////
#ifndef __MENUSTATE_H_
#define __MENUSTATE_H_
//////////////////////////////////////////////////////////////////////////
#include "GameState.h"

// TEMP
#include <Ni2DString.h>

//////////////////////////////////////////////////////////////////////////
// Main menu state.
// Would contain menu options and eventually push on the Loading state
class MenuState : public GameState
{
public:

	MenuState();
	virtual ~MenuState();

	virtual void			OnInsertion();
	virtual void			OnRemoval();

	virtual UPDATE_RESULT	OnUpdate();
	virtual void			OnRender(NiRenderer* a_pRenderer);

private:

	enum OPTIONS
	{
		OPTION_Start,
		OPTION_Quit,

		OPTION_COUNT,
	};

	int		m_iCurrent;

	class NiColor m_kBackgroundColor;

	// TEMP
	Ni2DStringPtr	m_sp2DString;
};
//////////////////////////////////////////////////////////////////////////
#endif // __MENUSTATE_H_
//////////////////////////////////////////////////////////////////////////
