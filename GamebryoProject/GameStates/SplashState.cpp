//////////////////////////////////////////////////////////////////////////
#include "SplashState.h"
#include <NiRenderer.h>
#include "MenuState.h"
#include "GameStateManager.h"
#include "../Input/Input.h"
#include "../Time.h"

#include "../AppBase/Window.h"
#include "../Application.h"
#include <NiTexturingProperty.h>
#include <NiMaterialProperty.h>

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
SplashState::SplashState(float a_fDuration /* = 0 */)
	: m_fDuration(a_fDuration),
	m_fTimer(0)
{

}

//////////////////////////////////////////////////////////////////////////
SplashState::~SplashState()
{

}

//////////////////////////////////////////////////////////////////////////
void SplashState::OnInsertion()	
{
	m_fTimer = 0;

	NiTexturingPropertyPtr pkTextureProp = NiNew NiTexturingProperty("./Textures/Splash.bmp");
	Window* pkWindow = ((Application*)Application::GetSingleton())->GetWindow();

	NiMaterialPropertyPtr pkMaterialProp = NiNew NiMaterialProperty();
	pkMaterialProp->SetEmittance(NiColor(1,1,1));

	m_spkSplashScreen = NiMeshScreenElements::Create(NiRenderer::GetRenderer(),0.0f,0.0f,pkWindow->GetWidth(),pkWindow->GetHeight(),NiRenderer::CORNER_TOP_LEFT);
	m_spkSplashScreen->AttachProperty(pkTextureProp);
	m_spkSplashScreen->AttachProperty(pkMaterialProp);

	m_spkSplashScreen->Update(0);
	m_spkSplashScreen->UpdateProperties();
}

//////////////////////////////////////////////////////////////////////////
void SplashState::OnRemoval()
{
	GameStateManager::GetSingleton()->PushState( EE_NEW MenuState() );
}

//////////////////////////////////////////////////////////////////////////
GameState::UPDATE_RESULT SplashState::OnUpdate()
{	
	m_fTimer += ITime::DeltaTime();

	// delay input creation so we have visuals ASAP
	// when game loads
	static bool sbInputInitialised = false;
	if (sbInputInitialised == false &&
		m_fTimer > (m_fDuration * 0.05f))
	{
		if (Input::GetSingleton() == nullptr)
			Input::Create();
		sbInputInitialised = true;
	}

	if (m_fDuration > 0)
	{
		if (m_fTimer >= m_fDuration &&
			Input::GetSingleton() != nullptr)
		{
			return STATE_FINISHED;
		}
	}
	else
	{
		if (m_fTimer >= m_fDuration ||
			Input::GetSingleton() != nullptr)
		{
			return STATE_FINISHED;
		}
	}	

	return STATE_RUNNING;
}

//////////////////////////////////////////////////////////////////////////
void SplashState::OnRender(NiRenderer* a_pRenderer)
{
	a_pRenderer->SetBackgroundColor(NiColor(1,0,0));
	a_pRenderer->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ALL);

	a_pRenderer->SetScreenSpaceCameraData();
	m_spkSplashScreen->RenderImmediate(a_pRenderer);
}