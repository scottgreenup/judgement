//////////////////////////////////////////////////////////////////////////
#ifndef __INGAMESTATE_H_
#define __INGAMESTATE_H_
//////////////////////////////////////////////////////////////////////////
#include "GameState.h"
#include <NiMain.h>

#include <efd/StdContainers.h>
#include <NiPhysXScene.h>

#include "../Entities/PhysX/PhysXRay.h"
#include "../Entities/Player.h"
#include "../Entities/Pickups/PickupController.h"
#include "../Entities/Boss.h"
#include "../Entities/PhysX/PhysXRay.h"

#include "../Cameras/ThirdPersonCamera.h"

#include <NiMeshScreenElements.h>
#include <NiMaterialProperty.h>
#include <NiTexturingProperty.h>
#include <NiAlphaProperty.h>
#include <NiFogProperty.h>

#include "../Toolbench/ToolbenchManager.h"

//////////////////////////////////////////////////////////////////////////
// Our state for when the game is being played.
// We would update any game systems that relate to "playing" the game.
class InGameState : public GameState
{
public:

	InGameState(NiNode* a_pkScene,NiPhysXScene* a_pkPhysXScene, efd::list<NiSpawnPoint> *a_pakSpawnPoints);
	virtual ~InGameState();

	virtual void			OnInsertion();
	virtual void			OnRemoval();

	virtual void			OnSuspend();
	virtual void			OnResume();

	virtual UPDATE_RESULT	OnUpdate();

	virtual void			OnRender(NiRenderer* a_pRenderer);


private:
	// camera
	void CreateCamera(const NiPoint3& a_rvCameraPosition);
	void DeleteCamera();
	void UpdateCamera();

	// destroying Content
	void DestroyScene();

	// test
	void CreateTest();
	void DeleteTest();
	void UpdateTest();
	void RenderTest();

	void HandleSpawns();
	void UpdateEnemies();


	// for moving mouse to centre
	unsigned int m_uiCentreScreenX;
	unsigned int m_uiCentreScreenY;

	// required for quitting, pause state changes this
	bool m_bIsQuiting;

	NiMeshScreenElementsPtr m_spkBackground;
	NiMeshScreenElementsPtr m_spkForeground;
	NiMeshScreenElementsPtr m_spkOverlay;

	// game Variables
	NiNodePtr m_spkScene;
	NiPhysXScenePtr m_spkPhysXScene;

	NiCameraPtr	m_spCamera;
	class ThirdPersonCamera *m_pkTPCamera;

	Player* m_pkPlayer;
	Boss* m_pkBoss;
	efd::list<Enemy*> m_apkEnemies;


	NiVisibleArray		m_oVisibleEntities;
	NiCullingProcessPtr	m_spCullingProcess;

	efd::list<NiSpawnPoint>* m_pakSpawnPoints;

	class InGameGUI* m_pkInterface;
	class CameraManager* m_pkCameraManager;
	efd::list<class Agent*> apkCameraTargets;
	PickupController* m_pkPickupController;

	class FreeLookCamera* m_pkFreeCam;
	bool m_bIsFreeCam;

};
//////////////////////////////////////////////////////////////////////////
#endif // __INGAMESTATE_H_
//////////////////////////////////////////////////////////////////////////
