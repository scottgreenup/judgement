//////////////////////////////////////////////////////////////////////////
#include "MenuState.h"
#include <NiRenderer.h>
#include <NiMath.h>
#include <NiString.h>
#include "../Input/Input.h"
#include "../Application.h"
#include "../AppBase/Window.h"
#include "LoadingState.h"
#include "GameStateManager.h"
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
MenuState::MenuState()
	: m_iCurrent(0)
{

}

//////////////////////////////////////////////////////////////////////////
MenuState::~MenuState()
{

}

//////////////////////////////////////////////////////////////////////////
void MenuState::OnInsertion()
{
	// TEMP
	NiFont* pFont = NiFont::Create(((Application*)Application::GetSingleton())->GetRenderer(),"Font/CelticGaramondII_28_A.nff");
	char acBuffer[32];
	m_sp2DString = NiNew Ni2DString(pFont,0,32,acBuffer,NiColorA::WHITE);

	m_kBackgroundColor = NiColor(0.0f, 0.0f, 0.0f);
}

//////////////////////////////////////////////////////////////////////////
void MenuState::OnRemoval()
{
	switch (m_iCurrent)
	{
	case OPTION_Start:
		{
			// load level
			GameStateManager::GetSingleton()->PushState(NiNew LoadingState(0));
			break;
		}
	case OPTION_Quit:
		{
			// nothing, just quit
			break;
		}
	default:
		{
			break;
		}
	};
}

//////////////////////////////////////////////////////////////////////////
GameState::UPDATE_RESULT MenuState::OnUpdate()
{
	// menu access
	if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_UP) ||
		Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_W))
	{
		if (--m_iCurrent < 0)
			m_iCurrent = OPTION_COUNT - 1;
	}
	else if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_DOWN) ||
			 Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_S))
	{
		if (++m_iCurrent >= OPTION_COUNT)
			m_iCurrent = 0;
	}
	else if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_RETURN))
	{
		return STATE_FINISHED;
	}

	float i = 0.05f;		// 10%
	float j = NiUnitRandom() * i - (i * 0.6f);
	float k = NiUnitRandom() * i - (i * 0.6f);

	float m = NiUnitRandom();

	if (m <= 0.5f)
		m_kBackgroundColor = m_kBackgroundColor + NiColor( j, j, j * 2.0f );
	else
		m_kBackgroundColor = m_kBackgroundColor + NiColor( k, k, k * 2.0f );

	m_kBackgroundColor.r = m_kBackgroundColor.r < 0.0f ? 0.4f : m_kBackgroundColor.r;
	m_kBackgroundColor.g = m_kBackgroundColor.g < 0.0f ? 0.4f : m_kBackgroundColor.g;
	m_kBackgroundColor.b = m_kBackgroundColor.b < 0.0f ? 0.4f : m_kBackgroundColor.b;

	return STATE_RUNNING;
}

//////////////////////////////////////////////////////////////////////////
void MenuState::OnRender(NiRenderer* a_pRenderer)
{
	a_pRenderer->SetBackgroundColor(m_kBackgroundColor);
	a_pRenderer->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ALL);

	// TEMPORARY MENU DISPLAY
	{
		a_pRenderer->SetScreenSpaceCameraData();

		float fW = 0;
		float fH = 0;
		Window* pWindow = ((Application*)Application::GetSingleton())->GetWindow();

		///////////////////////////////////////////////////////////////////////////////
		// render play text
		if (m_iCurrent == OPTION_Start)
			m_sp2DString->SetText(".:: START ::.");
		else
			m_sp2DString->SetText("START");

		// FOR THE LOVE OF BJARNE STROUSTRUP PLEASE LEAVE THE TEXT AS "START" or ".:: START ::."
		// HAVING A "Y" AS THE LAST CHARACTER ADDS MOVEMENT. JUST TRUST ME!

		m_sp2DString->GetTextExtent(fW,fH);
		m_sp2DString->SetPosition( pWindow->GetWidth() / 2 - (short)(fW * 0.5f), pWindow->GetHeight() / 2 - 35 - (short)(fH * 0.5f));
		m_sp2DString->Draw(a_pRenderer);
		///////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////
		// render quit text
		if (m_iCurrent == OPTION_Quit)
			m_sp2DString->SetText(".:: QUIT ::.");
		else
			m_sp2DString->SetText("QUIT");

		m_sp2DString->GetTextExtent(fW,fH);
		m_sp2DString->SetPosition( pWindow->GetWidth() / 2 - (short)(fW * 0.5f), pWindow->GetHeight() / 2 + 35 - (short)(fH * 0.5f));
		m_sp2DString->Draw(a_pRenderer);
		///////////////////////////////////////////////////////////////////////////////
	}
}