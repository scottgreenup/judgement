//////////////////////////////////////////////////////////////////////////
#ifndef __LOADINGSTATE_H_
#define __LOADINGSTATE_H_
//////////////////////////////////////////////////////////////////////////
#include "GameState.h"
#include <efd/Thread.h>
#include <efd/ThreadFunctor.h>

#include <NiMeshScreenElements.h>

#include "../Toolbench/ToolbenchManager.h"

//////////////////////////////////////////////////////////////////////////
// The Loading state kicks off a thread that runs the Execute() method.
// This allows us to continue to update visuals while we load a lot of data.
// When the loading thread is finished we can remove this state and push on
// the InGameState.
class LoadingState : public GameState, public efd::ThreadFunctor
{
public:

	LoadingState(unsigned int a_uiLevelID);
	virtual ~LoadingState();

	virtual void			OnInsertion();
	virtual void			OnRemoval();
	
	virtual UPDATE_RESULT	OnUpdate();
	virtual void			OnRender(NiRenderer* a_pRenderer);

	// threaded loading function
	virtual unsigned int	Execute(efd::Thread* a_pOwner);

private:

	NiMeshScreenElementsPtr m_spkLoading;
	unsigned int	m_uiLevelID;
	efd::Thread*	m_pThread;

	ToolbenchManager* m_pkManager;
};
//////////////////////////////////////////////////////////////////////////
#endif // __LOADINGSTATE_H_
//////////////////////////////////////////////////////////////////////////
