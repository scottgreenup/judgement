//////////////////////////////////////////////////////////////////////////
#include "PauseState.h"
#include <NiRenderer.h>
#include "../Input/Input.h"
#include "../Application.h"
#include "../AppBase/Window.h"
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
PauseState::PauseState(bool& a_rbQuiting)
	: m_rbQuiting(a_rbQuiting)
{

}

//////////////////////////////////////////////////////////////////////////
PauseState::~PauseState()
{

}

//////////////////////////////////////////////////////////////////////////
void PauseState::OnInsertion()
{
	m_iCurrent = 0;

	// TEMP
	NiFont* pFont = NiFont::Create(((Application*)Application::GetSingleton())->GetRenderer(),"Font/Consolas_18.nff");
	char acBuffer[32];
	m_sp2DString = NiNew Ni2DString(pFont,0,32,acBuffer,NiColorA::WHITE);
}

//////////////////////////////////////////////////////////////////////////
void PauseState::OnRemoval()
{
	switch (m_iCurrent)
	{
	case OPTION_Resume:	m_rbQuiting = false;	break;
	case OPTION_Quit:	m_rbQuiting = true;		break;
	default:	break;
	};
}

//////////////////////////////////////////////////////////////////////////
GameState::UPDATE_RESULT PauseState::OnUpdate()
{
	// escape returns to game state
	if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_ESCAPE))
	{
		m_iCurrent = OPTION_Resume;
		return STATE_FINISHED;
	}

	// menu access
	if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_UP) ||
		Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_W))
	{
		if (--m_iCurrent < 0)
			m_iCurrent = OPTION_COUNT - 1;
	}
	else if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_DOWN) ||
		Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_S))
	{
		if (++m_iCurrent >= OPTION_COUNT)
			m_iCurrent = 0;
	}
	else if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_RETURN))
	{
		return STATE_FINISHED;
	}

	return STATE_RUNNING;
}

//////////////////////////////////////////////////////////////////////////
void PauseState::OnRender(NiRenderer* a_pRenderer)
{
	// TEMP
	{
		a_pRenderer->SetScreenSpaceCameraData();

		float fW = 0, fH = 0;
		Window* pWindow = ((Application*)Application::GetSingleton())->GetWindow();

		// render resume text
		if (m_iCurrent == OPTION_Resume)
			m_sp2DString->SetText("> RESUME <");
		else
			m_sp2DString->SetText("RESUME");

		m_sp2DString->GetTextExtent(fW,fH);
		m_sp2DString->SetPosition( pWindow->GetWidth() / 2 - (short)(fW * 0.5f), pWindow->GetHeight() / 2 - 50);
		m_sp2DString->Draw(a_pRenderer);

		// render quit text
		if (m_iCurrent == OPTION_Quit)
			m_sp2DString->SetText("> QUIT <");
		else
			m_sp2DString->SetText("QUIT");

		m_sp2DString->GetTextExtent(fW,fH);
		m_sp2DString->SetPosition( pWindow->GetWidth() / 2 - (short)(fW * 0.5f), pWindow->GetHeight() / 2 + 50);
		m_sp2DString->Draw(a_pRenderer);
	}
}