//////////////////////////////////////////////////////////////////////////
#ifndef __GAMESTATEMANAGER_H_
#define __GAMESTATEMANAGER_H_
//////////////////////////////////////////////////////////////////////////
#include <efd/StdContainers.h>
#include <NiDebug.h>
#include <NiMemObject.h>

class GameState;
class NiRenderer;

//////////////////////////////////////////////////////////////////////////
// A simple Game State controller, implemented as a singleton and stack.
// Only the top state is updated.
// All states are rendered, from the bottom of the stack to the top.
// Only one state can be pushed each update, with the last push being applied.
// Pushed states are applied after the current state updates.
// Removal of a state that returns finished from its update occurs before
// pushed states are applied. (allows for ending state to remove itself and
// push on a new state).
class GameStateManager : public NiMemObject
{
public:

	GameStateManager();
	~GameStateManager();

	static GameStateManager*	GetSingleton()		{	return sm_pSingleton;	}

	GameState*					GetCurrentState()	{	return m_aStates.back();			}
	unsigned int				GetStateCount()		{	return m_aStates.size();			}
	bool						HasStates()			{	return m_aStates.empty() == false;	}

	void						UpdateStates();
	void						RenderStates(NiRenderer* a_pRenderer);

	void						PushState(GameState* a_pState)	{ NIASSERT(m_pNextState == nullptr);	m_pNextState = a_pState;	}

	void						ForcePopState();

private:

	efd::deque<GameState*>		m_aStates;
	GameState*					m_pNextState;

	static GameStateManager*	sm_pSingleton;
};
//////////////////////////////////////////////////////////////////////////
#endif // __GAMESTATEMANAGER_H_
//////////////////////////////////////////////////////////////////////////
