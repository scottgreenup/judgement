//////////////////////////////////////////////////////////////////////////
#ifndef __PAUSESTATE_H_
#define __PAUSESTATE_H_
//////////////////////////////////////////////////////////////////////////
#include "GameState.h"

// TEMP
#include <Ni2DString.h>

//////////////////////////////////////////////////////////////////////////
// Pause state overlay that sits over an InGameState
class PauseState : public GameState
{
public:

	PauseState(bool& a_rbQuiting);
	virtual ~PauseState();

	virtual void			OnInsertion();
	virtual void			OnRemoval();

	virtual UPDATE_RESULT	OnUpdate();
	virtual void			OnRender(NiRenderer* a_pRenderer);

private:

	enum OPTIONS
	{
		OPTION_Resume,
		OPTION_Quit,

		OPTION_COUNT,
	};

	bool&			m_rbQuiting;

	int				m_iCurrent;

	// TEMP
	Ni2DStringPtr	m_sp2DString;
};
//////////////////////////////////////////////////////////////////////////
#endif // __PAUSESTATE_H_
//////////////////////////////////////////////////////////////////////////
