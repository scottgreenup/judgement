#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Agent.h"

#include "./Enemies/Enemy.h"
#include "Boss.h"

#include <NiPoint3.h>
#include <efd\StdContainers.h>

#include <NiCamera.h>

class Player : public Agent
{
	friend class CombatController;
public:
	Player(NiNodePtr a_spkScene,NiPhysXScenePtr a_spkPhysXScene, const NiPoint3& a_rkSpawnAt);
	~Player();

	void Update(NiCamera& a_rkCamera, efd::list<Enemy*>& a_apkEnemies, Boss& a_rkBoss, NiPhysXScene* a_pkPhysXScene);
	void Draw();
	void DrawDebug() {}

	void IncreaseMana(unsigned int a_uiMana);
	void DecreaseMana(unsigned int a_uiMana);

	NiPoint3 GetDirectionXY()
	{
		return m_vDirectionXY;
	}

protected:

	void UpdateInputMovement(NiCamera& a_rkCamera);

	void UpdateCameraController(NiCamera& a_rkCamera);

	static void CameraRayCollision(const NxRaycastHit& ac_rkHit);
	
private:

	NiPoint3 m_vDirection;
	NiPoint3 m_vDirectionXY;
	NiPoint3 m_vDirectionXYPerp;
	NiPoint3 m_vDeltaVelocity;

	float m_fRotate;

	float m_fDodgeTimer;
	float m_fDodgeDelay;
	bool m_bDodge;
	bool m_bAttacking;
	NiPoint3 m_vDodgeDirection;

	float m_fDodgingTimer;
	float m_fDodgingDelay;

	float m_fPlayerSpeed;
	float m_fAttackRadius;
	unsigned int m_uiAttackDamage;

	float m_fAngleOfAttack;

	float m_fJumpVelocity;
	float m_fJumpInpulse;

	bool m_bJumping;

	float m_fAttackTimer;
	float m_fAttackDelay;

	bool m_bDead;
	bool m_bCameraObstructed;

	class CombatController* m_pkCombatController;
	class PhysXRay* m_pkCameraRay;
};

#endif
