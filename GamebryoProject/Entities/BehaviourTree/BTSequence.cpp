////////////////////////////////////////////////////////////////////////////////
///	@brief		BTSequence class
///	@author		Scott J. Greenup
///	@date		01.05.2012
////////////////////////////////////////////////////////////////////////////////

#include "BTSequence.h"

////////////////////////////////////////////////////////////////////////////////
bool BTSequence::Run(Agent* a_poAgent, efd::list<Agent*> a_apoAgents)
{
	// go through each node, if one returns false, return false
	for(efd::list<Behaviour*>::iterator iter = m_lpoNodes.begin(); iter != m_lpoNodes.end(); ++iter)
	{
		if ( (*iter)->Run(a_poAgent, a_apoAgents) == false )
		{
			return false;
		}
	}

	// all passed, return true
	return true;
}