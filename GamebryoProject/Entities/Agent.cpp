////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file Entities\Agent.cpp
/// @author Scott Greenup
/// @date 28.07.2011
/// @brief An entity that has animation, physX and movement input
////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
#include "Agent.h"
#include "../AppBase/Utilities.h"
#include "../AppBase/DebugLineMesh.h"
#include "../AppBase/DebugTriMesh.h"
#include "../Time.h"

#include <NiParticle.h>
#include <NiString.h>

///////////////////////////////////////////////////////////////////////////////
Agent::Agent()
	: m_pkPhysXController(nullptr),
	m_spkActorManager(nullptr)
{
	m_bAlive = false;
}

///////////////////////////////////////////////////////////////////////////////
Agent::~Agent()
{
	if(m_pkPhysXController)
	{
		NiDelete m_pkPhysXController;
		m_pkPhysXController = nullptr;
	}
}

///////////////////////////////////////////////////////////////////////////////
void Agent::Update()
{
	float fDT = ITime::DeltaTime();

	if(m_spkActorManager)
		m_spkActorManager->Update(ITime::CurrentTime());

}

///////////////////////////////////////////////////////////////////////////////
void Agent::Draw()
{

}

///////////////////////////////////////////////////////////////////////////////
void Agent::DrawDebug()
{
	
}

///////////////////////////////////////////////////////////////////////////////
void Agent::IncreaseHP(unsigned int a_uiValue)
{
	// increase hp
	m_uiHP += a_uiValue;

	// cap hp at MaxHP
	if (m_uiHP > m_uiMaxHP)
	{
		m_uiHP = m_uiMaxHP;
	}
}

///////////////////////////////////////////////////////////////////////////////
void Agent::DecreaseHP(unsigned int a_uiValue)
{
	// if the value exceeds the health left, make health 0
	if (a_uiValue > m_uiHP)
	{
		m_uiHP = 0;

		unsigned int ID = m_spkActorManager->FindSequenceID("death");
		m_spkActorManager->SetTargetAnimation(ID);
	}
	else
	{
		m_uiHP -= a_uiValue;
	}
}

///////////////////////////////////////////////////////////////////////////////
NiPoint3 Agent::GetTranslate() const
{
	if(m_pkPhysXController)
	{
		return m_pkPhysXController->GetTranslate();
	}
	
	return NiPoint3::ZERO;
}

///////////////////////////////////////////////////////////////////////////////
unsigned int Agent::GetHP() const
{
	return m_uiHP;
}

///////////////////////////////////////////////////////////////////////////////
unsigned int Agent::GetMaxHP() const
{
	return m_uiMaxHP;
}

///////////////////////////////////////////////////////////////////////////////
bool Agent::IsAlive() const
{
	return m_bAlive;
}

///////////////////////////////////////////////////////////////////////////////
void Agent::CreateActorManager(const NiString& a_kKFMFileDir,const NiString& a_kDefaultAnim)
{
	m_spkActorManager = NiActorManager::Create(a_kKFMFileDir);
	m_spkNifRoot = NiDynamicCast(NiNode,m_spkActorManager->GetNIFRoot());
	 
	NiActorManager::SequenceID kSequence = m_spkActorManager->FindSequenceID(a_kDefaultAnim);
	m_spkActorManager->SetTargetAnimation(kSequence);
}

///////////////////////////////////////////////////////////////////////////////
NiActorManager* Agent::GetActorManager() const
{
	return m_spkActorManager;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void Agent::SetTargetAnimation(const char* ac_szName)
{
	NiActorManager::SequenceID kSequence =
		m_spkActorManager->FindSequenceID(ac_szName);

	m_spkActorManager->SetTargetAnimation(kSequence);
}

void Agent::ActivateSequence(const char* ac_szName)
{
	NiActorManager::SequenceID kSequence =
		m_spkActorManager->FindSequenceID(ac_szName);

	m_spkActorManager->ActivateSequence(kSequence);
}

void Agent::DeactivateSequence(const char* ac_szName)
{
	NiActorManager::SequenceID kSequence =
		m_spkActorManager->FindSequenceID(ac_szName);

	m_spkActorManager->DeactivateSequence(kSequence);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// End of Entities\Agent.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////
