///////////////////////////////////////////////////////////////////////////////
//	file		Projectile.cpp
//	details		Projectile Class Definition
//	author		Nathan Chambers
//	date		31.07.2012
///////////////////////////////////////////////////////////////////////////////
#include "./Projectile.h"
#include "../AppBase/Utilities.h"
#include "../AppBase/DebugLineMesh.h"
#include "../AppBase/DebugTriMesh.h"
#include "../Time.h"
#include "./Player.h"

///////////////////////////////////////////////////////////////////////////////
Projectile::Projectile()
{
	//Setup Projectile Properties
	m_bAlive = false;
	m_fAliveTimer = 0.0f;
	m_fLifeTime = 5.0f;
	m_fVelocity = 0.0f;
	m_fHitRadious = 1.0f;

	m_vTranslate = NiPoint3(0,0,0);
	m_vHeading = NiPoint3(0,0,0);

	m_iDamage = 100;
}

///////////////////////////////////////////////////////////////////////////////
void Projectile::Update(Player& a_rkPlayer)
{
	//Move the Projectile in the directed heading
	m_vTranslate += m_vHeading * ITime::DeltaTime() * m_fVelocity;

	//Get XY Distance Between Player and Projectile
	NiPoint3 vDistance = a_rkPlayer.GetTranslate() - m_vTranslate;

	//If the Distance is less then the Hit Radious
	if (vDistance.Length() < m_fHitRadious)
	{
		//Attack the Player and Kill the Projectile
		a_rkPlayer.DecreaseHP(m_iDamage);
		m_bAlive = false;
	}

	//Increate the Life Timer
	m_fAliveTimer += ITime::DeltaTime();

	//If the Live Timer goes above the Lifetime of the projectile
	if(m_fAliveTimer > m_fLifeTime)
	{
		//Destroy the Projectile and Reset Timer
		m_bAlive = false;
		m_fAliveTimer = 0.0f;
	}

	if(m_vTranslate.z <= -0.1f)
	{
		m_bAlive = false;
	}
}

///////////////////////////////////////////////////////////////////////////////
void Projectile::DebugDraw()
{
	Utility::GetDebugTriMesh()->AddAABB(m_vTranslate, NiPoint3(0.25f,0.25f,0.25f), NiColorA(0.7f,0.0f,1.0f,1));
	Utility::GetDebugLineMesh()->AddAABB(m_vTranslate, NiPoint3(0.26f,0.26f,0.26f), NiColorA(0.7f,0,0.8f,1));
}

///////////////////////////////////////////////////////////////////////////////
void Projectile::Respawn(const NiPoint3& a_vSpawnPos,const NiPoint3& a_vDirection,float a_fVelocity,int a_iDamage)
{
	//Set the properties of the projectile
	m_bAlive = true;
	m_vTranslate = a_vSpawnPos;
	m_vHeading = a_vDirection;
	m_fVelocity = a_fVelocity;
	m_iDamage = a_iDamage;

	//Normalise Heading Vector
	NiPoint3::UnitizeVector(m_vHeading);
}

///////////////////////////////////////////////////////////////////////////////
bool Projectile::IsAlive() const
{
	//Return Alive Boolean
	return m_bAlive;
}