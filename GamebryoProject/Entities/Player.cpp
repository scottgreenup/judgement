#include "Player.h"

#include <NiColor.h>
#include <NiMatrix3.h>
#include <NiPoint3.h>
#include <NiQuaternion.h>
#include <NiSequenceData.h>

#include <NiMath.h>

#include "../AppBase/Utilities.h"
#include "../AppBase/DebugLineMesh.h"
#include "../AppBase/DebugTriMesh.h"
#include "../GameStates/InGameState.h"

#include "../Input/Input.h"
#include "../Time.h"
#include "./PhysX/PhysXRay.h"

#include "./Controllers/CombatController.h"

#include <Windows.h>

#include <NiTimeController.h>

////////////////////////////////////////////////////////////////////////////////////////////////////
Player::Player(NiNodePtr a_spkScene,NiPhysXScenePtr a_spkPhysXScene, const NiPoint3& a_rkSpawnAt)
{
	m_fRotate = 0;
	
	m_fPlayerSpeed = 15.0f;

	m_fAttackRadius = 5.0f;

	m_uiHP = 100;
	m_uiMaxHP = 100;

	m_uiAttackDamage = 20;

	m_fDodgingTimer = 0.4f;
	m_fDodgingDelay = 0.3f;

	m_fDodgeDelay = m_fDodgingDelay + 0.1f;
	m_fDodgeTimer = 0.0f;

	m_fAngleOfAttack = 0.25;

	m_bDodge = false;
	m_bAttacking = false;

	m_fJumpVelocity = 0;
	m_fJumpInpulse = 30.0f;
	m_bJumping = false;
	m_bDead= false;

	// the timer counts down... instead of counting up
	m_fAttackDelay = 0.5f;
	m_fAttackTimer = m_fAttackDelay;

	//CreateActorManager("Characters/Demon/ANIM_Demon.kfm","Idle");
	CreateActorManager("Props/Vandal.kfm","idle");

	NiTransform kNifTransform;
	kNifTransform.m_Translate = NiPoint3(0,0,-1.5f);
	kNifTransform.m_Rotate = NiMatrix3::IDENTITY;
	kNifTransform.m_fScale = 0.01f;

	m_pkPhysXController = NiNew PhysXController(
		a_spkScene,
		a_spkPhysXScene,
		a_rkSpawnAt + NiPoint3(0,0,2),
		1.5f,
		0.7f,
		m_spkNifRoot,
		kNifTransform,
		"player");

	m_pkCombatController = new CombatController(this);

	m_pkCameraRay = NiNew PhysXRay();
	m_pkCameraRay->SetOnHitCallback(Player::CameraRayCollision);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
Player::~Player()
{
	delete m_pkCombatController;
	NiDelete m_pkCameraRay;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void Player::Update(NiCamera& a_rkCamera, efd::list<Enemy*>& a_apkEnemies, Boss& a_rkBoss,NiPhysXScene* a_pkPhysXScene)
{		
	// center cursor
	SetCursorPos(840, 525);


	if(m_uiHP > 0)
	{
		m_vDeltaVelocity = NiPoint3::ZERO;	

		UpdateInputMovement(a_rkCamera);

		static float fTimer = 0;
		static bool bAttacking = false;

		if(Input::Mouse()->ButtonWasPressed(NiInputMouse::NIM_LEFT))
		{
			m_pkCombatController->LightAttack(a_apkEnemies);
		}

		m_pkCombatController->Update(a_apkEnemies);
	}
	else
	{
		NiActorManager::SequenceID kDeath = m_spkActorManager->FindSequenceID("damaged");
		m_spkActorManager->SetTargetAnimation(kDeath);
		m_vDeltaVelocity = NiPoint3::ZERO;
	}

	m_pkPhysXController->Move(m_vDeltaVelocity);

	Agent::Update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void Player::Draw()
{
	//NiColorA kColorToDraw = m_uiHP > 0 ? NiColorA(0,1,0,0.4f) : NiColorA(1,0,0,0.4f);

	//// draw attack cone
	//NiMatrix3 qAttackRot;
	//qAttackRot.MakeZRotation(NI_PI * m_fAngleOfAttack);
	//NiPoint3 vLeftAngle = m_vDirectionXY;
	//vLeftAngle = vLeftAngle * qAttackRot;

	//qAttackRot.MakeZRotation(NI_PI * -m_fAngleOfAttack);
	//NiPoint3 vRightAngle = m_vDirectionXY;
	//vRightAngle = vRightAngle * qAttackRot;

	//Utility::GetDebugLineMesh()->AddLine(m_pkTranslate, m_pkTranslate + vLeftAngle * m_fAttackRadius, NiColorA(0,1,0,1), NiColorA(0,1,0,1));
	//Utility::GetDebugLineMesh()->AddLine(m_pkTranslate, m_pkTranslate + vRightAngle * m_fAttackRadius, NiColorA(0,1,0,1), NiColorA(0,1,0,1));
	//Utility::GetDebugLineMesh()->AddLine(m_pkTranslate, m_pkTranslate + m_vDirectionXY * m_fAttackRadius, NiColorA(1,0,1,1), NiColorA(1,0,1,1));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void Player::UpdateInputMovement(NiCamera& a_rkCamera)
{
	// movement - running
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_W))
	{
		m_vDeltaVelocity += m_vDirectionXY;
		NiActorManager::SequenceID uiCurrentAnimation = m_spkActorManager->FindSequenceID("run");
		GetActorManager()->SetTargetAnimation(uiCurrentAnimation);
	}
	else if(Input::Keyboard()->KeyWasReleased(NiInputKeyboard::KEY_W))
	{
		NiActorManager::SequenceID uiCurrentAnimation = m_spkActorManager->FindSequenceID("idle");
		GetActorManager()->SetTargetAnimation(uiCurrentAnimation);
	}
	
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_S))
	{
		m_vDeltaVelocity += -m_vDirectionXY;
	}
		
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_A))
	{
		m_vDeltaVelocity += m_vDirectionXYPerp;
	}

	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_D))
	{
		m_vDeltaVelocity += -m_vDirectionXYPerp;
	}

	if(Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_SPACE) && m_bJumping == false)
	{
		m_pkPhysXController->Jump(m_fJumpInpulse);
	}

	if (m_fDodgingTimer <= m_fDodgingDelay)
	{
		m_vDeltaVelocity += m_vDodgeDirection * 2.0f;
	}

	if (m_fDodgeTimer >= m_fDodgeDelay)
	{
		if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_LSHIFT) && !m_bJumping)
		{
			m_fDodgingTimer = 0.0f;
			m_fDodgeTimer = 0.0f;
		}
	}

	
	// get mouse movement
	int iDeltaX = 0;
	int iDeltaY = 0;
	int iDeltaZ = 0;

	Input::Mouse()->GetPositionDelta(iDeltaX, iDeltaY, iDeltaZ);
	float fMouseSpeedMul = 0.2f;
	float fAddRotZ = -iDeltaX * ITime::DeltaTime() * fMouseSpeedMul;

	NiQuaternion kRotToAdd;
	kRotToAdd.FromAngleAxis(fAddRotZ, NiPoint3(0,0,1));

	NiQuaternion kRotCurr;
	m_spkNifRoot->GetRotate(kRotCurr);

	NiQuaternion kRotNew = kRotCurr * kRotToAdd;
	kRotNew.Normalize();

	m_spkNifRoot->SetRotate(kRotNew);
	NiMatrix3 kRotate;
	kRotNew.ToRotation(kRotate);

	NiPoint3 kColumnY;
	kRotate.GetCol(1, kColumnY);

	m_vDirectionXYPerp = NiPoint3(kColumnY.y, -kColumnY.x, 0);
	m_vDirectionXY = NiPoint3(-kColumnY.x, -kColumnY.y, 0);

	m_vDeltaVelocity.Unitize();
	m_vDeltaVelocity = m_vDeltaVelocity * m_fPlayerSpeed;

	m_spkNifRoot->SetRotate(kRotate);
}

void Player::CameraRayCollision( const NxRaycastHit& ac_rkHit )
{
	float fDistanceToContact = ac_rkHit.distance;
	//NiNode* pkObj = NiDynamicCast(NiNode,ac_rkHit.shape->getActor().userData);
	//NiString kName = pkObj->GetName();
	
	if(fDistanceToContact > 0)
	{
		NiPoint3 vPosition;
		NiPhysXTypes::NxVec3ToNiPoint3(ac_rkHit.worldImpact,vPosition);
		Utility::GetDebugTriMesh()->AddAABB(vPosition,NiPoint3(0.1f,0.1f,0.1f),NiColorA(1,1,1,1));
	}
}
