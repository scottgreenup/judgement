
#include "ManaPickup.h"

ManaPickup::ManaPickup()
{
	m_kDebugColor = NiColorA(0.0f, 0.7f, 0.9f, 1.0f);
}

ManaPickup::~ManaPickup()
{

}

void ManaPickup::Update(Player& a_rkPlayer)
{
	// update the base
	Pickup::Update(a_rkPlayer);

	if (m_bIsCollected)
	{
		//a_rkPlayer.Heal(5);
	}
}

void ManaPickup::Draw()
{

}