
#include "Pickup.h"

#include "../../AppBase/Utilities.h"
#include "../../AppBase/DebugTriMesh.h"
#include "../../AppBase/DebugLineMesh.h"

#include "../../Time.h"

Pickup::Pickup()
{
	m_kDebugColor = NiColorA(1.0f, 1.0f, 1.0f, 1.0f);

	m_bIsAlive = false;
}

Pickup::~Pickup()
{

}

void Pickup::Update(Player& a_rkPlayer)
{
	NiPoint3 vDifference = a_rkPlayer.GetTranslate() - m_vTranslate;
	
	if (vDifference.Length() < 1.0f)
	{
		m_bIsCollected = true;
		m_bIsAlive = false;
	}

	NiPoint3 kVel = NiPoint3::ZERO;

	switch(m_eFlightStage)
	{
	case FLIGHTSTAGE_NONE:
		{
			break;
		}
	case FLIGHTSTAGE_INIT:
		{
			kVel = (m_kTarget - m_vTranslate) * 2.0f;

			if ( (m_kTarget - m_vTranslate).Length() < 0.2f )
			{
				m_eFlightStage = FLIGHTSTAGE_SEEK;
			}

			break;
		}
	case FLIGHTSTAGE_SEEK:
		{
			vDifference.Unitize();
			kVel = vDifference * 30.0f;
			break;
		}
	default:
		{
			break;
		}
	}

	m_vTranslate += kVel * ITime::DeltaTime();

}

void Pickup::DrawDebug()
{
	Utility::GetDebugTriMesh()->AddCylinder(m_vTranslate, 0.2f, 0.2f, 8, m_kDebugColor);
	Utility::GetDebugLineMesh()->AddCylinder(m_vTranslate, 0.21f, 0.21f, 8, NiColorA(0.0f, 0.0f, 0.0f, 1.0f));
}

void Pickup::Draw()
{

}

void Pickup::Spawn(const NiPoint3& ac_rvPos)
{
	m_vTranslate = ac_rvPos;

	m_bIsAlive = true;
	m_bIsCollected = false;

	m_fHangDelay = 2.0f;
	m_fHangTimer = 0.0f;

	// initial velocity
	m_kTarget = ac_rvPos + NiPoint3(
		NiRand() % 2 == 0 ? 2.0f + NiUnitRandom() * 1.5f : -2.0f - NiUnitRandom() * 1.5f,
		NiRand() % 2 == 0 ? 2.0f + NiUnitRandom() * 1.5f : -2.0f - NiUnitRandom() * 1.5f,
		3.0f + NiUnitRandom() * 1.5f);

	m_eFlightStage = FLIGHTSTAGE_INIT;
}