#ifndef _HEALTHPICKUP_H_
#define _HEALTHPICKUP_H_

#include "Pickup.h"

class HealthPickup : public Pickup
{
public:
	HealthPickup();
	~HealthPickup();

	void Update(Player& a_rkPlayer);
	void Draw();

protected:
private:

};

#endif