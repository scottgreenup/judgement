#ifndef _PICKUP_H_
#define _PICKUP_H_

#include <NiPoint3.h>
#include <NiColor.h>

#include "../Player.h"

class Pickup
{
public:
	Pickup();
	~Pickup();

	virtual void Update(Player& a_rkPlayer);
	virtual void Draw();

	virtual void DrawDebug();

	virtual void Spawn(const NiPoint3& ac_rvPos);

	bool Alive() const { return m_bIsAlive; }

protected:
	NiPoint3 m_vTranslate;
	NiColorA m_kDebugColor;

	bool m_bIsAlive;
	bool m_bIsCollected;

private:
	NiPoint3 m_kTarget;

	float m_fHangDelay;
	float m_fHangTimer;

	enum FLIGHTSTAGE
	{
		FLIGHTSTAGE_NONE,
		FLIGHTSTAGE_INIT,
		FLIGHTSTAGE_SEEK
	} m_eFlightStage;
};

#endif