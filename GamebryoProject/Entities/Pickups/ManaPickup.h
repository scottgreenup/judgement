#ifndef _MANAPICKUP_H_
#define _MANAPICKUP_H_

#include "Pickup.h"

class ManaPickup : public Pickup
{
public:
	ManaPickup();
	~ManaPickup();

	void Update(Player& a_rkPlayer);
	void Draw();

protected:
private:

};

#endif