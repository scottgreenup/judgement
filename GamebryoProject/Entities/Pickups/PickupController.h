#ifndef _PICKUPCONTROLLER_H_
#define _PICKUPCONTROLLER_H_

// manages pickups, just a wrapper for the code.

#include <NiMemObject.h>
#include "efd\StdContainers.h"
#include "Pickup.h"

enum PICKUP
{
	PICKUP_HEALTH,
	PICKUP_MANA,
	PICKUP_COUNT
};

class PickupController : public NiMemObject
{
public:
	PickupController();
	~PickupController();

	void Update(class Player& a_rkPlayer);
	void Draw();

	void SpawnPickups(PICKUP a_eType, unsigned int a_uiAmount, const NiPoint3& a_rkPos);
protected:
private:

	unsigned int m_uiSectionSize;
	unsigned int m_uiArraySize;
	class Pickup** m_akPickups;
};

#endif