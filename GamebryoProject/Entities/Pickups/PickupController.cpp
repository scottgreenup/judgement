#include "PickupController.h"

#include "HealthPickup.h"
#include "ManaPickup.h"

#include "..\Player.h"

// 00-49	= HEALTH
// 50-99	= MANA

PickupController::PickupController()
{
	m_uiSectionSize = 200;
	m_uiArraySize = PICKUP_COUNT * m_uiSectionSize;

	m_akPickups = new Pickup*[m_uiArraySize];

	for (unsigned int i = 0; i < m_uiArraySize; ++i)
	{
		if (i >= PICKUP_HEALTH * m_uiSectionSize && i < (PICKUP_HEALTH + 1) * m_uiSectionSize)
		{
			m_akPickups[i] = new HealthPickup();
		}
		else if (i >= PICKUP_MANA * m_uiSectionSize && i < (PICKUP_MANA + 1) * m_uiSectionSize)
		{
			m_akPickups[i] = new ManaPickup();
		}
	}
}

PickupController::~PickupController()
{
	for (unsigned int i = 0; i < m_uiArraySize; ++i)
	{
		delete m_akPickups[i];
	}

	delete[] m_akPickups;
}

void PickupController::Update(Player& a_rkPlayer)
{
	for (unsigned int i = 0; i < m_uiArraySize; ++i)
	{
		if (m_akPickups[i]->Alive())
		{
			m_akPickups[i]->Update(a_rkPlayer);
		}
	}
}

void PickupController::Draw()
{
	for (unsigned int i = 0; i < m_uiArraySize; ++i)
	{
		if (m_akPickups[i]->Alive())
		{
			m_akPickups[i]->DrawDebug();
		}
	}
}

void PickupController::SpawnPickups(PICKUP a_eType, unsigned int a_uiAmount, const NiPoint3& a_rkPos)
{
	for (unsigned int i = 0; i < a_uiAmount; ++i)
	{
		int iStart = (a_eType) * m_uiSectionSize;
		int iEnd = (a_eType + 1) * m_uiSectionSize;

		// spawn health pickups
		for (int j = iStart; j < iEnd; ++j)
		{
			if (m_akPickups[j]->Alive() == false)
			{
				m_akPickups[j]->Spawn(a_rkPos);
				break;
			}
		}
	}
}