
#include "HealthPickup.h"

HealthPickup::HealthPickup()
{
	m_kDebugColor = NiColorA(0.0f, 1.0f, 0.0f, 1.0f);
}

HealthPickup::~HealthPickup()
{

}

void HealthPickup::Update(Player& a_rkPlayer)
{
	// update the base
	Pickup::Update(a_rkPlayer);

	if (m_bIsCollected)
	{
		a_rkPlayer.IncreaseHP(5);
	}
}

void HealthPickup::Draw()
{

}