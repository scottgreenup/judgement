#include "Enemy.h"

#include <NiColor.h>
#include <NiPoint3.h>
#include <NiMatrix3.h>
#include <NiQuaternion.h>
#include <NiMath.h>

#include "../Player.h"

#include "../Pickups/PickupController.h"

#include "../../Time.h"
#include "../../AppBase/Utilities.h"
#include "../../AppBase/DebugLineMesh.h"
#include "../../AppBase/DebugTriMesh.h"

#include "../PhysX/PhysXRay.h"

Enemy::Enemy(NiNode* a_pkScene,NiPhysXScene* a_pkPhysXScene,NiPoint3 a_kSpawn)
	: m_pkPhysXScene(a_pkPhysXScene)
	, m_fAggroEnterRange(20.0f)
	, m_fAggroExitRange(30.0f)
	, m_fDespawnTimer(5.0f)
	, m_bAttack(true)
	, m_fAttackRange(2.0f)
	, m_fAttackDelay(1.5f)
	, m_fAttackTimer(0.0f)
	, m_uiAttackDamage(5)
{
	m_uiHP = 0;
	m_uiMaxHP = 100;
	m_bAlive = false;

	CreateActorManager("Characters/Demon/ANIM_Demon.kfm","Idle");

	NiTransform kNifTransform;
	kNifTransform.m_Translate = NiPoint3(0.0f, 0.0f, -2.0f);
	kNifTransform.m_Rotate = NiMatrix3::IDENTITY;
	kNifTransform.m_fScale = 2.0f;

	m_pkPhysXController = NiNew PhysXController(
		a_pkScene,
		a_pkPhysXScene,
		a_kSpawn,
		2.0f,
		1.0f,
		m_spkNifRoot,
		kNifTransform,
		"enemy");

	m_pkRaySteerM = NiNew PhysXRay( NiPoint3::ZERO, NiPoint3::ZERO );
	m_pkRaySteerL = NiNew PhysXRay( NiPoint3::ZERO, NiPoint3::ZERO );
	m_pkRaySteerR = NiNew PhysXRay( NiPoint3::ZERO, NiPoint3::ZERO );
}

Enemy::~Enemy()
{
	NiDelete m_pkRaySteerM;
	NiDelete m_pkRaySteerL;
	NiDelete m_pkRaySteerR;
}

void Enemy::Update(Player& a_rkPlayer)
{
	if(m_uiHP <= 0)
	{
		UpdateDeath();
		return;
	}

	NiPoint3 kVel = NiPoint3::ZERO;

	if(!m_bAttack)
	{
		SetTargetAnimation("Idle");
	}

	NiPoint3 vDiff = m_pkPhysXController->GetTranslate() - a_rkPlayer.GetTranslate();
	if(vDiff.Length() < m_fAggroEnterRange)
	{
		// move
		UpdateMovement(a_rkPlayer);

		// rotate
		float fAngle = NiATan2(vDiff.x,vDiff.y);
		NiMatrix3 kRot;
		kRot.MakeZRotation(fAngle);
		m_spkNifRoot->SetRotate(kRot);

		// attack
		if(m_fAttackTimer == 0)
		{
			UpdateAttack(a_rkPlayer);
		}

		ActivateSequence("Run");
	}
	else
	{
		DeactivateSequence("Run");
	}

	if(m_fAttackTimer > 0)
	{
		m_fAttackTimer -= ITime::DeltaTime();

		if (m_fAttackTimer <= 0)
			m_fAttackTimer = 0;
	}

	m_spkActorManager->Update(ITime::CurrentTime());

	if(m_uiHP <= 0)
	{
		UpdateDeath();
	}

	Agent::Update();
}

void Enemy::UpdateDeath()
{
	DeactivateSequence("Run");

	// health is 0, setup death animation
	if(!m_bDespawn)
	{
		m_bDespawn = true;
		ActivateSequence("Death 01");
	}

	m_fDespawnTimer -= ITime::DeltaTime();
	if(m_fDespawnTimer <= 0)
	{
		m_bAlive = false;
		DeactivateSequence("Death 01");
	}
	
}

void Enemy::UpdateAttack(Player& a_pkPlayer)
{
	NiPoint3 vForward;
	m_spkNifRoot->GetRotate().GetCol(1,vForward);
	vForward.Unitize();

	NiPoint3 vEnemyPlayerDiff = GetTranslate() - a_pkPlayer.GetTranslate();

	// if in circle radius
	if (vEnemyPlayerDiff.Length() <= m_fAttackRange)
	{
		// if in front (dot product)
		m_fAttackTimer = m_fAttackDelay;
		a_pkPlayer.DecreaseHP(m_uiAttackDamage);

		Utility::ConsoleMessage("Attack player\n");
		
		NiActorManager::SequenceID kAttack = m_spkActorManager->FindSequenceID("Attack 01");
		m_spkActorManager->SetTargetAnimation(kAttack);
	}
}

void Enemy::OnDeath(PickupController& a_rkPickupController)
{
	a_rkPickupController.SpawnPickups(PICKUP_HEALTH, 10, m_spkNifRoot->GetTranslate());
}

void Enemy::Spawn(const NiPoint3& a_rvPos)
{
	m_pkPhysXController->SetTranslate(a_rvPos);
	m_uiHP = m_uiMaxHP;
	m_bAlive = true;
	m_bDespawn = false;
	m_fDespawnTimer = 5.0f;

	m_fAttackTimer = 0.0f;
}

void Enemy::UpdateMovement(Player& a_rkPlayer)
{

	NiMatrix3 mRotate = m_spkNifRoot->GetWorldRotate();
	NiPoint3 vForward;
	mRotate.GetCol(1, vForward);

	vForward.Unitize();

	NiPoint3 vRight = vForward.Cross(NiPoint3::UNIT_Z);
	NiPoint3 vLeft = -vRight;
	
	// move das rays
	m_pkRaySteerM->SetOrigin( GetTranslate() - vForward * 2.0f );
	m_pkRaySteerM->SetDirection( -vForward );

	Utility::GetDebugLineMesh()->AddLine( m_pkRaySteerM->GetOrigin(), m_pkRaySteerM->GetOrigin() + m_pkRaySteerM->GetDirection() * 10.0f, NiColorA(1.0f, 1.0f, 1.0f, 1.0f));

	m_pkRaySteerL->SetOrigin( vLeft + vForward * 2.0f);
	m_pkRaySteerL->SetDirection( vForward );

	m_pkRaySteerR->SetOrigin( vRight + vForward * 2.0f );
	m_pkRaySteerR->SetDirection( vForward );

	m_vVelocity = NiPoint3::ZERO;
	
	m_vDirection = a_rkPlayer.GetTranslate() - GetTranslate();
	m_vDirection.Unitize();
	m_vVelocity += m_vDirection * 7.0f;

	NxRaycastHit kHitInfo = m_pkRaySteerM->ShootFirst(m_pkPhysXScene);

	m_pkPhysXController->Move(m_vVelocity);
}