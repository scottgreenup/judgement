#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "../Agent.h"
#include "../Projectile.h"
#include <NiPhysXScene.h>

class Enemy : public Agent
{
	friend class CombatController;
public:
	Enemy(NiNode* m_pkScene, NiPhysXScene* a_pkPhysXScene, NiPoint3 a_kSpawn);
	virtual ~Enemy();

	virtual void Update(class Player& a_rkPlayer);
	virtual void OnDeath(class PickupController& a_rkPickupController);
	void Spawn(const NiPoint3& a_rvPos);
	void UpdateAttack(class Player& a_pkPlayer);

protected:
	virtual void UpdateDeath();
	virtual void UpdateMovement(class Player& a_rkPlayer);

	//static void RayHitM(const NxRaycastHit& a_rkHitReport);
	//static void RayHitL(const NxRaycastHit& a_rkHitReport);
	//static void RayHitR(const NxRaycastHit& a_rkHitReport);

	float m_fMovementSpeed;

	float m_fAttackRange;
	float m_fAttackDelay;
	float m_fAttackTimer;

	float m_fAggroEnterRange;
	float m_fAggroExitRange;

	unsigned int m_uiAttackDamage;	

	float m_fDespawnTimer;
	bool m_bDespawn;
	bool m_bAttack;

	class NiPhysXScene* m_pkPhysXScene;

	// line of sight
	class PhysXRay* m_pkRaySight;

	// steering behaviour
	class PhysXRay* m_pkRaySteerM;
	class PhysXRay* m_pkRaySteerL;
	class PhysXRay* m_pkRaySteerR;

	NiPoint3 m_vVelocity;
	NiPoint3 m_vDirection;

private:
	Enemy() { };

};

#endif _ENEMY_H_