#include "Boss.h"

#include <NiColor.h>
#include <NiPoint3.h>
#include <NiMath.h>
#include <NiMatrix3.h>
#include <NiQuaternion.h>

#include "..\AppBase\Utilities.h"
#include "..\AppBase\DebugLineMesh.h"
#include "..\AppBase\DebugTriMesh.h"
#include "..\Time.h"

#include ".\Pickups\PickupController.h"

#include "Player.h"

#include "Projectile.h"

//////////////////////////////////////////////////////////////////////////
Boss::Boss()
	: Enemy(nullptr, nullptr, NiPoint3::ZERO)
{
	m_uiAttackDamage = 10;
	m_fAttackRange = 5.0f;
	m_fAttackTimer = 0.0f;
	m_fAttackDelay = 0.05f;

	m_fMovementSpeed = 20.0f;

	m_fChangeTimer = 0.0f;
	m_fChangeDelay = 0.1f;

	m_fStateTimer = 0.0f;
	m_fStateDelay = 2.0f;

	m_uiHP = 200;
	m_uiMaxHP = 200;

	m_uiProjArraySize = 50;
}

//////////////////////////////////////////////////////////////////////////
Boss::~Boss()
{

}

//////////////////////////////////////////////////////////////////////////
void Boss::Update(Player &a_rkPlayer)
{
	m_fStateTimer += ITime::DeltaTime();

	// convert to state machine
	switch (m_eState)
	{
	//////////////////////////////////////////////////////
	case STATE_SWITCHING_A:
		{
			m_fChangeTimer += ITime::DeltaTime();

			if (m_fChangeTimer >= m_fChangeDelay)
			{
				m_fChangeTimer = 0.0f;
			}

			NiPoint3 kGoal = a_rkPlayer.GetTranslate() + m_kGoal;
			NiPoint3 kVel = kGoal - m_pkTranslate;
			kVel.Unitize();
			m_pkTranslate += kVel * ITime::DeltaTime() * m_fMovementSpeed;

			m_fAttackTimer = 0.0f;

			if (m_fStateTimer >= 2.0f)
			{
				m_eState = STATE_ATTACKING_HALOS;
				m_fStateTimer = 0.0f;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_ATTACKING_HALOS:
		{
			if (m_fAttackTimer >= m_fAttackDelay)
			{

				NiPoint3 kDirection = a_rkPlayer.GetTranslate() - m_pkTranslate;


				for(unsigned int i = 0; i < m_uiProjArraySize; ++i)
				{
					if(!m_akProjectiles[i].IsAlive())
					{
						m_akProjectiles[i].Respawn(
							m_pkTranslate,
							kDirection,
							50.0f,
							m_uiAttackDamage / 2);
						break;
					}
				}
				

				m_fAttackTimer = 0.0f;
			}

			m_fAttackTimer += ITime::DeltaTime();

			if (m_fStateTimer >= 1.7f)
			{
				m_eState = STATE_SWITCHING_B;
				m_fStateTimer = 0.0f;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_SWITCHING_B:
		{
			m_fChangeTimer += ITime::DeltaTime();

			if (m_fChangeTimer >= m_fChangeDelay)
			{
				m_fChangeTimer = 0.0f;
			}

			NiPoint3 kGoal = a_rkPlayer.GetTranslate() + m_kGoal;
			NiPoint3 kVel = kGoal - m_pkTranslate;
			kVel.Unitize();
			m_pkTranslate += kVel * ITime::DeltaTime() * m_fMovementSpeed;

			if (m_fStateTimer >= 1.0f)
			{
				m_eState = STATE_SEEKING;
				m_fStateTimer = 0.0f;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_SEEKING:
		{
			NiPoint3 kGoal = a_rkPlayer.GetTranslate() - m_pkTranslate;

			if (kGoal.Length() <= 1.0f)
			{
				m_eState = STATE_ATTACKING_MELEE;
				m_iAttackCount = 0;
			}
			else
			{
				kGoal.Unitize();
				m_pkTranslate += kGoal * ITime::DeltaTime() * m_fMovementSpeed;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_ATTACKING_MELEE:
		{
			m_fAttackTimer += ITime::DeltaTime();

			if (m_fAttackTimer >= m_fAttackDelay)
			{
				m_iAttackCount++;
				a_rkPlayer.DecreaseHP(m_uiAttackDamage);
			}

			// state has been switched
			if (m_iAttackCount == 3)
			{
				m_eState = STATE_ATTACKING_IDLE;

				m_kGoal = NiPoint3(
					29.393f,
					0.0f,
					6.0f);

				NiMatrix3 kRot;
				kRot.MakeZRotation(NiUnitRandom() * 2.0f * NI_PI);

				m_kGoal = m_kGoal * kRot;
				m_fStateTimer = 0.0f;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_ATTACKING_IDLE:
		{
			if (m_fStateTimer > 2.0f)
			{
				m_eState = STATE_BACKINGUP;
				m_fStateTimer = 0.0f;
			}

			break;
		}
	//////////////////////////////////////////////////////
	case STATE_BACKINGUP:
		{
			NiPoint3 kGoal = a_rkPlayer.GetTranslate() + m_kGoal;

			NiPoint3 kVel = kGoal - m_pkTranslate;
			kVel.Unitize();

			m_pkTranslate += kVel * ITime::DeltaTime() * m_fMovementSpeed;

			if (m_fStateTimer > 2.0f)
			{
				m_eState = STATE_SWITCHING_A;
			}


			break;
		}
	};

	for(unsigned int i = 0;i < m_uiProjArraySize;++i)
	{
		if(m_akProjectiles[i].IsAlive())
		{
			m_akProjectiles[i].Update(a_rkPlayer);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void Boss::DrawDebug()
{
	Utility::GetDebugTriMesh()->AddAABB(
		m_pkTranslate,
		NiPoint3(1.6f,1.6f,1.6f),
		NiColorA(0.7f, 0.0f, 1.0f, 1.0f));

	float fScale = m_uiHP / m_uiMaxHP * 0.5f;
	float fHealthColoring = fScale * 2.0f;

	Utility::GetDebugTriMesh()->AddAABB(m_pkTranslate + NiPoint3::UNIT_Z * 4.0f, NiPoint3(fScale,fScale,fScale), NiColorA(1 - fHealthColoring,fHealthColoring,0,1));

	NiColorA kOutline(0.0f, 1.0f, 0.0f, 1.0f);

	if (m_eState == STATE_ATTACKING_HALOS || m_eState == STATE_ATTACKING_MELEE)
		kOutline = NiColorA(1.0f, 0.0f, 0.0f, 1.0f);

	Utility::GetDebugLineMesh()->AddAABB(
		m_pkTranslate,
		NiPoint3(1.61f,1.61f,1.61f),
		kOutline);

	for(unsigned int i = 0;i < m_uiProjArraySize;++i)
	{
		if(m_akProjectiles[i].IsAlive())
		{
			m_akProjectiles[i].DebugDraw();
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void Boss::Draw() const
{

}

//////////////////////////////////////////////////////////////////////////
void Boss::Spawn(class NiPoint3& a_rvPos)
{
	m_eState = STATE_SWITCHING_A;

	m_kGoal = NiPoint3(
		29.393f,
		0.0f,
		6.0f);
	NiMatrix3 kRot;
	kRot.MakeZRotation(NiUnitRandom() * 2.0f * NI_PI);
	m_kGoal = m_kGoal * kRot;

	m_uiHP = 200;
	m_uiMaxHP = 200;
	m_fAttackTimer = 0.0f;

	m_pkTranslate = a_rvPos;
}

//////////////////////////////////////////////////////////////////////////
void Boss::AttackMelee(Player& a_rkPlayer)
{
	a_rkPlayer.DecreaseHP(m_uiAttackDamage);
}

//////////////////////////////////////////////////////////////////////////
void Boss::OnDeath(PickupController& a_rkPickupController)
{
	a_rkPickupController.SpawnPickups(PICKUP_HEALTH, 20, m_pkTranslate);
	a_rkPickupController.SpawnPickups(PICKUP_MANA, 20, m_pkTranslate);
}