#include "./PhysXRay.h"

PhysXRay::PhysXRay(const NiPoint3& a_rkOrigin,const NiPoint3& a_rkDirection )
{
	m_kOrigin = a_rkOrigin;
	m_kDirection = a_rkDirection;

	NxVec3 kOrigin;
	NxVec3 kDirection;

	NiPhysXTypes::NiPoint3ToNxVec3(m_kOrigin,kOrigin);
	NiPhysXTypes::NiPoint3ToNxVec3(m_kDirection,kDirection);

	m_kRay.orig = kOrigin;
	m_kRay.dir = kDirection;
	m_kRay.dir.normalize();

	m_pkCallback = new PhysXRayCallback;
}

PhysXRay::PhysXRay()
{
	m_kOrigin = NiPoint3::ZERO;
	m_kDirection = NiPoint3::ZERO;
	m_pkCallback = new PhysXRayCallback;
}

PhysXRay::~PhysXRay()
{
	delete m_pkCallback;
}

void PhysXRay::SetOnHitCallback( PhysXRayCallback::onHitFunctor a_pkHitFunctionPtr )
{
	if(m_pkCallback == nullptr)
	{
		m_pkCallback = new PhysXRayCallback();
	}

	m_pkCallback->onHitFunc = a_pkHitFunctionPtr;
}

void PhysXRay::Shoot( NiPhysXScene* a_pkPhysXScene )
{
	if(m_pkCallback != nullptr)
	{
		a_pkPhysXScene->GetPhysXScene()->raycastAllShapes(m_kRay,*m_pkCallback,NX_ALL_SHAPES);
	}
}

NxRaycastHit& PhysXRay::ShootFirst( NiPhysXScene* a_pkPhysXScene )
{
	a_pkPhysXScene->GetPhysXScene()->raycastClosestShape(m_kRay, NX_ALL_SHAPES, m_kLatest);
	return m_kLatest;
}

NiPoint3 PhysXRay::GetOrigin() const
{
	return m_kOrigin;
}

void PhysXRay::SetOrigin( const NiPoint3& a_rkOrigin )
{
	m_kOrigin = a_rkOrigin;

	NxVec3 kOrigin;
	NiPhysXTypes::NiPoint3ToNxVec3(m_kOrigin,kOrigin);
	m_kRay.orig = kOrigin;
}

NiPoint3 PhysXRay::GetDirection() const
{
	return m_kDirection;
}

void PhysXRay::SetDirection( const NiPoint3& a_rkDirection )
{
	m_kDirection = a_rkDirection;

	NxVec3 kDirection;
	NiPhysXTypes::NiPoint3ToNxVec3(m_kDirection,kDirection);
	m_kRay.dir = kDirection;
	m_kRay.dir.normalize();
}