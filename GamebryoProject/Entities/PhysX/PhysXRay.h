#ifndef __PHYSXRAY_H_
#define __PHYSXRAY_H_

#include <NiPhysXScene.h>
#include "./PhysXRayCallback.h"

#include "../../AppBase/Utilities.h"
#include "../../AppBase/DebugTriMesh.h"

class PhysXRay : public NiMemObject
{
public:
	PhysXRay();
	~PhysXRay();

	PhysXRay(const NiPoint3& a_rkOrigin,const NiPoint3& a_rkDirection);

	void SetOnHitCallback(PhysXRayCallback::onHitFunctor a_pkHitFunctionPtr);

	void Shoot(NiPhysXScene* a_pkPhysXScene);

	NxRaycastHit& ShootFirst( NiPhysXScene* a_pkPhysXScene );

	NiPoint3 GetOrigin() const;

	void SetOrigin(const NiPoint3& a_rkOrigin);

	NiPoint3 GetDirection() const;

	void SetDirection(const NiPoint3& a_rkDirection);

private:
	NxRay m_kRay;
	NiPoint3 m_kOrigin;
	NiPoint3 m_kDirection;
	PhysXRayCallback* m_pkCallback;

	NxRaycastHit m_kLatest;
};

#endif//__PHYSXRAY_H_