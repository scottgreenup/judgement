#include "PhysXController.h"

//////////////////////////////////////////////////////////////////////////
#include <efdPhysX/PhysXSDKManager.h>
#include <NxCapsuleController.h>
#include <NiPhysXTransformDest.h>

#include "../../Time.h"
#include "../../Input/Input.h"

//////////////////////////////////////////////////////////////////////////
PhysXController::PhysXController(
	NiNode* a_pkScene,
	NiPhysXScene* a_pkPhysXScene,
	const NiPoint3& ac_rvSpawnPosition,
	const float ac_fHeight,
	const float ac_fWidth,
	NiAVObject* a_pkNif,
	const NiTransform& ac_rkTransform,
	const char* ac_szName)
{
	//Create PhysX Manager
	efdPhysX::PhysXSDKManager* pkManager = efdPhysX::PhysXSDKManager::GetManager();
	NxUserAllocator& kAllocator = pkManager->m_pPhysXSDK->getFoundationSDK().getAllocator();
	m_pkCtrlManager = NxCreateControllerManager(&kAllocator);

	m_pkCallback = new PhysXCallback();

	//Create Character Capsule Collider
	NxCapsuleControllerDesc kCapControllerDesc;
	kCapControllerDesc.setToDefault();

	//kCapControllerDesc.upDirection = NX_Z;
	kCapControllerDesc.radius = ac_fWidth;
	kCapControllerDesc.height = ac_fHeight;
	kCapControllerDesc.skinWidth = 0.0001f;
	kCapControllerDesc.slopeLimit = NiCos(NI_HALF_PI * 0.75f);
	kCapControllerDesc.stepOffset = 0.5f;
	kCapControllerDesc.callback = m_pkCallback;

	//Set Capsule Position
	NxVec3 vSpawn;
	NiPhysXTypes::NiPoint3ToNxVec3(ac_rvSpawnPosition,vSpawn);
	kCapControllerDesc.position.x = vSpawn.x;
	kCapControllerDesc.position.y = vSpawn.y;
	kCapControllerDesc.position.z = vSpawn.z;
	kCapControllerDesc.upDirection = NX_Z;

	//Create Controller
	NxScene* pkPhysXScene = a_pkPhysXScene->GetPhysXScene();
	m_pkController = m_pkCtrlManager->createController(pkPhysXScene,kCapControllerDesc);

	//Set Nif Translation
	a_pkNif->SetTranslate(ac_rkTransform.m_Translate);
	a_pkNif->SetRotate(ac_rkTransform.m_Rotate);
	a_pkNif->SetScale(ac_rkTransform.m_fScale);
	
	
	//Create Node Linking
	NiNodePtr kColliderRoot = NiNew NiNode();
	NiNodePtr kNifRoot = NiNew NiNode();

	NiMatrix3 kRotCorrect;
	kRotCorrect.MakeXRotation(NI_HALF_PI);
	kNifRoot->SetRotate(kRotCorrect);

	kColliderRoot->AttachChild(kNifRoot);
	kNifRoot->AttachChild(a_pkNif);

	//Add PhysX Destination To Scene
	NxActor* pkControllerActor = m_pkController->getActor();
	pkControllerActor->userData = (void*)a_pkNif;
	pkControllerActor->setName(ac_szName);
	

	m_pkTransformDest = NiNew NiPhysXTransformDest(kColliderRoot,pkControllerActor,NULL);
	a_pkPhysXScene->AddDestination(m_pkTransformDest);
	a_pkScene->AttachChild(kColliderRoot);

	//Set Gravity From Scene
	NxVec3 vSceneGavity;
	pkPhysXScene->getGravity(vSceneGavity);
	NiPhysXTypes::NxVec3ToNiPoint3(vSceneGavity,m_vGravity);

	m_bJumping = false;
	m_bGravity = true;

	m_pkPhysXScene = a_pkPhysXScene;
	m_pkScene = a_pkScene;
	m_pkNifRoot = NiDynamicCast(NiNode,a_pkNif);
}

//////////////////////////////////////////////////////////////////////////
PhysXController::~PhysXController()
{
	m_pkScene->DetachChild(m_pkNifRoot);
	m_pkPhysXScene->DeleteDestination(m_pkTransformDest);

	delete m_pkCallback;
	NxReleaseControllerManager(m_pkCtrlManager);
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::Move(const NiPoint3& a_vVelocity)
{
	NxVec3 vDisplacement;
	NiPhysXTypes::NiPoint3ToNxVec3(a_vVelocity,vDisplacement);
	
	//Apply Gravity to Character
	if(m_bGravity)
	{
		NxVec3 vGravity;
		NiPhysXTypes::NiPoint3ToNxVec3(m_vGravity,vGravity);
		vDisplacement += vGravity;
	}

	//Change Velocity if the Character is Jumping
	if(m_bJumping)
	{
		NxVec3 vJumpVel(0,0,m_fJumpImpulse);
		vDisplacement += vJumpVel;

		if(m_fJumpImpulse > m_vGravity.z)
		{
			m_fJumpImpulse += m_vGravity.z * ITime::DeltaTime();
		}

	}
 
	//Move Character
	unsigned int uiCollisionFlag = 0;
	vDisplacement = vDisplacement * ITime::DeltaTime();
	m_pkController->move(vDisplacement,COLLIDER_STATIC | COLLIDER_DYNAMIC,0.0000000001f,uiCollisionFlag);

	//Handle Collisions
	switch(uiCollisionFlag)
	{
	case NXCC_COLLISION_UP:
		{
			m_bJumping = false;
			m_pkCallback->onCollisionTop();
			break;
		}

	case NXCC_COLLISION_DOWN:
		{
			m_bJumping = false;
			m_pkCallback->onCollisionBottom();
			break;
		}

	case NXCC_COLLISION_SIDES:
		{
			m_pkCallback->onCollisionSide();
			break;
		}
	}

}

//////////////////////////////////////////////////////////////////////////
void PhysXController::Jump(const float a_fImpulseForce)
{
	if(!m_bJumping)
	{
		m_bJumping = true;
		m_fJumpImpulse = a_fImpulseForce;
	}
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::ApplyGravity(bool a_bGravityOn)
{
	m_bGravity = a_bGravityOn;
}

//////////////////////////////////////////////////////////////////////////
bool PhysXController::IsJumping() const
{
	return m_bJumping;
}

//////////////////////////////////////////////////////////////////////////
NiPoint3 PhysXController::GetTranslate() const
{
	NxVec3 vPosition = m_pkController->getActor()->getGlobalPosition();
	NiPoint3 vRet;
	NiPhysXTypes::NxVec3ToNiPoint3(vPosition,vRet);
	return vRet;
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetTranslate(const NiPoint3& a_rvPosition)
{
	NxExtendedVec3 vNxPosition(a_rvPosition.x,a_rvPosition.y,a_rvPosition.z);
	m_pkController->setPosition(vNxPosition);
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetShapeHitCallback(PhysXCallback::onShapeHitFunctor a_pHitFunc)
{
	m_pkCallback->onShapeHitFunc = a_pHitFunc;
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetControllerHitCallback(PhysXCallback::onControllerHitFunctor a_pHitFunc)
{
	m_pkCallback->onControllerHitFunc = a_pHitFunc;
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetCollisionTopCallback(PhysXCallback::onCollisionFunctor a_pCollisionFunc)
{
	m_pkCallback->onCollisionTop = a_pCollisionFunc;
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetCollisionBottomCallback(PhysXCallback::onCollisionFunctor a_pCollisionFunc)
{
	m_pkCallback->onCollisionBottom = a_pCollisionFunc;
}

//////////////////////////////////////////////////////////////////////////
void PhysXController::SetCollisionSideCallback(PhysXCallback::onCollisionFunctor a_pCollisionFunc)
{
	m_pkCallback->onCollisionSide = a_pCollisionFunc;
}

//////////////////////////////////////////////////////////////////////////
