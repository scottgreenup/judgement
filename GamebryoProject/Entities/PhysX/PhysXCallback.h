////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file PhysXCallback.h
/// @author Nathan Chambers
/// @date 01.08.12
/// @brief Declares the PhysX callback class.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _PHYSXCALLBACK_H_
#define _PHYSXCALLBACK_H_

///////////////////////////////////////////////////////////////////////////////
#include <NxCharacter.h>
#include <NxController.h>

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class PhysXCallback
/// @brief PhysX callback.
////////////////////////////////////////////////////////////////////////////////////////////////////
class PhysXCallback : public NxUserControllerHitReport
{
	friend class PhysXController;

public:
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @fn PhysXCallback::PhysXCallback();
	/// @brief Default constructor.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	PhysXCallback();
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @typedef void (*onShapeHitFunctor)(const NxControllerShapeHit& ac_rkHit)
	/// @brief Defines an alias representing a shape hit.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	typedef void (*onShapeHitFunctor)(const NxControllerShapeHit& ac_rkHit);
		

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @typedef void (*onControllerHitFunctor)(const NxControllersHit& ac_rkHit)
	/// @brief Defines an alias representing a controller hit.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	typedef void (*onControllerHitFunctor)(const NxControllersHit& ac_rkHit);


	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @typedef void (*onCollisionFunctor)()
	/// @brief Defines an alias representing the a collision functor.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	typedef void (*onCollisionFunctor)();

private:
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @fn virtual NxControllerAction PhysXCallback::onShapeHit(const NxControllerShapeHit& ac_rkHit);
	/// @brief Executes the shape hit action.
	/// @param ac_rkHit The shape hit.
	/// @return NxController Action
	////////////////////////////////////////////////////////////////////////////////////////////////////
	virtual NxControllerAction onShapeHit(const NxControllerShapeHit& ac_rkHit);


	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// @fn virtual NxControllerAction PhysXCallback::onControllerHit(const NxControllersHit& ac_rkHit);
	/// @brief Executes the controller hit action.
	/// @param ac_rkHit The controller hit.
	/// @return NxController Action
	////////////////////////////////////////////////////////////////////////////////////////////////////
	virtual NxControllerAction onControllerHit(const NxControllersHit& ac_rkHit);

	onShapeHitFunctor onShapeHitFunc;
	onControllerHitFunctor onControllerHitFunc;

	onCollisionFunctor onCollisionTop;
	onCollisionFunctor onCollisionBottom;
	onCollisionFunctor onCollisionSide;

};

#endif//_CHARACTER_CALLBACK_H_
