#include "PhysXRayCallback.h"

void DefaultOnHit( const NxRaycastHit& a_rkHitReport )
{

}

PhysXRayCallback::PhysXRayCallback()
{
	onHitFunc = &DefaultOnHit;
}

bool PhysXRayCallback::onHit( const NxRaycastHit& a_rkHitReport )
{
	onHitFunc(a_rkHitReport);
	return true;
}
