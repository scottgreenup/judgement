#include "CombatController.h"
#include "../../Time.h"

CombatController::AnimData::AnimData()
{
	kAnimName = "NONE";
	kSequenceID = 0;
	fDuration = 0.0f;
	fInterrupt = -1;
}

CombatController::AnimData::AnimData( Player* a_pkPlayer,NiString a_kAnimName,float a_fInterrupt /*= -1.0f*/ )
{
	NiActorManager::SequenceID kCurrentID = a_pkPlayer->GetActorManager()->FindSequenceID(a_kAnimName);
	NiSequenceData* pkSequenceData = a_pkPlayer->GetActorManager()->GetSequenceData(kCurrentID);

	kAnimName = a_kAnimName;
	kSequenceID = kCurrentID;
	fDuration = pkSequenceData->GetDuration() / 4.0f;
	if(a_fInterrupt < 0.0f)
	{
		fInterrupt = fDuration;
	}
	else
	{
		fInterrupt = a_fInterrupt;
	}
}


CombatController::CombatController( Player* a_pkPlayer )
{
	m_bIsAttacking = false;
	m_bQueuedAnim = false;
	m_iComboCount = 0;
	m_fAnimTimer = 0.0f;
	m_vMoveVel = NiPoint3(0,0,0);

	m_pkPlayer = a_pkPlayer;

	//Map Anim Data
	AnimData kAttack1(m_pkPlayer,"attack1",0.15f);
	m_akAnimData[kAttack1.kAnimName] = kAttack1;

	AnimData kAttack2(m_pkPlayer,"attack2",0.15f);
	m_akAnimData[kAttack2.kAnimName] = kAttack2;

	//AnimData kAttack3(m_pkPlayer,"attack3");
	//m_akAnimData[kAttack3.kAnimName] = kAttack3;
	 
	m_bAttackEnemy = true;
}

CombatController::~CombatController()
{

}

void CombatController::Update( efd::list<Enemy*>& a_apkEnemies )
{
	if(m_fAnimTimer > 0.0f)
	{
		m_fAnimTimer -= ITime::DeltaTime();

		if(m_fAnimTimer < m_kCurrentAnim.fDuration - m_kCurrentAnim.fInterrupt)
		{
			if(m_bQueuedAnim)
			{
				m_pkPlayer->GetActorManager()->DeactivateSequence(m_kCurrentAnim.kSequenceID);

				m_kCurrentAnim = m_kQueuedAnim;
				m_bQueuedAnim = false;
				m_bIsAttacking = true;

				m_fAnimTimer = m_kCurrentAnim.fDuration;
				m_pkPlayer->GetActorManager()->ActivateSequence(m_kCurrentAnim.kSequenceID);

				AttackEnemy(a_apkEnemies);
			}
		}

		float fClosestDist = 0;
		Enemy* pkClosestEnemy = nullptr;

		NiPoint3 vForward = m_pkPlayer->GetDirectionXY();

		//Look at closest enemy
		for(auto pIter = a_apkEnemies.begin();pIter != a_apkEnemies.end();pIter++)
		{
			if(!(*pIter)->IsAlive())
			{
				continue;
			}

			NiPoint3 vEnemyPlayerDiff = ((*pIter)->GetTranslate() - m_pkPlayer->GetTranslate());
			float fDist =  vEnemyPlayerDiff.Length();
			if(fDist < m_pkPlayer->m_fAttackRadius)
			{
				float fDotResult = vForward.Dot(vEnemyPlayerDiff);

				if (fDotResult > m_pkPlayer->m_fAngleOfAttack)
				{
					if(!pkClosestEnemy)
					{
						fClosestDist = fDist;
						pkClosestEnemy = (*pIter);
					}
					else if(fDist < fClosestDist)
					{
						fClosestDist = fDist;
						pkClosestEnemy = (*pIter);
					}
				}
			}
		}

		if(pkClosestEnemy)
		{
			NiPoint3 kVecDiff = (m_pkPlayer->GetTranslate() - pkClosestEnemy->GetTranslate());
			float fZRot = NiATan2(kVecDiff.x,kVecDiff.y);

			NiMatrix3 kRotMatrix;
			kRotMatrix.MakeZRotation(fZRot);
			m_pkPlayer->m_spkNifRoot->SetRotate(kRotMatrix);
		}
	}
	else
	{
		m_bAttackEnemy = true;

		m_pkPlayer->GetActorManager()->DeactivateSequence(m_kCurrentAnim.kSequenceID);
		m_bIsAttacking = false;

		m_iComboCount = 0;
	}
}

CombatController::AnimData CombatController::GetNextAttack()
{
	AnimData kSelectedAttack;

	if(m_iComboCount > 1)
	{
		m_iComboCount = 0;
	}

	switch(m_iComboCount)
	{
	case 0:
		{
			kSelectedAttack = m_akAnimData["attack1"];
			break;
		}

	case 1:
		{
			kSelectedAttack = m_akAnimData["attack2"];
			break;
		}

	case 2:
		{
			kSelectedAttack = m_akAnimData["attack3"];
			break;
		}
	}

	m_iComboCount ++;

	return kSelectedAttack;
}

void CombatController::LightAttack( efd::list<Enemy*>& a_apkEnemies )
{
	if(!m_bIsAttacking)
	{
		m_kCurrentAnim = GetNextAttack();
		m_fAnimTimer = m_kCurrentAnim.fDuration;

		m_vDir = m_pkPlayer->GetDirectionXY();
		m_pkPlayer->GetActorManager()->ActivateSequence(m_kCurrentAnim.kSequenceID);
		m_bAttackEnemy = true;
		m_bIsAttacking = true;
	}
	else
	{
		if(!m_bQueuedAnim)
		{
			m_vQueuedDir = m_pkPlayer->GetDirectionXY();
			m_kQueuedAnim = GetNextAttack();
			m_bQueuedAnim = true; 
		}
	}

	if(m_bAttackEnemy)
	{
		AttackEnemy(a_apkEnemies);
	}
}

void CombatController::AttackEnemy( efd::list<Enemy*>& a_apkEnemies )
{
	NiPoint3 vForward = m_pkPlayer->GetDirectionXY();
	vForward.Unitize();

	for(auto pIter = a_apkEnemies.begin();pIter != a_apkEnemies.end();pIter++)
	{
		NiPoint3 vEnemyPlayerDiff = (*pIter)->GetTranslate() - m_pkPlayer->GetTranslate();

		// if in circle radius
		if (vEnemyPlayerDiff.Length() < m_pkPlayer->m_fAttackRadius)
		{
			// if in front (dot product)

			float fDotResult = vForward.Dot(vEnemyPlayerDiff);

			if (fDotResult > m_pkPlayer->m_fAngleOfAttack)
			{
				m_bAttackEnemy = false;
				NiOutputDebugString("HIT ENEMY");
				(*pIter)->DecreaseHP(50);
				break;
			}
		}
	}
}

void CombatController::HeavyAttack()
{
	m_bIsAttacking = true;
}

bool CombatController::IsAttacking()
{
	return m_bIsAttacking;
}

