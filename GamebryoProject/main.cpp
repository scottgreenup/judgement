//////////////////////////////////////////////////////////////////////////
/// @author      Conan H. Bourke
/// @date        13.01.2012
/// @brief		Application starting point. Creates a new Application instance
///				and tracks memory leaks.
//////////////////////////////////////////////////////////////////////////



#include "Application.h"
#include <NiMain.h>
#include <NiLicense.h>
NiEmbedGamebryoLicenseCode;

#include <efd/DefaultInitializeMemoryManager.h>
EE_USE_DEFAULT_ALLOCATOR;

#include <crtdbg.h>

//////////////////////////////////////////////////////////////////////////
int WINAPI WinMain(HINSTANCE a_hInstance, HINSTANCE a_hPrevInstance, LPSTR a_szCommandLine, int a_iWinMode)
{
#ifdef _DEBUG
	// keep track of how many gamebryo objects there are to begin with
	unsigned int uiInitialCount = NiRefObject::GetTotalObjectCount();
#endif // _DEBUG

	// initialise gamebryo memory tracking
	NiInit();

	// create our application instance
	Application* pApp = NiNew Application();

	// if successfully created, start main game loop
	if (pApp->Create(a_szCommandLine))
	{
		pApp->Execute();
	}

	// cleanup memory
	NiDelete pApp;
	pApp = nullptr;
	NiShutdown();

#ifdef _DEBUG
	// check if there are any 'object leaks'
	unsigned int uiFinalCount = NiRefObject::GetTotalObjectCount();
	char acMsg[256];
	NiSprintf(acMsg, 256,"\nGamebryo NiRefObject counts:  initial = %u, final = %u. ", uiInitialCount, uiFinalCount);
	NiOutputDebugString(acMsg);

	if (uiFinalCount > uiInitialCount)
	{
		unsigned int uiDiff = uiFinalCount - uiInitialCount;
		NiSprintf(acMsg, 256, "Application is leaking %u objects!\n\n", uiDiff);
		NiOutputDebugString(acMsg);
	}
	else
	{
		NiOutputDebugString("Application has no object leaks.\n\n");
	}

	_CrtDumpMemoryLeaks();
#endif // _DEBUG

	return 0;
}