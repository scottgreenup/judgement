//////////////////////////////////////////////////////////////////////////
#ifndef __AUDIOMANAGER_H_
#define __AUDIOMANAGER_H_
//////////////////////////////////////////////////////////////////////////
#include <NiMemObject.h>

//////////////////////////////////////////////////////////////////////////
// FMOD Wrapper
class AudioManager : public NiMemObject
{
public:

	static AudioManager*	Initialise();
	static AudioManager*	GetSingleton()	{	return sm_pSingleton;	}
	static void				Shutdown();

	void					Update();

	void					Mute(bool a_bMute);
	bool					IsMuted() const						{	return m_bMuted;		}

	void					SetMusicVolume(float a_fVolume);
	float					GetMusicVolume() const				{	return m_fMusicVolume;	}

	void					SetFXVolume(float a_fVolume);
	float					GetFXVolume() const					{	return m_fFXVolume;		}

private:

	AudioManager();
	~AudioManager();

private:

	bool					m_bMuted;
	float					m_fMusicVolume;
	float					m_fFXVolume;

	static AudioManager*	sm_pSingleton;
};

//////////////////////////////////////////////////////////////////////////
#endif // __AUDIOMANAGER_H_
//////////////////////////////////////////////////////////////////////////