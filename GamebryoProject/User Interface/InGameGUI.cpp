////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file User Interface\InGameGUI.cpp
/// @author Scott Greenup
/// @date 04.09.2012
/// @bug Update doesn't centre on window, it centres based upon 1280, 720
/// @brief Implements the in game graphical user interface class.
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "InGameGUI.h"

#include <NiMeshScreenElements.h>
#include <NiMaterialProperty.h>
#include <NiTexturingProperty.h>
#include <NiAlphaProperty.h>

#include "../Entities/Player.h"

InGameGUI::InGameGUI()
{
	NiTexturingPropertyPtr pkTexPropBack	= NiNew NiTexturingProperty("./Textures/GUI/healthbar_background.bmp");
	NiTexturingPropertyPtr pkTexPropFore	= NiNew NiTexturingProperty("./Textures/GUI/healthbar_foreground.bmp");
	NiTexturingPropertyPtr pkTexPropOver		= NiNew NiTexturingProperty("./Textures/GUI/healthbar_overlay.dds");

	NiMaterialPropertyPtr pkMatProp	= NiNew NiMaterialProperty();
	pkMatProp->SetEmittance(NiColor(1, 1, 1));

	NiAlphaPropertyPtr pkAlphaProp = NiNew NiAlphaProperty();
	pkAlphaProp->SetAlphaBlending(true);
	pkAlphaProp->SetAlphaTesting(true);

	m_pkHPBack = NiMeshScreenElements::Create(
		NiRenderer::GetRenderer(),
		0.0f,0.0f, 300, 32,
		NiRenderer::CORNER_TOP_LEFT);

	m_pkHPFore = NiMeshScreenElements::Create(
		NiRenderer::GetRenderer(),
		0.0f,0.0f, 300, 32,
		NiRenderer::CORNER_TOP_LEFT);

	m_pkHPOver = NiMeshScreenElements::Create(
		NiRenderer::GetRenderer(),
		0.0f,0.0f, 300, 32,
		NiRenderer::CORNER_TOP_LEFT);

	m_pkHPBack->AttachProperty(pkTexPropBack);
	m_pkHPFore->AttachProperty(pkTexPropFore);
	m_pkHPOver->AttachProperty(pkTexPropOver);

	m_pkHPBack->AttachProperty(pkMatProp);
	m_pkHPFore->AttachProperty(pkMatProp);
	m_pkHPOver->AttachProperty(pkMatProp);

	m_pkHPOver->AttachProperty(pkAlphaProp);

	m_pkHPBack->Update(0);
	m_pkHPBack->UpdateProperties();

	m_pkHPFore->Update(0);
	m_pkHPFore->UpdateProperties();

	m_pkHPOver->Update(0);
	m_pkHPOver->UpdateProperties();
}

InGameGUI::~InGameGUI()
{
	delete m_pkHPBack;
	delete m_pkHPFore;
	delete m_pkHPOver;
}

void InGameGUI::Update(Player const * const ac_pkPlayer)
{
	float fX;
	float fY;
	float fSizeX;
	float fSizeY;

	NiRenderer::GetRenderer()->ConvertFromPixelsToNDC( 1280 / 2 - 338 / 2, 720 - 33 - 10, fX, fY);
	NiRenderer::GetRenderer()->ConvertFromPixelsToNDC( (unsigned int)(338 * (ac_pkPlayer->GetHP() / (float)ac_pkPlayer->GetMaxHP()) ), 33, fSizeX, fSizeY);

	m_pkHPFore->SetRectangle(0, fX, fY, fSizeX, fSizeY);

	NiRenderer::GetRenderer()->ConvertFromPixelsToNDC(338, 33, fSizeX, fSizeY);

	m_pkHPBack->SetRectangle(0, fX, fY, fSizeX, fSizeY);
	m_pkHPOver->SetRectangle(0, fX, fY, fSizeX, fSizeY);
}

void InGameGUI::Render(NiRenderer* a_pkRenderer)
{
	a_pkRenderer->SetScreenSpaceCameraData();
	m_pkHPBack->RenderImmediate(a_pkRenderer);
	m_pkHPFore->RenderImmediate(a_pkRenderer);
	m_pkHPOver->RenderImmediate(a_pkRenderer);
}