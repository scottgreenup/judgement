#ifndef _INGAMEGUI_H_
#define _INGAMEGUI_H_

#include <NiMemObject.h>

class InGameGUI : public NiMemObject
{
public:
	InGameGUI();
	~InGameGUI();

	void Update(class Player const * const ac_pkPlayer);
	void Render(class NiRenderer* a_pkRenderer);

protected:
private:
	class NiMeshScreenElements* m_pkHPBack;
	class NiMeshScreenElements* m_pkHPFore;
	class NiMeshScreenElements* m_pkHPOver;
};

#endif