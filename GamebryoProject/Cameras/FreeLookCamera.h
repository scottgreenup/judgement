#ifndef _FREELOOKCAMERA_H_
#define _FREELOOKCAMERA_H_

// #include <Camera.h>

#include "../AppBase/Utilities.h"

class FreeLookCamera
{
public:
	FreeLookCamera();
	~FreeLookCamera();

	void Update(float a_fDT, NiCamera* a_pkCamera);

protected:
private:
};

#endif