#include "FreeLookCamera.h"

#include <NiCamera.h>

#include "../Input/Input.h"
#include "../Time.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
FreeLookCamera::FreeLookCamera()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
FreeLookCamera::~FreeLookCamera()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void FreeLookCamera::Update(float a_fDT, NiCamera* a_pkCamera)
{
	
	int dx, dy, dz;
	Input::Mouse()->GetPositionDelta(dx, dy, dz);

	NiPoint3 kForward		= a_pkCamera->GetWorldDirection();
	NiPoint3 kRight			= a_pkCamera->GetWorldRightVector();
	NiPoint3 kUp			= a_pkCamera->GetWorldUpVector();
	NiPoint3 kTranslation	= a_pkCamera->GetTranslate();

	float a_fMoveSpeed = 10.0f;

	// Translate camera
	float fSpeed = Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_LSHIFT) ? a_fMoveSpeed * 2 : a_fMoveSpeed;	

	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_W))
	{
		kTranslation += kForward * ITime::DeltaTime() * fSpeed;
	}
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_S))
	{
		kTranslation -= kForward * ITime::DeltaTime() * fSpeed;
	}
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_D))
	{
		kTranslation += kRight * ITime::DeltaTime() * fSpeed;
	}
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_A))
	{
		kTranslation -= kRight * ITime::DeltaTime() * fSpeed;
	}
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_Q))
	{
		kTranslation += kUp * ITime::DeltaTime() * fSpeed;
	}
	if (Input::Keyboard()->KeyIsDown(NiInputKeyboard::KEY_E))
	{
		kTranslation -= kUp * ITime::DeltaTime() * fSpeed;
	}

	if (dz != 0)
	{
		float fScroll = dz / 10.f;
		kTranslation += kForward * ITime::DeltaTime() * fSpeed * fScroll;
	}

	a_pkCamera->SetTranslate(kTranslation);
	a_pkCamera->Update(0);

	// check for camera rotation
	if (Input::Mouse()->ButtonIsDown(NiInputMouse::NIM_RIGHT))
	{
		NiMatrix3 mMat;

		// pitch
		if (dy != 0)
		{
			NiMatrix3 mMat;
			mMat.MakeRotation(dy / 150.f, kRight);
			a_pkCamera->SetRotate( mMat * a_pkCamera->GetRotate() );
			a_pkCamera->Update(0);
		}

		// yaw
		if (dx != 0)
		{
			NiMatrix3 mMat;
			mMat.MakeRotation(dx / 150.f, NiPoint3(0,0,1));
			a_pkCamera->SetRotate(  mMat * a_pkCamera->GetRotate() );
			a_pkCamera->Update(0);
		}
	}
}