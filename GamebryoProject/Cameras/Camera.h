#ifndef _CAMERAACTOR_H_
#define _CAMERAACTOR_H_

// going to use this for switching between brawl, third person and cinematic cameras
// need to discuss about whether this is feasible.
// preferably going Camera::GetCamera() or Camera::GetCameraActor() would be awesome.
// maybe this should be CameraActor...

// if it were static and abstract... then ThirdPersonCamera::GetCamera() is the same as
// Camera::GetCamera() because ThirdPersonCamera : public Camera ... fuck

// talk to Nathan =P

enum CAMERATYPE
{
	THIRDPERSON,
	BRAWL,
	FREEMOVE,
	CINEMATIC
};

class CameraActor
{
public:
protected:
private:
};

#include "Camera.inl"

#endif