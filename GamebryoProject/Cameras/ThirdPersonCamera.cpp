
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "ThirdPersonCamera.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
#include "../Input/Input.h"
#include "../Time.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
#include <NiMath.h>
#include <NiCamera.h>
#include <NiPoint3.h>
#include <NiMatrix3.h>

#include "../Entities/Player.h"
#include "../AppBase/Utilities.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
ThirdPersonCamera::ThirdPersonCamera()
	: m_fRotationY(0.0f)
	, m_fRotationZ(0.0f)
	, m_fSpeedY(0.2f)
	, m_fSpeedZ(0.2f)
	, m_fDist(2.0f)
	, m_fDistMax(30.0f)
	, m_fDistMin(10.0f)
	, m_bFreeToggle(false)
	, m_fHeight(5.0f)
	, m_fCurrentZ(0.0f)
	, m_fHeightDelay(2.0f)
	, m_fHeightTimer(0.0f)
	, m_fHeightLerped(5.0f)
{
	// these are points so we can have the "#define" and "class Vector3"
	m_pkOffset = NiNew DVector3();
	m_kCurrPos = NiNew NiPoint3();

}

////////////////////////////////////////////////////////////////////////////////////////////////////
ThirdPersonCamera::~ThirdPersonCamera()
{
	m_pkTarget = nullptr;
	delete m_pkOffset;
	delete m_kCurrPos;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ThirdPersonCamera::Update()
{
	Input::Mouse()->GetPositionDelta(m_iDeltaX, m_iDeltaY, m_iDeltaZ);

	m_fHeightTimer += ITime::DeltaTime();

	if (m_fHeightTimer > m_fHeightDelay && false)
	{
		m_fHeight += -m_iDeltaY * ITime::DeltaTime();

		if (m_fHeight < 2.8f)
			m_fHeight = 2.8f;
		else if (m_fHeight > 5.8f)
			m_fHeight = 5.8f;

		m_fHeightLerped = NiLerp(ITime::DeltaTime() * 10.0f, m_fHeightLerped, m_fHeight);
	}

	if (Input::Keyboard()->KeyWasPressed(NiInputKeyboard::KEY_F))
	{
		m_bFreeToggle = !m_bFreeToggle;
	}

	if ( m_bFreeToggle )
	{
		Utility::FreeCameraMovement(ITime::DeltaTime(), 20.0f, m_pkCamera, Input::Mouse(), Input::Keyboard());
	}
	else
	{
		UpdateRotation();
		UpdateZoom();
	}

	m_pkCamera->Update(ITime::DeltaTime());

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ThirdPersonCamera::UpdateRotation()
{
	const efd::Point3& upAxis = NiPoint3::UNIT_Z;

	//efd::Point3 cameraPos = m_pkCamera->GetTranslate();
//	efd::Point3 cameraRightDir = m_pkCamera->GetWorldRightVector();

	NiMatrix3 targetsRot = m_pkTarget->GetActorManager()->GetNIFRoot()->GetRotate();
	NiPoint3 targetsXYZ;
	targetsRot.ToEulerAnglesXYZ(targetsXYZ.x, targetsXYZ.y, targetsXYZ.z);

	m_fGoal = targetsXYZ.z;

	// this sorta fixes the probel of -180 < q < 180
	if ( m_fGoal > m_fCurrentZ)
	{
		if ( m_fGoal - m_fCurrentZ > NI_PI )
		{
			m_fGoal -= NI_PI * 2;
		}
	}
	else
	{
		if ( m_fGoal - m_fCurrentZ < -NI_PI )
		{
			m_fGoal += NI_PI * 2;
		}
	}


	if (m_fGoal > NI_PI * 2)
	{
		m_fGoal = m_fGoal - NI_PI * 2;
		m_fCurrentZ = m_fCurrentZ - NI_PI * 2 ;
	}


	if (m_fGoal < -NI_PI * 2)
	{
		m_fGoal = m_fGoal + NI_PI * 2;
		m_fCurrentZ = m_fCurrentZ + NI_PI * 2 ;
	}

	// get where we want the camera to be rotated
	m_fCurrentZ = NiLerp(ITime::DeltaTime() * 10.0f, m_fCurrentZ, m_fGoal);
	targetsRot.MakeZRotation(m_fCurrentZ);

	// get where we want the camera to be
	efd::Point3 targetPos = m_pkTarget->GetTranslate();
	efd::Point3 targetForwadDir = NiPoint3::UNIT_Y * targetsRot;
	targetPos.x -= targetForwadDir.x * 9;
	targetPos.y += targetForwadDir.y * 9;
	targetPos.z += m_fHeightLerped;

	targetPos.x = NiLerp(1.0f, m_kCurrPos->x, targetPos.x);
	targetPos.y = NiLerp(1.0f, m_kCurrPos->y, targetPos.y);
	targetPos.z = NiLerp(1.0f, m_kCurrPos->z, targetPos.z);

	*m_kCurrPos = targetPos;

	// move to target
	// calculate rotation axis
	efd::Point3 kForward = (m_pkTarget->GetTranslate() + NiPoint3(0, 0, 1.8f)) - targetPos;
	kForward.Unitize();

	efd::Point3 kRight = kForward.Cross(NiPoint3::UNIT_Z);
	kRight.Unitize();

	efd::Point3 kUp = kRight.Cross(kForward);
	kUp.Unitize();

	// set rotation
	NiMatrix3 kCameraRot(kForward, kUp ,kRight);

	m_pkCamera->SetTranslate(targetPos);
	m_pkCamera->SetRotate(kCameraRot);

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ThirdPersonCamera::UpdateZoom()
{
	// deltaZ is scroll-wheel
	m_fDist -= m_iDeltaZ * 0.01f;

	// clamp
	m_fDist = NiClamp(m_fDist, 0.0f, 20.0f);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void ThirdPersonCamera::SetTarget(Agent &a_kTarget)
{
	if (&a_kTarget != m_pkTarget)
		m_pkTarget = &a_kTarget;
}